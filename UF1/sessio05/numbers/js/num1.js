/*
Number

    1. Fes un programa on es forci el resultat NaN i es mostri per pantalla.

    2. Fes un programa on es forci el resultat -Infinity o Infinity i es mostri 
    per pantalla.
    
    3. Fes un programa per validar que no es un número

    4. Valida la longitud del campo código postal

*/
"use strict";
//1
console.log("1. " + parseInt(134 * "a"));
//2
console.log("2. " + Math.exp(999,999));

//3
const isNotNumber = (num) => {
    return isNaN(1 * num);
}
console.log("3. " + isNotNumber(134));
console.log("3.  " + isNotNumber("134aeiou"));

//4
const validateLength = () => {
    const numPost = prompt("Insert postcode");
    return (numPost + "").length == 5;
}
console.log("4. " + validateLength());
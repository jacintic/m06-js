/*
3. Crea un objecte de tipus Date amb la data i hora actual. Què passa si 
s’assigna amb el mètode setHours el valor 26? Què passa si s’assigna amb 
el mètode setMinutes el valor 65? Què passa si s’assigna com a dia de mes 
35?
*/
"use strict";

let myTime =  new Date();
console.log(myTime + "");
myTime.setHours(26);
console.log(myTime + " - It adds 24 + 2 hours to the current date");
myTime.setMinutes(65);
console.log(myTime + " - time is set to + 1 hour and minutes reset to 5");
myTime.setDate(2020,10,35);
console.log(myTime + " - it adds 6 years and don't know what else");
/*
12. Validar si una persona era major edat, en la data donada.
*/
"use strict";
// input data neixement
const dataNeixement = prompt("Insert the birth date in this format: YYYY,M,D");

const dN = new Date(dataNeixement);

console.log();

const dC = new Date(2015,0,1);



const yearDiff = (dC - dN) / (1000 * 60 * 60 * 24 * 365 ) ;
if (yearDiff < 18) {
    console.log("Not 18 yet");
} else {
    console.log("18+");

}
console.log("Notice there is a 5 day deviation in this program, (1997,1,6) will tell you he isn't 18 yet but anything lower than that will show + 18");

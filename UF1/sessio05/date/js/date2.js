/*
2. Fes una pàgina amb un botó i que calculi el temps que 
passa des de que es carrega el document de la página web 
fins que l’usuari pulsi el botó.
*/
"use strict";

const myNow =  new Date();
const myTime = () => {
    const buttonTime =  new Date();
    console.log("Milliseconds since button was clicked: " + (buttonTime - myNow) );
}
document.getElementById("button").addEventListener("click",myTime);


/*
1. Calcula els milisegons que falten fins al dia del teu 
aniversari.
*/
"use strict";
const birthday = new Date(2021,8,17);
const myNow =  new Date();
console.log("Milliseconds left for my birthday: " + (birthday - myNow) );

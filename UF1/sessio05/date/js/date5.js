/*
5. Crea un programa que mostri per pantalla la data en format català. 
Per exemple: 11 de setembre de 1714.


*/
"use strict";

// 5.
const dataCat = new Date(1714,8,11);
console.log(dataCat);
const dataInfo = dataCat.toString().split(" ");
const meuMes = dataCat.toString().split(" ")[1];
const mes = () => {
    
    let tradMes;
    switch(meuMes) {
        case 'Jan':
            tradMes = "Gener";
        break;
        case 'Feb':
            tradMes = "Febrer";
        break;
        case 'Mar':
            tradMes = "Març";
        break;
        case 'Apr':
            tradMes = "Abril";
        break;
        case 'May':
            tradMes = "Maig";
        break;
        case 'Jun':
            tradMes = "Juny";
        break;
        case 'Jul':
            tradMes = "Juliol";
        break;
        case 'Aug':
            tradMes = "Agost";
        break;
        case 'Sep':
            tradMes = "Septembre";
        break;
        case 'Oct':
            tradMes = "Octubre";
        break;
        case 'Nov':
            tradMes = "Novembre";
        break;
        case 'Dec':
            tradMes = "Dessembre";
        break;
    }
    return tradMes;
}
console.log(dataInfo[2] + " " + mes() + " de " + dataInfo[3]);
// 11 de setembre de 1714.


/*
6. Crea un programa que mostri per pantalla l’hora en format català. 
Per exemple: 01:45 tres quarts de dues // Calculem únicament els quarts
*/
//const hora = new Date();
const hora = new Date(2020,11,17,3,45);
let hores = parseInt(hora.getHours());
let minuts = parseInt(hora.getMinutes());

const hourFormat = () => {
    let strRes = "";
    let horesQuarts = hores;
    if (minuts == 15){
        horesQuarts++;
        strRes += " un quart de " + horesQuarts;
    } else if (minuts == 30) {
        horesQuarts++;
        strRes += " dos quarts de " + horesQuarts;
    } else if (minuts == 45) {
        horesQuarts++;
        strRes += " tres quarts de " + horesQuarts;
    }
    hores = hores <= 9 ? "0" + hores : hores;
    minuts = minuts <= 9 ? "0" + minuts : minuts;  
    strRes = hores + ":" + minuts + strRes;
    console.log(strRes);
}
hourFormat();


/*
7. Crea un programa que mostri l’hora per pantalla en format digital i 
l’actualitzi cada segon.
*/

const horaRef = () => {
    const myDate = new Date();
    let hours = parseInt(myDate.getHours());
    let minutes = parseInt(myDate.getMinutes());
    let seconds = parseInt(myDate.getSeconds());
    if (hours <= 9) {
        hours = "0" + hours;
    }
    if (minutes <= 9) {
        minutes = "0" + minutes;
    }
    if (seconds <= 9) {
        seconds = "0" + seconds;
    }
    document.getElementById("clock").innerText = hours + ":" + minutes + ":" + seconds;
}
setInterval(horaRef, 1000);
/*
8. Crea un programa on es demani a l’usuari una data, i calculi quants 
dies falten, o han passat respecte a la data actual.
*/
const dataIn = prompt("Inserta data format yyyy,m,d");
const dataCal = new Date(dataIn);
const daydiff = (new Date() - dataCal) / (1000 * 60 * 60 * 24);
console.log("Dies de diferencia: " +daydiff);


/*
9. Crea un comptador on es mostrin els anys, mesos, dies, hores i segons 
que falten fins a la inauguració dels pròxims Jocs Olímpics.
*/
let olymp = new Date(2021,6,23);
let datNow = new Date();
let diffYears = olymp - datNow / (1000 * 60 * 60 * 24 * 365);
console.log("Years left for the olympics: " + diffYears);
const diffMonths =   (olymp - datNow / (1000 * 60 * 60 * 24 * 30)) - ((diffYears * 365) / 30 );
console.log("Months left for the olympics: " + diffMonths);
const diffDays = (olymp - datNow / (1000 * 60 * 60 * 24)) - ( (diffMonths * 30) + (diffYears * 365) );
console.log("Days left for the olympics: " + diffDays);
const diffHours = (olymp - datNow / (1000 * 60 * 60 )) - ( (diffMonths * 30 * 24) + (diffYears * 365 * 24) );
console.log("Hours left for the olympics: " + diffHours);
const diffSeconds = (olymp - datNow / (1000 )) - ( (diffMonths * 30 * 24 * 60 * 60) + (diffYears * 365 * 24 * 60 * 60) );
console.log("Seconds left for the olympics: " + diffSeconds);
// no funciona y no entenc ben be per que

/*
11. Crea un programa on es demanin dues dates i digui quina és la més gran.
*/

const dat1 = new Date();
const dat2 = new Date(2020,6,8);
console.log(dat1 < dat2);
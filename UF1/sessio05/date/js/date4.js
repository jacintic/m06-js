/*
4. Demana la data de naixement a un usuari, i mostra l’edat en anys, 
mesos, dies, segons i milisegons.
*/
"use strict";

const birthday = prompt("Introduce your birthdate in YYYY,m,d format");
const birthDate =  new Date(birthday);

//const birthDate =  new Date(1986,7,17)
const dateNow = new Date(); 

const dNDays = dateNow.getDay();
const bDDays = birthDate.getDay();
const dnMonth = dateNow.getMonth();
const bDMonth = birthDate.getMonth();
const dNYear = dateNow.getFullYear();
const bDYear = birthDate.getFullYear();

const dnHours = dateNow.getHours();
const bdHours = birthDate.getHours();

const dnMinutes = dateNow.getMinutes();
const bdMinutes = birthDate.getMinutes();

const dnSeconds = dateNow.getSeconds();
const bdSeconds = birthDate.getSeconds();

const totalHours = dnHours - bdHours;
const totalMinutes = dnMinutes - bdMinutes;
const totalSeconds = dnSeconds - bdSeconds;


let totYears = dNYear - bDYear;
let totMonths = dnMonth - bDMonth;
let totDays = dNDays - bDDays;



const totalDate = () => {
   
    // calculate months
    if (totMonths < 0) {
        totMonths = 12 + totMonths;
        totYears--
    }
    if (totDays < 0) {
        totDays = 30 + totDays;
        totMonths--;
    }
      
}
totalDate();

const totalMiliseconds = new Date().getMilliseconds();
console.log("Years: " + totYears + " months: " + totMonths + " days: " + totDays + " Hours: " + totalHours + " minutes: " + totalMinutes + " seconds: " + totalSeconds + " milliseconds: " + totalMiliseconds);
# UF 06, Entorn Client.  
  
## Sessió 11: challenge UF1.  
  
### Documentar Javascript versions  
### Identifica, analitza, organitza i escriu quines són les diferents versions de javascript. ES5, Es6/ES2015 and ES6.  
  
## Comenzare especificando lo que yo se en este momento y continuare con la información que he buscado en internet. Lo que yo se:

ES5 es el estandar de JavaScript que funciona en todos los navegadores y al que se recurre siempre y cuando se busca "backwards compatibility", es decir que los navegadores y sistemas operativos antiguos ejecuten su codigo sin problema. Tanto es asi, que hoy en dia, hay desarrolladores que aun utilizar el estandar ES6, utilizan compiladores de codigo javascript que transforman el codigo original en ES6 en codigo valido ES5, de esta manera se asegura la compativilidad con navegadores antiguos. Cave destacar, que algunos metodos utilizados no compilan bien de ES6 a ES5, como es el ejemplo de el metodo `.closest()`. Este es el metodo y como funciona [Link a metodo .closest()](https://developer.mozilla.org/en-US/docs/Web/API/Element/closest) y este es un usuario reclamando a **Babel** (un compilador de codigo que entre otras cosas traduce ES6 a ES5) que añadan una opcion por defecto que soporte este metodo [Link a petición de integración de metodo .closest() en babel](https://github.com/diverent2/SimplePlate/issues/11). La alternativa recae en los propios usuarios, o bien la comunidad desarolla un metodo para traducir metodos reveldes, o bien se optan por metodos o sintaxis mas compatibles. Yo personalmente, trato de solo usar metodos y sintaxis que por defecto funcionen en todos los navegadores (sin pensar en extremos como que funcione en Internet Explorer 9-6). Recientemente, Microsoft ha lanzado, y de alguna manera remplazado los navegadores Internet Explorer 11 por Edge, su nueva versión basada en el navegador open source **chromium**, lo que en principio deveria incrementar aun mas el numero de navegadores que son compatibles con ES6.

## A continuación documentare mi busqueda de información al respecto de ES5 y ES6: 

ES5 nace al rededor de 2009. ES6 nace en 2015. En ES6 una funcion requiere de las *keywords* `function` y `return`, en ES6 eso no es necesario, ejemplos de funciones ES5 y ES6.  
```
// ES5
function myFunction() {
    return "Hello world!";
}

// ES6
const myFunction = () => {
    return "Hello world!";
}
// tambien ES6
const myFunction = () => "Hello world"
```
En ES6 mientras la función sea reducida a una unica orden u operación (una linea de codigo) es posible no usar la *keyowrd* `return`.  
  
### Otras diferencias consiste en el uso de `const` y `let` respecto a `var`.  
Comenzaremos con un ejemplo de codigo y lo analizaremos.
```
// version con var

 var greeter = "hey hi";
    var times = 4;

if (times > 3) {
    var greeter = "say Hello instead"; 
}

console.log(greeter) // "say Hello instead"


// version con let

let greeting = "say Hi";
if (true) {
    let greeting = "say Hello instead";
    console.log(greeting); // "say Hello instead"
}
console.log(greeting); // "say Hi"
 ```
Al contrario que `var`, `let` esta contendia dentro de su bloque de `scope`, esto quiere decir que mientras este declarada dentro de un bloque (ya sea una función, condicional, loop, etc) su valor permanecera contenido y no afectara fuera del bloque o no se podra acceder. No entiendo muy bien por que pero al parecer esto es deseable en programación, entiendo que limitara el uso de memoria o recursos destinados a variables.  
  
`const` y `let`. `const` es una variable que no se redeclarara ni cambiara de valor, es obligatorio declararla con un valor o obtendremos un error, en principio esto permite una gestión de memoria mas efectiva. `let` es una variable que se redeclarara y/o que sera restringida a un bloque de codigo que he definido antes.  
  
#### Template literals  
Se mostrara como se concatenaban variables con strings en ES5 y ES6, en ES6 se introducen los `template literals`.  
```
// ES5
var miValor = 6;
var mivalor2 = 23;
var miConcatenación = "Ahora string, ahora valor1 " + miValor + " ahora string de nuevo ahora valor2 " + mivalor2;

// ES6 (template literals)
const miValor1 = 6;
const miValor2 = 23;
var miTemplateLiteral = `Ahora string, ahora valor1 ${miValor1} ahor string de nuevo ahora valor2 ${miValor2}`;
```
No solo se pueden mostrar variables sino que tambien se pueden llamar a funciones, hacer calculos, etc, p.e. `${3 + 2 }` o `${callFunction()}`.  
  
Tambien se introducen un monton de metodos para tratar con arrays y objetos asi como tipos de bucle para iterar objetos. Pondre un ejemplo, for of:  
```
let iterable = [10, 20, 30];

for (let value of iterable) {
  value += 1;
  console.log(value);
}
// 11
// 21
// 31
```
  
  
  
## Comparar cadenes
Com es comparen les cadenes de JavaScript. Hi ha sis maneres de comparar les cadenes a JavaScript.  Enumera'n i descriu amb algun exemple, almenys 5.

### Metodo tradicional con for loops  
```
const checkEqualStrings = () => {}
    const myStr = "Hello world!";
    const myStr2 = "Hello there!";
    const myLenght = myStr.length
    let i = 0;
    if (myLenght == myStr2.lenght) {
        for (; i < myLenght && myStr[i] == myStr2[i]; i++) {
        }
    }
    if (i == myLenght) {
        return "Les cadenes son iguals!";
    }
    return "Les cadenes son diferents!"
}
```
### Metodo facil y gracioso  
```
const myStr = "Hello world!";
const myStr2 = "Hello there!";
const myBoolean = myStr == myStr2;
```
### Metodo localeCompare  
```
"abc".localeCompare("abc")
//0
"abc".localeCompare("abs")
//-1
```
### Metodo regex  
```
const my1 = "abc";
const re = new RegExp("abc");
my1.match(re);
//["abc", index: 0, input: "abc", groups: undefined]


const re2 = new RegExp("abd");
my1.match(re2);
// null
```
## Metodo .replace  
```
const str = "Visit Microsoft!";
const res = str.replace("Microsoft", "");
const resultBoolean = res.length == 0;
console.log(resultBoolean);
// true


const str = "Microsoft";
const res = str.replace("MicrosoftABC", "");
const resultBoolean = res.length == 0;
console.log(resultBoolean);
// false
```
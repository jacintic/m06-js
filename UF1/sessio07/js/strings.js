/*
Exercicis

    1. Fes un programa que donat un número de DNI, calculi si la lletra és 
    correcta. Escriure primer l'algoritme en paper
    2. Fes un programa que determini si un password és segur. 
    3. Escriure primer l'algoritme en paper
    4. Fes un programa per validar el mail sigui correcte.
    5. Escriure primer l'algoritme en paper
    6. Escriviu un programa per realitzar la compressió bàsica de cadenes 
    utilitzant el recompte de caràcters repetits, per exemple, "aabcccccaaa" 
    es convertiria en "a2b1c5a3", si no hi ha repetitcions, només cal que 
    imprimiu l'original
    7. Crea un Captcha senzill amb JAVASCRIPT. Fes primer l'algoritme

*/
"use strict";

// 1. 
/*
Para obtener la letra, es necesario dividir el número del DNI entre 23 y en 
lugar de calcular los decimales, nos fijaremos en el resto que ofrece la 
solución. El resto siempre ofrecerá un valor que se encuentra entre 0 y 22, 
y este número determinará cuál es la letra correspondiente.
Resto 	Letra
0 	T
1 	R
2 	W
3 	A
4 	G
5 	M
6 	Y
7 	F
8 	P
9 	D
10 	X
11 	B
12 	N
13 	J
14 	Z
15 	S
16 	Q
17 	V
18 	H
19 	L
20 	C
21 	K
22 	E
*/

// declare DNI lookup table
const DNI_LOOKUP = ['T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'];
// get the full DNI
const dniIn = prompt("introdueix el teu DNI amb la lletra inclossa.");
// get only the letter
const dniLletraIn = dniIn[dniIn.length - 1];
// get only the numbers
const dniNumsIn = dniIn.slice(0,dniIn.length - 1);

// validate
// calcualte DNI pos from given DNI
const lookupPos = dniNumsIn % 23;
// see if it matches
console.log("Is DNI valid? " + ( DNI_LOOKUP[lookupPos] == dniLletraIn));


// 2.  password
/*
// define pass1
const myPass = "AaaaaaabCd4#.";
// define pass 2
const myPass2 = 'Abc1ddddda';
// check if they match the regex (include all needed characters)
const includesChars = /((([a-z])+([A-Z])+)+|(([A-Z])+([a-z])+)+)\d+([#|\"|\-|@|\.])+/g.test(myPass) && myPass.length > 9;
const doesntIncludeChars = /((([a-z])+([A-Z])+)+|(([A-Z])+([a-z])+)+)\d+([#|\"|\-|@|\.])+/g.test(myPass2) && myPass2.length > 9;
// show results
console.log(includesChars);
console.log(doesntIncludeChars);
*/
// 2. V2.0
const checkPass = (pass) => {
    // has upper and lower case letters
    const lowCas = /[a-z]/;
    const upperC = /[A-Z]/;
    const hasLowC = pass.search(lowCas) != -1;
    const hasCapC =  pass.search(upperC) != -1;
     // check if it contains numbers
    const nums = /\d/;
    const hasNum = pass.search(nums) != -1;
    // check if it contains  special characters
    const specChars = /[!#\$%_-\.]/;
    const hasSpChar = pass.search(specChars) != -1;
    // length greater or equal to 10
    const matchesLength =pass.length >= 10;
    // final boolean return (checks all cases)
    console.log(hasLowC , hasCapC , hasNum , hasSpChar , matchesLength);
    return hasLowC && hasCapC && hasNum && hasSpChar && matchesLength;
}
console.log("Pass checking, right. ",checkPass("Juncnusjuf1#"));
console.log("Pass checking, not long enough. ",checkPass("Jnusjf1#"));
console.log("Pass checking, lacks a capital letter. ",checkPass("jnussssssjf1#"));
console.log("Pass checking, lacks some low caps. ",checkPass("ABCDEFGHIJKLMNOPQ1#"));
console.log("Pass checking, lacks a number. ",checkPass("Juncnusjuf#"));
console.log("Pass checking, lacks a special character. ",checkPass("Juncnusjuf1a"));

// 4. validate email
// define emails
/*
const mail1 = 'correct@email.com';
const mail2 = 'incorrect.mail.com';
// formulate regex
// see if it is valid
const validateMail1 = /([a-z0-9]){3,15}@{1}([a-z0-9]){3,15}\.(com|org|es|cat|net)/g.test(mail1);
const validateMail2 = /([a-z0-9]){3,15}@{1}([a-z0-9]){3,15}\.(com|org|es|cat|net)/g.test(mail2);
console.log("Are mails valid?",validateMail1,validateMail2);
*/

// 4. V2.0

const mail1 = 'correct@email.com';
const mail2 = 'incorrect.mail.com';
const checkMail = (mail) => {
    // contains only one "@"
    const onlyOneAt = mail.split("@").length == 2;
    // captures the last "."+ some characters in case there are any
    const endInDomain = mail.split(".")[mail.split(".").length - 1];
    // gets the lenght of the last captured characters 
    const domain = endInDomain.length;
    // checks length is 2 or 3 (like .com or .es)
    const isDomain = domain == 2 || domain == 3;
    // initiate a string
    let resultMail = "This mail is ";
    // check all the conditions above
    if (!onlyOneAt || !isDomain) {
        resultMail +="in";
    }
    console.log(resultMail + "valid.");
}
checkMail(mail1);
checkMail(mail2);


/*
 6. Escriviu un programa per realitzar la compressió bàsica de cadenes 
    utilitzant el recompte de caràcters repetits, per exemple, "aabcccccaaa" 
    es convertiria en "a2b1c5a3", si no hi ha repetitcions, només cal que 
    imprimiu l'original
*/
console.time('#1');
const origStr = "aabcccccaaa";
let myResult2 = "";

for (let i = 0, isRepeating = false, myRepCount = 0; i <  origStr.length ; i++) {
    if (i <  (origStr.length - 1) && origStr[i] == origStr[i + 1]) {
        isRepeating = true;
        myRepCount++;
    } else if(isRepeating && i == (origStr.length - 1)){
        myRepCount++;
        myResult2 += origStr[i] + "" +  myRepCount;
        isRepeating = false;
        myRepCount = 0;
    } else if (isRepeating){
        myRepCount++;
        myResult2 += origStr[i] + "" +  myRepCount;
        isRepeating = false;
        myRepCount = 0;
    } else {
        myResult2 += origStr[i];
    }
}
console.log("String compress: " + myResult2);
console.timeEnd('#1');
console.time('#2');
let myResult = "";
for (let i = 1, isRepeating = false, myRepCount = 0; i <  origStr.length ; i++) {
    if (origStr[i] == origStr[i - 1] ){
        isRepeating = true;
        if (myRepCount == 0) {
            myRepCount++
        }
        myRepCount++;
        if (i == (origStr.length - 1) ) {myResult += origStr[i] + "" +  myRepCount;}
    } else if (isRepeating) {
        myResult += origStr[i] + "" +  myRepCount;
        isRepeating = false;
        myRepCount = 0;
    } else {
        myResult += origStr[i];
    }
}
console.log("String compress: " + myResult);
console.timeEnd('#2');


// 7.  create a captcha
// declare string with all the characters to be used in the captcha
const captChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789#@%$/()';
// random generator function
const myRandom = () => {
    return Math.floor(Math.random() * captChars.length );
}
const captchVal = "" + captChars[myRandom()] + captChars[myRandom()] + captChars[myRandom()];
// ask user for input and check if it matchs
const promptMsg = `Introduce this captcha: ${captchVal}`;
const check = prompt(promptMsg);
console.log("Match:", check == captchVal);
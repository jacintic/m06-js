"use strictt";
/*

Exercicis Iframes

    1. Fes una pàgina web amb un iframe de 500x500px. El seu color de fons haurà de 
    canviar de forma aleatòria cada segon.

    2. Fes un pàgina web que tingui un iframe amb l’etiqueta <title>. Accedeix des 
    del document principal a l’iframe i mostra per consola el valor de l’etiqueta 
    title de l’iframe.

    3. Fes un pàgina web que tingui un iframe. En apretar un botó, canvia el valor 
    de l’etiqueta <h1> de l’iframe i mostra-ho per pantalla.

    4. Fes un pàgina web que tingui un iframe. Canvia el <h1> del host a través de 
    l’iframe.

    5. Fes un pàgina web que tingui dos iframes amb l’etiqueta <title>. Cada iframe, 
    en apretar un botó, ha de mostrar l’etiqueta <title> de l’altra iframe.

*/
/*
  2. Fes un pàgina web que tingui un iframe amb l’etiqueta <title>. Accedeix des 
    del document principal a l’iframe i mostra per consola el valor de l’etiqueta 
    title de l’iframe.

*/



const ifr1r = () => {
    const myIframe = document.createElement("iframe");
    myIframe.height = 500;
    myIframe.width = 500;
    myIframe.id = "iframeId";
    myIframe.title = 'My title!';
    const myBody = document.getElementById("myBody");
    myBody.appendChild(myIframe);
    console.log(myIframe.title);
}

const ifr1 = document.getElementById("ifr1");
ifr1.addEventListener("click",ifr1r);
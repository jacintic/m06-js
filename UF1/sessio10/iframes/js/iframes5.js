"use strictt";
/*

Exercicis Iframes

    1. Fes una pàgina web amb un iframe de 500x500px. El seu color de fons haurà de 
    canviar de forma aleatòria cada segon.

    2. Fes un pàgina web que tingui un iframe amb l’etiqueta <title>. Accedeix des 
    del document principal a l’iframe i mostra per consola el valor de l’etiqueta 
    title de l’iframe.

    3. Fes un pàgina web que tingui un iframe. En apretar un botó, canvia el valor 
    de l’etiqueta <h1> de l’iframe i mostra-ho per pantalla.

    4. Fes un pàgina web que tingui un iframe. Canvia el <h1> del host a través de 
    l’iframe.

    5. Fes un pàgina web que tingui dos iframes amb l’etiqueta <title>. Cada iframe, 
    en apretar un botó, ha de mostrar l’etiqueta <title> de l’altra iframe.

*/
/*
   5. Fes un pàgina web que tingui dos iframes amb l’etiqueta <title>. Cada iframe, 
    en apretar un botó, ha de mostrar l’etiqueta <title> de l’altra iframe.

*/



// if onload is not used the content within the iframe can not be accessed
window.onload = function() {
    const switchCont = () => {
        // target each frame
        const myIframe1 = document.getElementById("myIframe1");
        const myIframe2 = document.getElementById("myIframe2");
        
        // get the h1 elements within each frame
        let if1 = myIframe1.contentWindow.document.getElementById("if1");
        let if2 = myIframe2.contentWindow.document.getElementById("if2");

        // switch inner text values between nodes by using ".textContent"
        const myTemp = if1.textContent;
        if1.textContent = if2.textContent;
        if2.textContent = myTemp;
    }
    const button = document.getElementById("switchCont");
    button.addEventListener("click",switchCont);
  };

  
"use strict";
/*
Exercicis Local Storage

Repeteix l’anterior activitat, però en comptes de fer 
servir galetes utilitza localStorage, amb els mètodes 
setItem i getItem. Fixeu-vos que la manera en què 
escriviu i llegiu els valors és més simple. A més, 
localStorage permet altres opcions, com aixecar 
events en resposta a un canvi de valor d’alguna 
variable.
*/

/*
2. Fes dues pàgines web. En la primera posareu un paràgraf. En la segona, 
un paràgraf diferent al primer. A cada pàgina també posareu dos botons:

    1. Un que canviarà el color del text a través d’una galeta.

    2. Un que canviarà la font del text a través d’una galeta.

A la primera pàgina els botons canviaran, respectivament, al color 
vermell i la font Arial. A la segona pàgina els botons canviaran, 
també respectivament, al color blau i la font Verdana. Quan es 
premi un d’aquests botons, la pàgina que el conté emmagatzemarà 
una galeta amb la informació del canvi realitzat i eliminarà la 
informació dels canvis anteriors del mateix tipus (color o font). 
D’aquesta manera, en carregar-se qualsevol de les dues pàgines, 
aquestes consultaran les galetes i aplicaran al seu text el darrer 
canvi realitzat (si n’hi ha) per alguna d’aquestes pàgines tant al 
color com a la font al seu text. I això amb independència de la 
pàgina que hagi gravat la galeta. 
*/
/*
1.
color 
vermell i la font Arial

2.
color blau i la font Verdana
*/



const insertItem = (mKey,mVal) => {
    localStorage.setItem(mKey, mVal);
}

const getItem = (mKey) => {
    return localStorage.getItem(mKey);
}
const clearStorage = () => {
    localStorage.clear();
}
const setStyle = () => {
    insertItem('color','red');
    insertItem('font','Arial');
    renderText();
}
const resetStyle = () => {
    clearStorage();
    renderText();
}
const myText = document.getElementById("myText");
// function that switches the colors and fonts
const renderText = () => {
    const myColor = getItem('color');
    const myFont = getItem('font');
    if (myColor) {
        myText.style.color = myColor;
        myText.style.fontFamily = myFont;
    } else {
        myText.style.color = "";
        myText.style.fontFamily = "";
    }
}
// button elements
const bSet = document.getElementById("buttonSet");
const bClear = document.getElementById("buttonClear")
// button event handlers
bSet.addEventListener("click",setStyle);
bClear.addEventListener("click",resetStyle);


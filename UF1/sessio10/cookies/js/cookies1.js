"use strict";
/*
Exercicis Galetes

1. Implementa, idealment sense utilitzar arrays, les següents funcions de tal 
manera que es puguin reutilitzar en futurs programes. Tingues present les 
propietats i mètodes dels objectes estudiats anteriorment per facilitar-te 
la feina.

    1. assignaGaleta(<nom>, <valor>, <expiració>): seteja els valors passats    
    com a paràmetre a la cookie.

    2. retornaGaleta(<nom>): retorna el valor de la galeta <nom>.

    3. esborraGaleta(<nom>): esborra la galeta <nom>.


2. Fes dues pàgines web. En la primera posareu un paràgraf. En la segona, 
un paràgraf diferent al primer. A cada pàgina també posareu dos botons:

    1. Un que canviarà el color del text a través d’una galeta.

    2. Un que canviarà la font del text a través d’una galeta.

A la primera pàgina els botons canviaran, respectivament, al color 
vermell i la font Arial. A la segona pàgina els botons canviaran, 
també respectivament, al color blau i la font Verdana. Quan es 
premi un d’aquests botons, la pàgina que el conté emmagatzemarà 
una galeta amb la informació del canvi realitzat i eliminarà la 
informació dels canvis anteriors del mateix tipus (color o font). 
D’aquesta manera, en carregar-se qualsevol de les dues pàgines, 
aquestes consultaran les galetes i aplicaran al seu text el darrer 
canvi realitzat (si n’hi ha) per alguna d’aquestes pàgines tant al 
color com a la font al seu text. I això amb independència de la 
pàgina que hagi gravat la galeta. 
*/



/*
1. Implementa, idealment sense utilitzar arrays, les següents funcions de tal 
manera que es puguin reutilitzar en futurs programes. Tingues present les 
propietats i mètodes dels objectes estudiats anteriorment per facilitar-te 
la feina.

    1. assignaGaleta(<nom>, <valor>, <expiració>): seteja els valors passats    
    com a paràmetre a la cookie.

    2. retornaGaleta(<nom>): retorna el valor de la galeta <nom>.

    3. esborraGaleta(<nom>): esborra la galeta <nom>.
*/
// write/create a new cookie
const assignaGaleta = (nom,valor,expiracio) => {
    document.cookie = `${nom}=${valor};${expiracio};Samesite=Lax;`;
}
// return the value of the key from the key of it
const retornaGaleta = (nom) => {
    const myCookie = document.cookie ;
    return myCookie.split(";")[0].split("=")[1];
}
assignaGaleta("nom","Jacint",new Date(2020,11,4).toUTCString());

console.log(retornaGaleta("nom"));

// delete the cookie via the expires key, giving it a date in the past
const esborraGaleta = (nom) => {
    const myDay = new Date().getDate() - 1;
    const myMonth = new Date().getMonth() - 1;
    const myYear = new Date().getFullYear() - 1;
    document.cookie = `${nom}=_;expires=${new Date(myYear,myMonth,myDay).toUTCString()};Samesite=Lax;`;
}
esborraGaleta("nom");
console.log(retornaGaleta("nom"));
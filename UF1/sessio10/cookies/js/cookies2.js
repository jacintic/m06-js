"use strict";
/*
Exercicis Galetes

1. Implementa, idealment sense utilitzar arrays, les següents funcions de tal 
manera que es puguin reutilitzar en futurs programes. Tingues present les 
propietats i mètodes dels objectes estudiats anteriorment per facilitar-te 
la feina.

    1. assignaGaleta(<nom>, <valor>, <expiració>): seteja els valors passats    
    com a paràmetre a la cookie.

    2. retornaGaleta(<nom>): retorna el valor de la galeta <nom>.

    3. esborraGaleta(<nom>): esborra la galeta <nom>.


2. Fes dues pàgines web. En la primera posareu un paràgraf. En la segona, 
un paràgraf diferent al primer. A cada pàgina també posareu dos botons:

    1. Un que canviarà el color del text a través d’una galeta.

    2. Un que canviarà la font del text a través d’una galeta.

A la primera pàgina els botons canviaran, respectivament, al color 
vermell i la font Arial. A la segona pàgina els botons canviaran, 
també respectivament, al color blau i la font Verdana. Quan es 
premi un d’aquests botons, la pàgina que el conté emmagatzemarà 
una galeta amb la informació del canvi realitzat i eliminarà la 
informació dels canvis anteriors del mateix tipus (color o font). 
D’aquesta manera, en carregar-se qualsevol de les dues pàgines, 
aquestes consultaran les galetes i aplicaran al seu text el darrer 
canvi realitzat (si n’hi ha) per alguna d’aquestes pàgines tant al 
color com a la font al seu text. I això amb independència de la 
pàgina que hagi gravat la galeta. 
*/



/*
2. Fes dues pàgines web. En la primera posareu un paràgraf. En la segona, 
un paràgraf diferent al primer. A cada pàgina també posareu dos botons:

    1. Un que canviarà el color del text a través d’una galeta.

    2. Un que canviarà la font del text a través d’una galeta.

A la primera pàgina els botons canviaran, respectivament, al color 
vermell i la font Arial. A la segona pàgina els botons canviaran, 
també respectivament, al color blau i la font Verdana. Quan es 
premi un d’aquests botons, la pàgina que el conté emmagatzemarà 
una galeta amb la informació del canvi realitzat i eliminarà la 
informació dels canvis anteriors del mateix tipus (color o font). 
D’aquesta manera, en carregar-se qualsevol de les dues pàgines, 
aquestes consultaran les galetes i aplicaran al seu text el darrer 
canvi realitzat (si n’hi ha) per alguna d’aquestes pàgines tant al 
color com a la font al seu text. I això amb independència de la 
pàgina que hagi gravat la galeta. 
*/
/*
1.
color 
vermell i la font Arial

2.
color blau i la font Verdana
*/
// on load function for the pages (will also be called on cookie set or reset to re-render the text's style)
const onLoad1 = (reset) => {
        const myCookie = document.cookie ;
        let myFont;
        const myText = document.getElementById("myText");
        if (myCookie.indexOf("color1") != -1) {
            const myColor =  myCookie.split(";")[0].split("=")[1];
            const myFont =  myCookie.split(";")[1].split("=")[1];
            
            myText.style.color = myColor;
            myText.style.fontFamily = myFont;
        } else if (reset) {
            myText.style.color = "black";
            myText.style.fontFamily = "Times New Roman";
        }
}
// on load function for the pages (will also be called on cookie set or reset to re-render the text's style)
const onLoad2 = (reset) => {
    const myCookie = document.cookie ;
    let myFont;
    const myText = document.getElementById("myText");
    if (myCookie.indexOf("color2") != -1) {
        const myColor =  myCookie.split(";")[2].split("=")[1];
        const myFont =  myCookie.split(";")[3].split("=")[1];
        
        myText.style.color = myColor;
        myText.style.fontFamily = myFont;
    } else if (reset) {
        myText.style.color = "black";
        myText.style.fontFamily = "Times New Roman";
    }
   
}
// call on page load
onLoad1();
onLoad2();
// functions called on click, set the cookie and re-render the text's style via onLoad1/2() function
const assignaGaleta1 = () => {
    document.cookie = `color1=red;Samesite=Strict;`;
    document.cookie = `font1=Arial;Samesite=Strict;`;
    onLoad1();
}
const assignaGaleta2 = () => {
    document.cookie = `color2=blue;Samesite=Strict;`;
    document.cookie = `font2=Verdana;Samesite=Strict;`;
    onLoad2();

}
// deletes the cookies and call the style render to reset the text style
const reSet1 = () => {
    const myDay = new Date().getDate() - 1;
    const myMonth = new Date().getMonth() - 1;
    const myYear = new Date().getFullYear() - 1;
    document.cookie = `color1=_;expires=${new Date(myYear,myMonth,myDay).toUTCString()};Samesite=Lax;`;
    document.cookie = `color2=_;expires=${new Date(myYear,myMonth,myDay).toUTCString()};Samesite=Lax;`;
    document.cookie = `font1=_;expires=${new Date(myYear,myMonth,myDay).toUTCString()};Samesite=Lax;`;
    document.cookie = `font2=_;expires=${new Date(myYear,myMonth,myDay).toUTCString()};Samesite=Lax;`;
    onLoad1(true);
    onLoad2(true);
}
// declare the button elements
const cambiaColorButt1 = document.getElementById("button211");
const cambiaFontButt2 = document.getElementById("button221");
const myReset = document.getElementById("reSet");
// clicl handlers for each button (have extra check for if button exists since each button is in a different page)
if (cambiaColorButt1 != null) {
    cambiaColorButt1.addEventListener("click",assignaGaleta1);
}
if (cambiaFontButt2 != null) {
    cambiaFontButt2.addEventListener("click",assignaGaleta2);
}
myReset.addEventListener("click",reSet1);
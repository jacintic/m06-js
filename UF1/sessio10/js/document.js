"use strictt";
/*

Exercicis Document

    1. Fes un programa que obri 5 finestres en apretar un botó a la finestra pare. 
    En el moment de generar les finestres a la finestra pare, utilitza 
    document.write()per a generar per a cadascuna de les finestres fill:

        1. El número de finestra que representa

        2. Un botó per a tancar la finestra

    3. Fes un programa que en apretar un botó obri una finestra, i des de la 
    finestra pare es generi un formulari.



*/
/*
1. Fes un programa que obri 5 finestres en apretar un botó a la finestra pare. 
    En el moment de generar les finestres a la finestra pare, utilitza 
    document.write()per a generar per a cadascuna de les finestres fill:

        1. El número de finestra que representa

        2. Un botó per a tancar la finestra
*/

const open5 = () => {
    const NUM_TIMES_WINDOW = 5;
    for (let i = 0; i < NUM_TIMES_WINDOW; i++) {
        myWindow = window.open('', '_blank', 'location=yes,height=300,width=200,scrollbars=yes,status=yes');
        myWindow.document.write(`<h1>${i+1}</h1><input type="submit" value="X" onclick="window.close()">`);
    }
}
const win5 = document.getElementById("win5");
win5.addEventListener("click",open5);

/*
 3. Fes un programa que en apretar un botó obri una finestra, i des de la 
    finestra pare es generi un formulari.

*/
const mod1 = () => {
    winMod = window.open('', '_blank', 'location=yes,height=300,width=200,scrollbars=yes,status=yes');
    const myForm = '<form><input type="text"><input type="submit" value="enviar"></form>'
    winMod.document.write(myForm);
}

const win1 = document.getElementById("win1");
win1.addEventListener("click",mod1);

"use strict";
/*
Exercicis Navigator
1.
    Fes un programa que saludi a l’usuari en l’idioma predefinit del navegador i li 
    informi del sistema operatiu i navegador utilitzats. 

*/

//1.
const sayHi = () => {
    // get browser's language
    const lang = navigator.languages[1];
    let cheer = "Hello";
    switch (lang) {
        case "en":
            break;
        case "ca":
            cheer = "Bon dia!";
            break;
        case "es":
            cheer = "Buenos dias!";
            break;
        default:
            // klingon
            cheer = "nuqneH ";
            break;
    }
    return cheer;
}
console.log(sayHi());
//2.
/*
Amplia l’anterior programa indicant si la combinació de sistema operatiu i navegador 
està suportada, o no.
*/
const checkSupport = () => {
    const usAg = navigator.userAgent;
    const noSupp = {
        windowsOS : ['Trident','MSIE','OPR','Opera','Seamonkey'],
        macOS : false,
        linuxOS : ['Trident','MSIE'],
    };
    if (usAg.search("Win")) {
        for (const browserEl in noSupp.windowsOS) {
           if ( usAg.search(noSupp.windowsOS[browserEl]) != -1 ) {
               //console.log("Windows " + noSupp.windowsOS[browserEl] + "not supported");
               return false;
           }
        }
        //console.log("Browser and OS supported");
        return true;
    } else if  ( usAg.search("Mac") ) {
        return noSupp.macOS;
    } else if ( usAg.search("Linux") ) {
        for (const browserEl in noSupp.linuxOS) {
            if (usAg.search(noSupp.linuxOS[browserEl]) != -1 ) {
                //console.log("Linux " + noSupp.macOS[browserEl] + "not supported");
                return false;
            }
         }
        //console.log("Browser and OS supported");
        return true;
    } else {
        return true;
    }
    return true;
   
}
// call check browser support
checkSupport();

   
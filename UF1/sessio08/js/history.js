"use strict";
/*
Exercicis Navigator
1.
   Exercicis History
Implementa la funcionalitats dels botons endarrera, endavant i recarrega.
*/

//1.
const buttBack = document.getElementById("backB");
const buttRelo = document.getElementById("reloadB");
const buttForw = document.getElementById("forwardB");

function clickBack() {
    history.back();
}

const clickRel = () => {
    history.go(0);
}

const clickFor = () => {
    history.forward();
}



buttBack.addEventListener('click', clickBack);
buttRelo.addEventListener('click', clickRel);
buttForw.addEventListener('click', clickFor);
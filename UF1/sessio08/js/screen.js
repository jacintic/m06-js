"use strict";
/*
Exercicis Navigator
1.
   Exercicis Screen
Fes un programa que quan detecti que la mida de la finestra és 
menor al 50% de la mida total disponible per a finestres, salti un missatge.
*/

//1.

const screenWidthOrigin = window.innerWidth;
const triggerAt = 0.5;

const checkWidth = () => {
    console.log(window.innerWidth,screenWidthOrigin);
    if (window.innerWidth <= (screenWidthOrigin * triggerAt)) {
        alert("You've downscaled the window " + (triggerAt * 100) + "% or further");
    }
}

window.addEventListener('resize', checkWidth);
"use strict";
/*
  Exercicis Location
1. Fes un programa que en apretar un botó carregui una nova 
pàgina web -la que vulguis- al mateix document conservant l’historial.

2. Modifica l’anterior codi per a que no conservi l’historial.

//1. (primer boto)
//2. (segon boto)

*/

const goPageNoHis = document.getElementById("buttNoHis");

const goNoHis = () => {
    location.replace("https://www.google.com");
}

goPageNoHis.addEventListener("click", goNoHis);


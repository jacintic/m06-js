"use strictt";
/*


1. Obre una finestra amb l’adreça www.escoladeltreball.org al navegador. 
La finestra s’ha de mostrar a una pestanya diferent a la del document on 
escrius el codi.

2. Obre una finestra amb l’adreça www.escoladeltreball.org al navegador. 
La finestra s’ha de mostrar a una pestanya diferent a la del document on 
escrius el codi i s’ha de poder tancar des de l’opener.

3.  Obre una finestra buida o amb l'adreça www.escoladeltreball.org. 
Aquesta finestra ha de tenir una amplada de 300 píxels i una altura de 
200 píxels. Mou la finestra aleatòriament un número finit de vegades i 
finalment, tanca-la.

4.  Obre una finestra que no disposi de barra d’adreces ni de barra d’eines.

5.  Obre una finestra nova amb les següents característiques: amplada 800 
píxels, altura 500 píxels, que no es pugui modificar la mida de la finestra 
un cop oberta.

        Nota: Amb L’estàndard actual de JavaScript ja no és possible crear 
        una finestra que no sigui ‘resizable’, a no ser que s’utilitzi 
        Internet Explorer.

6.  Crea un document que pregunti a l’usuari si desitja obrir una nova 
finestra i actuï conseqüentment.

7.  Fes un programa que obri una nova finestra. Aquest nova finestra s’ha 
de moure aleatòriament cada tres segons i la seva mida ha d’anar oscil·lant. 
Als moviments parells ha de modificar l’altura i als senars l’amplada. 
Opcionalment, a cada moviment canviarà el color del fons i mostrarà el 
contador de moviments.

8.  Modifica l’anterior programa per a poder aturar els moviments.

*/
/*
1. Obre una finestra amb l’adreça www.escoladeltreball.org al navegador. 
La finestra s’ha de mostrar a una pestanya diferent a la del document on 
escrius el codi.
*/
const openNewTab = () => {
    window.open('https://www.escoladeltreball.org','_blank');
}
document.getElementById("nwindow").addEventListener("click",openNewTab);




/*
2. Obre una finestra amb l’adreça www.escoladeltreball.org al navegador. 
La finestra s’ha de mostrar a una pestanya diferent a la del document on 
escrius el codi i s’ha de poder tancar des de l’opener.
*/

const winClose = document.getElementById("nwindowClose");
const openNewTabClose = () => {
    
    if (winClose.value == 'Obrir nova finestra 2') {
        nWindClose = window.open('https://www.escoladeltreball.org','_blank','location=yes,height=300,width=200,status=yes');
        winClose.value = 'Tancar finestra';
    } else {
        nWindClose.close();
        winClose.value = 'Obrir nova finestra 2';
    }    
}
winClose.addEventListener("click",openNewTabClose);


/*
3.  Obre una finestra buida o amb l'adreça www.escoladeltreball.org. 
Aquesta finestra ha de tenir una amplada de 300 píxels i una altura de 
200 píxels. Mou la finestra aleatòriament un número finit de vegades i 
finalment, tanca-la.
*/
const openMoveWindow = () => {
    let myWindow = window.open('', '_blank', 'location=yes,height=300,width=200,scrollbars=yes,status=yes');
    myWindow.focus();
    setTimeout(() => {
        myWindow.moveTo(300,300);
    }, 1500);
    setTimeout(() => {
        myWindow.moveTo(0,0);
    }, 1500);
    setTimeout(() => {
        myWindow.moveTo(500,500);
    }, 1500);
}

const winMove = document.getElementById("nwinmove");
winMove.addEventListener("click",openMoveWindow);




/*
6.  Crea un document que pregunti a l’usuari si desitja obrir una nova 
finestra i actuï conseqüentment.
*/

const newWinChoi = confirm("Wanna open a new tab? ");
if (newWinChoi) {
    window.open('https://www.google.com/','_blank', 'location=yes').focus();
}

/*
7.  Fes un programa que obri una nova finestra. Aquest nova finestra s’ha 
de moure aleatòriament cada tres segons i la seva mida ha d’anar oscil·lant. 
Als moviments parells ha de modificar l’altura i als senars l’amplada. 
Opcionalment, a cada moviment canviarà el color del fons i mostrarà el 
contador de moviments.
*/

const moveRandomArr = () => {
    const posX = Math.floor(Math.random() * 1921);
    const posY = Math.floor(Math.random() * 1081);
    const xyArr = [posX,posY];
    return xyArr;
}
let isOdd = true;
const lastSize = {
    x : 300,
    y : 200,
}
const winRandom = () => {
    if (isOdd) {
        myWindow.focus();
        const xPosition = moveRandomArr()[0]
        lastSize.x = xPosition;
        myWindow.resizeTo(lastSize.x,lastSize.y);
        myWindow.moveTo(moveRandomArr()[0],moveRandomArr()[1]);
        isOdd = false;
        console.log(lastSize.x,lastSize.y);
    } else {
        myWindow.focus();
        lastSize.y = moveRandomArr()[1];
        myWindow.resizeTo(lastSize.x,lastSize.y);
        myWindow.moveTo(moveRandomArr()[0],moveRandomArr()[1]);
        isOdd = true;
        console.log(lastSize.x,lastSize.y);
    }
}
const startRandom = () => {
    myWindow = window.open('', '_blank', 'location=yes,height=300,width=200,scrollbars=yes,status=yes');
    setInterval(() => {
        winRandom();
    }, 3000);
}


const winRandomEl = document.getElementById("moveRandom");
winRandomEl.addEventListener("click",startRandom);

/*
8.  Modifica l’anterior programa per a poder aturar els moviments.

*/

const moveWindow = () => {
    if (isOdd) {
        myWindow2.focus();
        const xPosition = moveRandomArr()[0]
        lastSize.x = xPosition;
        myWindow2.resizeTo(lastSize.x,lastSize.y);
        myWindow2.moveTo(moveRandomArr()[0],moveRandomArr()[1]);
        isOdd = false;
    } else {
        myWindow2.focus();
        lastSize.y = moveRandomArr()[1];
        myWindow2.resizeTo(lastSize.x,lastSize.y);
        myWindow2.moveTo(moveRandomArr()[0],moveRandomArr()[1]);
        isOdd = true;
    }
}

let firstTime = true;
let timerId;

const startRandom2 = (reset) => {
    // nomes crear una nova finestra el primer cop
    if (firstTime) {
        myWindow2 = window.open('', '_blank', 'location=yes,height=300,width=200,scrollbars=yes,status=yes');
        firstTime = false;
    }
    // moure la finestra periodicament, declarar el nom de l'interval (per poderlo resetejar des de l'altra funció)
    timerId = setInterval(() => moveWindow(), 3000);
 }
 // resetejar la funció de moure
 const stopRandom2 = () => {
     // crida a la funció per que es pari
    clearInterval(timerId);
 }
// 
 const winStop = document.getElementById("stopRandom");
 winStop.addEventListener("click",stopRandom2);

const winRandomEl2 = document.getElementById("moveRandom2");
winRandomEl2.addEventListener("click",startRandom2);


"use strict";
// prints top and bottom rows
const printTopBottomRow = (num) => {
    let myResult = "";
    let i = 0;
    while (i < num) {
        myResult += "*";
        i++;
    }
    return myResult + "\n";
}
const printInnerRows = (num) => {
    let myResult = "";
    let i = 0;
    while (i < num) {
        if (i == 0 || i == (num -1) ) {
            myResult += "*";
        } else {
            myResult += " ";
        }
        i++;
    }
    return myResult + "\n";
}
// prints asterisks
const printAsterisks = (num) => {
    let result = "";
    for (let i = 0; i < num; i++) {
        if (i == 0 || i == (num -1) ) {
            result += printTopBottomRow(num);
        } else {
            result += printInnerRows(num);
        }
    }
    document.getElementById("asteriskSquare").innerText = result;
}
// check if number is an integer
const isAnInteger = (num) => {
    let myNum =  parseInt(num);
    myNum = isNaN(myNum) ? false : true; 
    return myNum;
}
// gets number of asterisks, calls print asterisks function
const getNumOfAsterisks = () => {
    const numberOfAsterisks = document.getElementById("number").value;
    if (!isAnInteger(numberOfAsterisks)) {
        document.getElementById("asteriskSquare").innerText = 'Error: insert a valid integer';
        return;
    }
    printAsterisks(numberOfAsterisks);
}
// handles click on button, calls get time which in turn will start time
document.getElementById("starter").addEventListener("click", getNumOfAsterisks);
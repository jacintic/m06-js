"use strict";
// function check if value is a number (integer)
const isNum = (num) => {
    let myNum =  parseInt(num);
    myNum = isNaN(isNum) ? false : true; 
    return isNum;
}
// start counter
const startCounter = (num) => {
        // loop to manage the value the counter every 1000 miliseconds
        setInterval(function(){
                // leave the function when counter is = 0
                if (num == 0) {return;}
                    document.getElementById("counter").innerText = num;
                    num--;
                 
            }, 1000);
    
}
// get time from input
const getTime = () => {
    const timerValue = document.getElementById("number").value;  
    if (!isNum(timerValue)) {
        document.getElementById("counter").innerText = "Error: input is not a valid integer Number!";
        return;
    } 
    // start the counter
    startCounter(timerValue); 
}
// handles click on button, calls get time which in turn will start time
document.getElementById("starter").addEventListener("click", getTime);

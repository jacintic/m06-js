"use strict";
// print multiply table
const printTable = (num) => {
    const myTable = document.getElementById("multiplyTable");
    let result = "";
    for (let i = 0; i <= 10; i++) {
        result += i + " * " + num + " = " + (i * num) + "\n";
    }
    myTable.innerText = result;
}
// check if number is an integer
const isAnInteger = (num) => {
    let myNum =  parseInt(num);
    myNum = isNaN(myNum) ? false : true; 
    return myNum;
}
// gets number of asterisks, calls print asterisks function
const getNum = () => {
    const myNumber = document.getElementById("number").value;
    if (!isAnInteger(myNumber)) {
        document.getElementById("multiplyTable").innerText = 'Error: insert a valid integer';
        return;
    }
    printTable(myNumber);
}
// handles click on button, calls get number
document.getElementById("starter").addEventListener("click", getNum);
"use strict";
/*
    Escribir un programa donde el usuario elija entre lo siguiente:
    Pulse 1 si desea cenar Costilla
    Pulse 2 si desea cenar Pescado
    Y luego se le pregunte también si desea o no postre
    Usar las funciones: prompt y confirm para pedir los datos
     Si eligió cenar Costilla->pago 23 €
     Si eligió cenar Pescado->pago 15 €
     Si tomo postre->el postre valió 3 €
    Calcular el valor total de la cena dependiendo de las opciones que elija por teclado el usuario, incluyendo una propina del 6%.

*/
// constant variables 
const myBill = {
    totalBill : 0,
    myElement : 1,
}
// adds the price of the dish to the bill
const addToBill = (ammount) => {
    myBill.totalBill += parseInt(ammount)
    document.getElementById('bill').innerText = myBill.totalBill;
}
// key handler function (redirects to each subfunction)
/* Notes:
myBill.myElement will act as event control, it will make sure bill is being added only for each element of the prompt 
(1 = costillas, 2 = pescado, 3 = postre)
*/
const keyHandler = (event) => {
    const myKey = event.key;
    switch (myKey) {
        case '1':
            if (myBill.myElement == 1) {addToBill(23);}
          break;
        case '2':
            if (myBill.myElement == 2) {addToBill(15);}        
            break;
        case '3':
            if (myBill.myElement == 3) {addToBill(3);}       
          break;
        default:
          //random key out of range, do nothing
          return;
          break;
      }
    const myBody = document.getElementsByTagName('body')[0];
    const myDiv = document.getElementsByTagName('div')[0]; 
    myBody.removeChild(myDiv);
    myBill.myElement += 1;
    myPopups(myBill.myElement);
    
}
// listen to documment for keys pressed
document.addEventListener('keydown',keyHandler);
// popup manager displays the questions
const myPopups = (num) => {
    const myPopupWindow = document.createElement("div");
    let alertText = ''; 
    switch (num) {
        case 1:
            alertText = 'Si quieres costilla pulsa 1';
          break;
        case 2:
            alertText = 'Si quieres pescado pulsa 2';
            break;
        case 3:
            alertText = 'Si quieres postre pulsa 3';  
          break;
        default:
          //random key out of range, do nothing
          return;
          break;
      }
    const myText = document.createTextNode(alertText);
    const myBody = document.getElementsByTagName('body')[0];
    myPopupWindow.appendChild(myText);
    myBody.appendChild(myPopupWindow);
}
myPopups(1);

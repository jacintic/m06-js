"use strict";
// get sentence
const sentence = prompt("Introduce a sentence");
// reverse words function
const reverseWords = () => {
    // split in words, separator " "
    const arrayWords = sentence.split(" ");
    let i = arrayWords.length - 1;
    let wordsRevStr = "";
    while (i >= 0) {
        // start adding words to the string result starting by last word iterating backwards
        wordsRevStr += arrayWords[i] + " ";
        i--;
    }
    console.log(wordsRevStr);
}
reverseWords();
// reverse letters function
const reverseLetters = () => {
    let i = sentence.length - 1;
    let strResult = "";
    while (i >= 0) {
        strResult += sentence[i];
        i--;
    }
    console.log(strResult);
}
reverseLetters();
/*
    Es tracta de fer un programa en JavaScript que mostri per consola l’evolució d’un joc anomenat “El ratolí i el gat”. 
    Aquest joc, disposa d’una pista circular amb 60 caselles que van de la casella 0 a la 59. El ratolí comença a la casella 29 i el gat comença a la 0. 
    El ratolí i el gat tiren alternativament un dau. Comença tirant un dau de sis cares el ratolí. El ratolí avança tantes caselles com indica el dau. 
    El gat retrocedeix el número de caselles que indica el dau multiplicades per dos. El joc s’acaba quan el gat agafa al ratolí (guanya el gat) o quan 
    el ratolí agafa al gat (guanya el ratolí). Agafar significa que ambdós són a la mateixa casella.

A més, cal mostrar per consola l'evolució del joc, exemple
Ratolí: He tret un 4. Vaig a la casella 33.
Gat: He tret un 1. Vaig a la casella 58.
…
Ratolí: He tret un 1. Vaig a la casella 40.
Gat: He tret un 6. Vaig a la casela 40.
Gat: He guanyat!

Cal que m'entregueu en paper: 

- L'algorisme i la implementació del càlcul de posició del ratolí.
- L'algorisme i la implementació del càlcul de posició del gat.
- L'algorisme i la implementació de finalització del joc.

 OJO, fem servir l'ojecte Math.random() per la tirada del dau. Aquestes funcions les veurem en la propera sessió.
*/
// global variables 
"use strict";
const catGlobals = {
  NUMBER_OF_GAME_CELLS : 60,
  NUMBER_OF_SIDES_OF_DICE : 6,
  ratPos : 29,
  catPos : 0,
  diceValueRat : 0,
  diceValueCat : 0,
}
// function to check if the win condition has been met
const checkWinCondition = () => {
  return catGlobals.ratPos == catGlobals.catPos;
}
// move Cat function
const moveCat = () => {
  // cat move formula: (((position - move) % board) + board ) % board
  const moveRaw = catGlobals.catPos - (catGlobals.diceValueCat * 2) ;
  const moveNegativeCorrected = catGlobals.NUMBER_OF_GAME_CELLS + moveRaw;
  catGlobals.catPos = moveRaw >= 0 ? moveRaw : moveNegativeCorrected;
  console.log(`Cat: I've scored a ${catGlobals.diceValueCat * 2}. Now I go to the position ${catGlobals.catPos}`);
  if (checkWinCondition()) {
    console.log("Cat: I've won!");
    return;
  }
  throwDice();
}
// move Rat function
const moveRat = () => {
  catGlobals.ratPos = (catGlobals.ratPos + catGlobals.diceValueRat) % catGlobals.NUMBER_OF_GAME_CELLS;
  console.log(`Rat: I've scored a ${catGlobals.diceValueRat}. Now I go to the position ${catGlobals.ratPos}`);
  if (checkWinCondition()) {
    console.log("Rat: I've won!")
    return;
  }
  moveCat();  
}
// throw dice function, sets value of dice thrown
const throwDice = () => {
  catGlobals.diceValueRat =  Math.floor(Math.random() * catGlobals.NUMBER_OF_SIDES_OF_DICE ) + 1;
  catGlobals.diceValueCat =  Math.floor(Math.random() * catGlobals.NUMBER_OF_SIDES_OF_DICE ) + 1;
  moveRat();
}
throwDice();

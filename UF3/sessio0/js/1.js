"use strict";
/**
 * Sessió 09-10: Creació i ús de components en Vue 2. E 05 Exemples Basics

Avui programarem elements bàsics que ens podrem trobar en qualsevol pàgina web.

Farem una versió molt senzilla i personalitzada, per veure i entendre com funciona Vue, seguint els següents exemples:

    Construirem un Slide.
    Farem transicions.
    Progamarem filtres bàsics.
    Endreçarem llistes.
    Construirem una barra de navegació
    Inclourem un AJAX.
    Faram un acordió amb un arbre de continguts.

Veure exemples GIT

Investiga, selecciona e implementa 3 exemples fent servir element bàsics de Vue ( sense components ). Pensa en el teu projecte. Que podries implementar?

    Free Style 1
    Free Style 2
    Free Style 3

 */
new Vue({
    el: '#demo',
    data: {
      back: false,
      currentIndex: 0,
      nextVisible : true,
      prevVisible : false,
    },
    methods: {
      next(){
        this.back = false;
        this.currentIndex++;
        // shows and hides prev and next button according to possition in slideshow
        this.currentIndex > 0 ? this.prevVisible = true : this.prevVisible = false;
        this.currentIndex > 1 ? this.nextVisible = false : this.nextVisible = true;
      },
      prev(){
        this.back = true;
        this.currentIndex--;
        // same as above
        this.currentIndex > 0 ? this.prevVisible = true : this.prevVisible = false;
        this.currentIndex > 1 ? this.nextVisible = false : this.nextVisible = true;
      }
    },
  })

////// MY TRANSITIONS //////
new Vue({
    el: "#myTransitions",
    data: {
        noActivated: false,
        x : 0
    },
    methods: {
        xCoordinate(e) {
            this.x = e.clientX
        }
    }
});


  /*
  TRANSITIONS
  
  */
new Vue({
    el: "#div-9",
    data: {
        mostrar: true
    },
    methods: {
        show: function() {
            this.mostrar = !this.mostrar;
        }
    }
});
const vm = new Vue({
    el: '#app',
    methods: {
        createTask() {
            this.tasks.push({
                description: this.new_task,
                pending: true,
                editing: false
            });

            this.new_task = '';
        },
        toggleStatus(task) {
            task.pending = !task.pending;
        },
        editTask(task) {
            this.tasks.forEach(taskEl => taskEl.editing = false);
            
            this.draft = task.description;
            
            task.editing = true;
        },
        updateTask(task) {
            task.description = this.draft;

            task.editing = false;
        },
        discardTask(task) {
            task.editing = false;
        },
        deleteTask(index) {
            this.tasks.splice(index,1);
        },
        deleteCompleted() {
            this.tasks = this.tasks.filter(taskEl => taskEl.pending);
        }
    },
    data: {
        draft : '',
        new_task : '',
        tasks: [
            {
                description: 'Completa a CRUD app with Vue',
                pending: true,
                editing: false,
            },
            {
                description: 'Task being edited',
                pending: true,
                editing: false,
            },
            {
                description: 'Completed task',
                pending: false,
                editing: false,
            }
        ]
    }
});
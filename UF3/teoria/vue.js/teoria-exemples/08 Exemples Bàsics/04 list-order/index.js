let list = [
    "Michael Gary Scott",
      "Dwight Schrute",
      "Jim Halpert",
      "Pam Beesly-Halpert",
      "Darryl Philbin",
      "Erin Hannon",
      "Andy Bernard",
      "Phyllis Lapin-Vance",
      "Stanley Hudson",
  ]
  
  let vm = new Vue({
      data: {
          list: [],
          sortOrder: "Popularity",
      },
      created: function(){
          // simulating asynchronous loading of data
          setTimeout(() => {
              this.list = list;
          }, 1000)
      },
      computed: {
          orderedListOptions: function(){
              return {
                  "Popularity": () => { return this.list },
                  "A-Z": () => { return this.list.slice().sort() },
                  "Z-A": () => { return this.list.slice().sort().reverse()},
              }
          },
      },
      methods: {
          sort: function(sortOrder){
              return this.orderedListOptions[sortOrder]()
          }
      },
      el: "#app"
  })
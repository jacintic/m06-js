const sortable = new Vue({
    el: '#navbar',
    data: {
        selected : 0,
        items: [
            {
                name: 'Home',
                selected: true,
            },
            {
                name: 'Projects',
                selected: false,
            },
            {
                name: 'About us',
                selected: false,
            },
            {
                name: 'Contact',
                selected: false,
            },
        ]
    },
    methods: {
    },
})
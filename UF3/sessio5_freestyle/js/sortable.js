const sortable = new Vue({
    el: '#sortApp',
    data: {
        items: [
            {
                id : 4,
                title: 'First item',
                body: `P`
            },
            {
                id : 2,
                title: 'Second item',
                body: `R`
            },
            {
                id : 1,
                title: 'Third item',
                body: `F`
            },
            {
                id : 3,
                title: 'Fourth  item',
                body: `A`
            }
        ]
    },
    methods: {
        sortById: () => {
            sortable.items = sortable.items.sort((a,b) => a.id - b.id);
        },
        sortByTitle: () => {
            sortable.items = sortable.items.sort((a,b) => {
                if (a.title < b.title) {
                    return -1;
                }
                if (a.title > b.title) {
                    return 1;
                }
                  // title must be equal
                return 0;
            });
        },
        sortByBody: () => {
            sortable.items = sortable.items.sort((a,b) => {
                if (a.body < b.body) {
                    return -1;
                }
                if (a.body > b.body) {
                    return 1;
                }
                  // body must be equal
                return 0;
            });
        },
    },
})
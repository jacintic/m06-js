
var EventBus = new Vue;

// icons
Vue.component('app-icon', {
    template: '<span class= "glyphicon" :class="cssClasses" aria-hidden="true"></span>',
    props: ['img'],
    computed: {
        cssClasses() {
            return `glyphicon-${this.img}`;
        }
    }
});

// list item
Vue.component('app-task', {
    data : function() {
        return {
            editing : false,
            draft : '',
            draft_bod: '',
        }
    }, 
    template: '#task-template',
    props: ['task', 'index'],
    created() {
        EventBus.$on('editing', (index) => {
            if (this.index != index) {
                this.discard();
            }
        });
    },
    methods: {
        toggleStatus() {
            this.task.pending = !this.task.pending;
        },
        edit() {
            EventBus.$emit('editing', this.index);
            this.draft = this.task.title;
            this.draft_bod = this.task.news_bod;
            
            this.editing = true;
        },
        update() {
            this.task.title = this.draft;
            this.task.news_bod = this.draft_bod;

            this.editing = false;
        },
        discard() {
            this.editing = false;
        },
        remove() {
            this.$emit('remove', this.index);
        },
    }
});

const vm = new Vue({
    el: '#app',
    methods: {
        createTask() {
            this.tasks.push({
                title: this.new_title,
                news_bod: this.new_bod,
                pending: true,
                editing: false
            });

            this.new_title = '';
        },
        deleteTask(index) {
            this.tasks.splice(index, 1);
        },
        deleteCompleted() {
            this.tasks = this.tasks.filter(taskEl => taskEl.pending);
        }
    },
    data: {
        new_title : '',
        new_bod : '',
        tasks: [
            {
                title: 'Student completed the CRUD App tutorial in Vue.',
                news_bod: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
                pending: true,
            },
            {
                title: 'Student completed the CRUD App with components with Vue.',
                news_bod: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
                pending: true,
            },
            {
                title: 'A student is working on making the CRUD App a News portal.',
                news_bod: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
                pending: false,
            }
        ]
    }
});
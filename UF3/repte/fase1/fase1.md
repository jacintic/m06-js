# Fase 1  
  
## Treball en grup o individual?  
Individual.  
  
## Analitzar i definir funcionalitats.  
  
Funcionalitats basiques que crec que ha de tenir:  
* lectura i escritura de JSON (simulant una API)  
* gestió privilegis d'usuaris definits al JSON (usuari standard, professor, mod, admin)
  
Functionalitats que m'agradarien (per expandir):
* gestió de comentaris a un article (aprovar, eliminar, denegar access a comentaris a usuari => temporalment o definitivament)
* integració amb Laravel
  
## Entorn git (link):  
(Link a repo de git)[https://gitlab.com/iaw14270791/m06_repte_vuecli]  
  
## Descripció de backend:  
* Inicial: un sol JSON amb el que interactuarem  
* Inicial+: multiple JSONs
* Expandint: REST API with JSONs
* Desired: Laravel API  


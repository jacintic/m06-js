///////////////////////////////////////
// The Spread Operator (...)
//Spread operator allows an iterable to expand in places where 0+ arguments are expected. 
//It is mostly used in the variable array where there is more than 1 values are expected. 
//It allows us the privilege to obtain a list of parameters from an array. 
//Syntax of Spread operator is same as Rest parameter but it works completely opposite of it.
'use strict';

const weekdays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],
  openingHours: {
    [weekdays[3]]: {
      open: 12,
      close: 22,
    },
    [weekdays[4]]: {
      open: 11,
      close: 23,
    },
    [weekdays[5]]: {
      open: 0, // Open 24 hours
      close: 24,
    },
  },

  order(starterIndex, mainIndex) {
    return [this.starterMenu[starterIndex], this.mainMenu[mainIndex]];
  },

  orderDelivery({ starterIndex = 1, mainIndex = 0, time = '20:00', address }) {
    console.log(
      `Order received! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]} will be delivered to ${address} at ${time}`
    );
  },

  orderPasta(ing1, ing2, ing3) {
    console.log(
      `Here is your declicious pasta with ${ing1}, ${ing2} and ${ing3}`
    );
  },

  orderPizza(mainIngredient, ...otherIngredients) {
    console.log(mainIngredient);
    console.log(otherIngredients);
  },
};


//les següent instruccions són les mateixes.
console.log("a. manera tradicional");
const arr = [7, 8, 9];
const badNewArr = [1, 2, arr[0], arr[1], arr[2]];

console.log(badNewArr);

console.log("b. amb l'spreed operator");
const newArr = [1, 2, ...arr];
console.log(newArr);

console.log("Per si volem destructurar l'array");
console.log(...newArr);
console.log(1, 2, 7, 8, 9);


console.log("el new menu li afegirem gnocis");
const newMenu = [...restaurant.mainMenu, 'Gnocci'];
console.log(newMenu);

// Copy array
const mainMenuCopy1 = [...restaurant.mainMenu];
const mainMenuCopy2 = [restaurant.mainMenu];
console.log("mainMenuCopy1");
console.log(mainMenuCopy1);
console.log("mainMenuCopy2");
console.log(mainMenuCopy2);

// Join 2 arrays, mireu la diferencia
const menu1 = [restaurant.starterMenu, restaurant.mainMenu];
const menu2 = [...restaurant.starterMenu, ...restaurant.mainMenu];
console.log("juntem starters + mainmenu: menu1");
console.log(menu1);
console.log("juntem starters + mainmenu: menu2");
console.log(menu2);
console.log(...menu2);

console.log("exemple amb strings");
// Iterables: arrays, strings, maps, sets. NOT objects
const str = 'Jonas';
const letters = [...str, ' ', 'S.'];
console.log(letters);
console.log(...str);

// Fem una copia del Objecte restaurant, afegint noves propietats
console.log("New restaurant");
const newRestaurant = { foundedIn: 1998, ...restaurant, founder: 'Guiseppe' };
console.log(restaurant);
console.log(newRestaurant);

// Fem una copia del Objecte exacte del restaurant
const restaurantCopy = { ...restaurant };
restaurantCopy.name = 'Ristorante Roma';
console.log(restaurantCopy.name);
console.log(restaurant.name);

// Real-world example
const ingredients = [
  // prompt("Let's make pasta! Ingredient 1?"),
  // prompt('Ingredient 2?'),
  // prompt('Ingredient 3'),
];
console.log(ingredients);

restaurant.orderPasta(ingredients[0], ingredients[1], ingredients[2]);
restaurant.orderPasta(...ingredients);
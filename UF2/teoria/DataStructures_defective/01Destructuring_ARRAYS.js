///////////////////////////////////////
// Destructuring Arrays
// Destructuring in JavaScript is a simplified method of extracting multiple properties from an array by taking the structure and deconstructing 
// it down into its own constituent parts through assignments by using a syntax that looks similar to array literals.
// It creates a pattern that describes the kind of value you are expecting and makes the assignment. Array destructuring uses position.

const arr = [2, 3, 4];
const a = arr[0];
const b = arr[1];
const c = arr[2];

// això engloba les 4 instruccios anteriors.
const [x, y, z] = arr;
console.log(x, y, z);
console.log(arr);

// Anem a veure un exemple
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],  
  order(a,b) {
    myOrder = ['fogger', 'wigger','digger', 'higger'];
    return [myOrder[a], myOrder[b]];
  }, 
};


let [main, , secondary] = restaurant.categories;
console.log(main, secondary);

// Switching variables
// const temp = main;
// main = secondary;
// secondary = temp;
// console.log(main, secondary);

[main, secondary] = [secondary, main];
console.log(main, secondary);

// Receive 2 return values from a function
const [starter, mainCourse] = restaurant.order(2, 0);
console.log(starter, mainCourse);

// Nested destructuring
const nested = [2, 4, [5, 6]];
// const [i, , j] = nested;
const [i, , [j, k]] = nested;
console.log(i, j, k);

// Default values
const [p = 1, q = 1, r = 1] = [8, 9];
console.log(p, q, r);
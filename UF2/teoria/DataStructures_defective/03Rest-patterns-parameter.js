///////////////////////////////////////
// Rest Pattern and Parameters
// Rest parameter is an improved way to handle function parameter, allowing us to more easily handle various input as parameters in a function. 
// The rest parameter syntax allows us to represent an indefinite number of arguments as an array. 
// With the help of a rest parameter a function can be called with any number of arguments, no matter how it was defined. 
// Rest parameter is added in ES2015 or ES6 which improved the ability to handle parameter.


// 1) Destructuring
// SPREAD, because on RIGHT side of =
const restaurant = {
  name: 'Classico Italiano',
  location: 'Via Angelo Tavanti 23, Firenze, Italy',
  categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
  starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
  mainMenu: ['Pizza', 'Pasta', 'Risotto'],  
  order(a,b) {
    myOrder = ['fogger', 'wigger','digger', 'higger'];
    return [myOrder[a], myOrder[b]];
  }, 
};


const weekdays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
const openingHours2 = {
  [weekdays[3]]: {
    open: 12,
    close: 22,
  },
  [weekdays[4]]: {
    open: 11,
    close: 23,
  },
  [weekdays[5]]: {
    open: 0, // Open 24 hours
    close: 24,
  },
}; 


const arr = [1, 2, ...[3, 4]];

// REST, because on LEFT side of =
const [a, b, ...others] = [1, 2, 3, 4, 5];
console.log(a, b, others);
const ñ = [a,b, ...others];
console.log(ñ)

const [pizza, , risotto, ...otherFood] = [
  ...restaurant.mainMenu,
  ...restaurant.starterMenu,
];
console.log('hello! ', pizza, risotto, otherFood);

// Objects
const { sat, ...weekdays } = openingHours2;
console.log(weekdays);


// 2) Functions
const add = function (...numbers) {
  let sum = 0;
  for (let i = 0; i < numbers.length; i++) sum += numbers[i];
  console.log(sum);
};

add(2, 3);
add(5, 3, 7, 2);
add(8, 2, 5, 3, 2, 1, 4);

const x = [23, 5, 7];
add(...x);

restaurant.orderPizza('mushrooms', 'onion', 'olives', 'spinach');
restaurant.orderPizza('mushrooms');

"use strict";
/**
 * Problema 1
Fes un programa en Javascript que satisfaci els següents requeriments: 

(0 punts) Crea un botó anomenat ”Exercici 1” que iniciarà la resolució del problema 1. 

(2 punts) funció “inicia1()”:

Demana el número de jugadors per pantalla i genera un “array” amb tantes posicions com jugadors.

A partir de l’array anterior, genera un array del array. Amb una dimensió aleatòria de 0 a 10.

La longitud correspon als cops que pot tirar el dau, que oscil·len entre 0 i 10 per a cada jugador.

Mostra el resultat a l’usuari, inicialitzant totes les possibles tirades de cada jugador a “null”:

Exemple per a 6 jugadors:

[null, nul, null]

[ ]

[null, null, null]

[null, null, null, null, null]

[null, null]

[null, null, null, null, null, null, null, null, 

null, null]

*[null,..., null] array de longitud aleatòria entre 0 i 10. L’array del jugador 0, tirarà 3 vegades, i el del jugador 1, 0 vegades. 

(2 punts) funció “tirarDaus1()”:

Cada jugador tirarà un icosàedre (dau de 20 cares) i guardarà el valor de la tirada a l’array bidimensional.

Repetirà aquesta operació tants cops com pugui.

Mostra el resultat a l’usuari:

Exemple (continuació):

[3, 20, 7]

[ ]

[19, 18, 16]

[1, 6, 20, 4, 1]

[3, 1]

[1, 2, 1, 5, 7, 3, 2, 1, 

19, 20]

 

(2 punts) funció “resultatFinal1()”:

Mostra a l’usuari el número de punts totals acumulats i el número de tirades realitzades.

Mostra a l’usuari el jugador que ha guanyat, és a dir, el jugador que ha aconseguit més punts.

Exemple:
…

El jugador 0 ha acumulat 30 punts en 3 tirades.

El jugador 1 ha acumulat 0 punts en 0 tirades.

El jugador 2 ha acumulat 43 punts en 3 tirades.

El jugador 3 ha acumulat 32 punts en 4 tirades.

El jugador 4 ha acumulat 4 punts en 2 tirades.

El jugador 5 ha acumulat 61 punts en 10 tirades.
*/

//////////////////////////
//////// GLOBALS ////////
//////////////////////////
// button element
const button_ = document.getElementById("myButton");
// players array
let players = [];
// max of dice throws per player
const NUM_THROWS = 10;
// num of faces dice
const NUM_DICE = 20;

////// THROW DICE FUNCTION //////
const throwDice = () => {
    return Math.floor(Math.random() * 20 + 1);
}

///////// GAME FUNCTION ///////
const game = () => {
    for (let i= 0; i < players.length; i++) {
        for (let j = 0; j < players[i].length; j++) {
            players[i][j] = throwDice();
        }
        console.log(`Player ${i + 1} has accumulated ${players[i].length > 0 ? players[i].reduce((a,b)=> a + b) : 0} punts en ${players[i].length} tirades`);
    }
}
//////// CLICK FUNCTION ////////
const inicia1 = () => {
   // ask for number of players
    const numOfPlayersArr = prompt("Type the number of players in the game.");
    const numOfPlayers = parseInt(numOfPlayersArr);
    players = Array(numOfPlayers);
    for (let i = 0; i < numOfPlayers; i++) {
        const randomRounds = Math.floor(Math.random() * NUM_THROWS);
        players[i] = Array(randomRounds)
        for (let j = 0; j < randomRounds; j++) {
            players[i][j] = null;
        }
    }
    console.log("Initialized array:");
    console.log(players);
    game();
}



//////// CLICK EVENT ////////
button_.addEventListener('click',inicia1);
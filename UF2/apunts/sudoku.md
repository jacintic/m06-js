# M06 JS.  
  
## Sudoku teoria.  
  
### Tres condicions a comprovar:  
* suma fila
* suma columna
* suma recuadre
  
  
## OOP  

```
// instantiation

Class => Instance



// prototypal inheritance/ delegation

Object 		=>	 Prototype
  ^			    ^
can access		contains methods
methods
```  
  
### Array.prototype.map  
Array.prototype => contains all the methods of the Array object.  
The array inherits/delegates the .map method.  
  
### Prototype?  
* Constructor functions
* ES6 Classes
* Object.create()
  
ES6 are just abstraction over Constructor functions, use Constructor functions in the background.  
Object.create() THe easiest and most straightforward way of linking an object to a prototype object.  
  
### Constructor functions  
```
// arrow functions don't work as constructor, they lack *this* pointer and it is needed

const Person = function(firstName, birthYear) {
	//console.log(this);
	this.firstName = firstName;
	this.birthYear = birthYear;
	
	// never create a method inside a constructor
	//this.calcAge = function() {
	//	console.log("hi");
	//}
}


const jonas =const jonas = new Person('Jonas',1991);


console.log(jonas)

console.log(jonas instanceof Person);// true

// 1. New {} is created
// 2. function is called, this = ()
// 3. {} linked to a prototype
// 4. object automatically return {}
```
  
  
## Prototypes  
```
// arrow functions don't work as constructor, they lack *this* pointer and it is needed

const Person = function(firstName, birthYear) {
	//console.log(this);
	this.firstName = firstName;
	this.birthYear = birthYear;
	
	// never create a method inside a constructor
	//this.calcAge = function() {
	//	console.log("hi");
	//}
}


const jonas =const jonas = new Person('Jonas',1991);


console.log(jonas)

console.log(jonas instanceof Person);// true

// 1. New {} is created
// 2. function is called, this = ()
// 3. {} linked to a prototype
// 4. object automatically return {}

console.lgo(Person.prototype) // it carries the calcAge method

// this adds a method to the Class Person that can be called from every instance
// it is a single method and 
Person.prototype.calcAge = function () {
	console.log("age");
}

console.log(jonas.__proto__);
console.log(jonas.__proto__ === Person.prototype);// true

console.log(Person.prototype.isPrototypeOf(jonas));// true
console.log(Person.prototype.isPrototypeOf(Person));// false

Person.prototype.species = 'Homo Sapiens'; // adds this attribute to every instance
console.log(jonas.species);//'Homo Sapiens'
console.log(jonas.hasOwnProperty('species'))// false => cause it was not created inside the object but added
```


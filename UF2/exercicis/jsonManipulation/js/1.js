"use strict";
/*
challenge
Let's continue with our football betting app!

1. Loop over the game.scored array and print each player name to 
the console, along with the goal number (Example: "Goal 1: Lewandowski")
2. Use a loop to calculate the average odd and log it to the 
console. 
3. Print the 3 odds to the console, but in a nice formatted way, 
exaclty like this:
      Odd of victory Bayern Munich: 1.33
      Odd of draw: 3.25
      Odd of victory Borrussia Dortmund: 6.5
Get the team names directly from the game object, don't hardcode 
them (except for "draw"). HINT: Note how the odds and the game 
objects have the same property names 😉


GOOD LUCK 😀
*/
// object/Json to manipulate
const game = {
    team1: 'Bayern Munich',
    team2: 'Borrussia Dortmund',
    players: [
      [
        'Neuer',
        'Pavard',
        'Martinez',
        'Alaba',
        'Davies',
        'Kimmich',
        'Goretzka',
        'Coman',
        'Muller',
        'Gnarby',
        'Lewandowski',
      ],
      [
        'Burki',
        'Schulz',
        'Hummels',
        'Akanji',
        'Hakimi',
        'Weigl',
        'Witsel',
        'Hazard',
        'Brandt',
        'Sancho',
        'Gotze',
      ],
    ],
    score: '4:0',
    scored: ['Lewandowski', 'Gnarby', 'Lewandowski', 'Hummels'],
    date: 'Nov 9th, 2037',
    odds: {
      team1: 1.33,
      x: 3.25,
      team2: 6.5,
    },
  };
  /*
1. Loop over the game.scored array and print each player name to 
the console, along with the goal number (Example: "Goal 1: Lewandowski")
*/
// store the index => goal
// store the value of the player => player
// add 1 to the index to obtain the human readable index
// iterate through every entry of the games.scored array
for (const [goal,player] of game.scored.entries()) {
    console.log(`Goal ${goal + 1} scored by ${player}.`);
}
/*
2. Use a loop to calculate the average odd and log it to the 
console. 
*/
// get only the values of the odds since we only need those 
const [...oddsVal] = Object.values(game.odds);
// use reduce to obtain the sum of all elements
// use odds.length to make the average operation
console.log("Average odds is: " + (oddsVal.reduce((acc,el)=> acc += el) / oddsVal.length));
/*
3. Print the 3 odds to the console, but in a nice formatted way, 
exaclty like this:
      Odd of victory Bayern Munich: 1.33
      Odd of draw: 3.25
      Odd of victory Borrussia Dortmund: 6.5
Get the team names directly from the game object, don't hardcode 
them (except for "draw"). HINT: Note how the odds and the game 
objects have the same property names 😉
*/
// grabb the keys of game.odds
// with the keys alone we can access the values using:
// object[key] syntax
for (const key in game.odds) {
    // declare the string we'll use to build 
    // the result with the part that is common to all results
    let strBuild = 'Odd of ';
    // if the key doesn't match a draw
    // we add victory and name of the team
    // with game[key] we access the team's name
    if (key != 'x') {
        strBuild += `victory ${game[key]}`;
    // it's a draw so we announce a draw
    } else {
        strBuild += 'draw'
    }
    // we finally add to the string the name of the odd
    strBuild += `: ${game.odds[key]}`;
    // we log the full string
    console.log(strBuild); // Odd of victory Bayern Munich: 1.33 1.js:112:13 
                           // Odd of draw: 3.25 
}
"use strict";
/*



3. Amb el mètode “sort” i “slice” fes un programa que mostri els tres números més grans emmagatzemats a un array.

4. Amb el mètode “splice”, fes un programa en Javascript que tingui implementada la següent funció:

    1. Entrada: dos arrays, un amb valors numèric i un amb valors alfabètics

    2. Sortida: retorna un array amb els tres valors numèrics més grans i els tres elements alfabètics més baixos.

    3. Exemple:

        1. Array numèric: [1,5,8,999,43,-1]

        2. Array alfabètic: [“Java”,”hola”,”zebra”,”il·lusió”,”Al·lèrgic”,”cap-i-cua”]

        3. Sortida: [8,43,999,”Il·lusió”,”Java”,”zebra”]

*/
/*
4. Amb el mètode “splice”, fes un programa en Javascript que tingui implementada la 
següent funció:

    1. Entrada: dos arrays, un amb valors numèric i un amb valors alfabètics

    2. Sortida: retorna un array amb els tres valors numèrics més grans i els 
    tres elements alfabètics més baixos.

    3. Exemple:

        1. Array numèric: [1,5,8,999,43,-1]

        2. Array alfabètic: [“Java”,”hola”,”zebra”,”il·lusió”,”Al·lèrgic”,”cap-i-cua”]

        3. Sortida: [8,43,999,”Il·lusió”,”Java”,”zebra”]
*/
// define arrays
let myArray2sort = [1,233,54,27,8,6,21312,642,123];
let myArray2sort2 = ["pera","Mandarina","kiwi","banana", "Plàtan"];
// define arrays where the sorted version will be saved
let myArray2sortSorted = [];
let myArray2sort2Sorted = [];
// sort numeric array and store it in the target array
myArray2sortSorted = myArray2sort.sort((a, b) => a - b);
// sort alphabetic array and store it in the target array
myArray2sort2Sorted = myArray2sort2.sort(function (a, b) {
    return a.toLowerCase().localeCompare(b.toLowerCase());
});

// result final array
let resultArr = [];
// use method concat and slice to fill the result array
resultArr = myArray2sortSorted.slice(myArray2sortSorted.length - 3,myArray2sortSorted.length).concat(myArray2sort2Sorted.slice(myArray2sort2Sorted.length - 3,myArray2sort2Sorted.length));
// print the array
console.log(resultArr);



// declare result 2 array
let resultArr2 = [];
// use unshift ... arrname and push ... arrname methods to fill the array
// unshift... => places the items in the begining
// push ... => places the items at the end
resultArr2.unshift(...myArray2sortSorted.slice(myArray2sortSorted.length - 3,myArray2sortSorted.length));
resultArr2.push(...myArray2sort2Sorted.slice(myArray2sort2Sorted.length - 3,myArray2sort2Sorted.length));

console.log(resultArr2);
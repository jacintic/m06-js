"use strict";
/*



3. Amb el mètode “sort” i “slice” fes un programa que mostri els tres números més grans emmagatzemats a un array.

4. Amb el mètode “splice”, fes un programa en Javascript que tingui implementada la següent funció:

    1. Entrada: dos arrays, un amb valors numèric i un amb valors alfabètics

    2. Sortida: retorna un array amb els tres valors numèrics més grans i els tres elements alfabètics més baixos.

    3. Exemple:

        1. Array numèric: [1,5,8,999,43,-1]

        2. Array alfabètic: [“Java”,”hola”,”zebra”,”il·lusió”,”Al·lèrgic”,”cap-i-cua”]

        3. Sortida: [8,43,999,”Il·lusió”,”Java”,”zebra”]

*/
/*
3. Amb el mètode “sort” i “slice” fes un programa que mostri els tres 
números més grans emmagatzemats a un array.
*/

let myArray2sort = [1,233,54,27,8,6,21312,642,123];

myArray2sort.sort((a, b) => a - b);
console.log(myArray2sort.slice(myArray2sort.length - 3,myArray2sort.length));
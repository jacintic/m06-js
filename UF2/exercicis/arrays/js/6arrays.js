"use strict";
/*



5. Amb els mètodes “shift”, “unshift”, “push” i “pop”, fes un programa que passi l’array [“A”,”B”,”C”,”D”,”E”] a [“A”,”B”,”C”, ”E”,”D”]. Utilitza un únic array.

6. Amb l’ajuda dels mètodes “shift”, “unshift”, “push” i “pop”, fes una funció que implementi la funcionalitat del mètode “reverse”. Utilitza un únic array.

7. Amb el mètode “forEach”, fes un programa que reprodueixi el següent comportament fins que l’usuari enti un caràcter:

    1. demani un número a l’usuari, 

    2. el guardi en un array

    3. comprovi si aquest número és el més gran o el més petit de l’array,

    4. si és el més petit, resta aquest valor a tots els elements 

    5. si és el més gran, suma aquest valor a tots els elements

    6. informa per pantalla que és el més gran o el més petit, fins el moment

*/
/*
5. Amb els mètodes “shift”, “unshift”, “push” i “pop”, fes un programa que passi 
l’array [“A”,”B”,”C”,”D”,”E”] a [“A”,”B”,”C”, ”E”,”D”]. Utilitza un únic array.

*/
let originalArray = ["A","B","C","D","E"];
let myE = originalArray.pop();
let myD = originalArray.pop();
originalArray.push(myE);
originalArray.push(myD);
console.log(originalArray);
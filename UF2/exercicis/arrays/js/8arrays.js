"use strict";
/*



5. Amb els mètodes “shift”, “unshift”, “push” i “pop”, fes un programa que passi l’array [“A”,”B”,”C”,”D”,”E”] a [“A”,”B”,”C”, ”E”,”D”]. Utilitza un únic array.


7. Amb el mètode “forEach”, fes un programa que reprodueixi el següent comportament fins que l’usuari enti un caràcter:

    1. demani un número a l’usuari, 

    2. el guardi en un array

    3. comprovi si aquest número és el més gran o el més petit de l’array,

    4. si és el més petit, resta aquest valor a tots els elements 

    5. si és el més gran, suma aquest valor a tots els elements

    6. informa per pantalla que és el més gran o el més petit, fins el moment

*/
/*
7. Amb el mètode “forEach”, fes un programa que reprodueixi el següent comportament fins que l’usuari enti un caràcter:

    1. demani un número a l’usuari, 

    2. el guardi en un array

    3. comprovi si aquest número és el més gran o el més petit de l’array,

    4. si és el més petit, resta aquest valor a tots els elements 

    5. si és el més gran, suma aquest valor a tots els elements

    6. informa per pantalla que és el més gran o el més petit, fins el moment

*/

let myArr = [];
let myPrompt = parseFloat(prompt("Introdueix un numero. introdueix '3.14' per sortir."));
while(myPrompt != 3.14) {
    const isBigger = myArr[myArr.length - 1] < myPrompt;
    const isSmaller = myArr[0] > myPrompt; 
    myArr.push(parseFloat(myPrompt));
    myArr.sort((a,b) => a - b); 
    if (isBigger) {
        myArr.forEach((el, i) => myArr[i] = el + myPrompt);
        console.log(`The number ${myPrompt} is the biggest array`);
    } else if (isSmaller){
        myArr.forEach(function(el, i) {
            myPrompt = myPrompt < 0 ? (myPrompt * -1) : myPrompt;
            myArr[i] = el - myPrompt;
          });
        
        
        
        console.log(`The number ${myPrompt} is the smallest in the array`);
    }
    console.log(myArr);
    myPrompt = parseFloat(prompt("Introdueix un numero. introdueix '3.14' per sortir."));
}
console.log("has sortit del programa, aquest es el resultat de l'array: " + myArr);
"use strict";
/*

9. Amb el mètode “map”, fes un programa que donat un array amb elements de tipus string, 
retorni un nou array on cada nou element emmagatzema la primera i última lletra de cada 
element de l’array original.

*/

const origArr = ["cat","dog","giraffe","monkey","bat"];
const resArr = origArr.map(el => el.substr(0,1) + el.substr(el.length - 1));
console.log(resArr);
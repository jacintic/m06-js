"use strict";
/*



5. Amb els mètodes “shift”, “unshift”, “push” i “pop”, fes un programa que passi l’array [“A”,”B”,”C”,”D”,”E”] a [“A”,”B”,”C”, ”E”,”D”]. Utilitza un únic array.


7. Amb el mètode “forEach”, fes un programa que reprodueixi el següent comportament fins que l’usuari enti un caràcter:

    1. demani un número a l’usuari, 

    2. el guardi en un array

    3. comprovi si aquest número és el més gran o el més petit de l’array,

    4. si és el més petit, resta aquest valor a tots els elements 

    5. si és el més gran, suma aquest valor a tots els elements

    6. informa per pantalla que és el més gran o el més petit, fins el moment

*/
/*
6. Amb l’ajuda dels mètodes “shift”, “unshift”, “push” i “pop”, fes una funció que 
implementi la funcionalitat del mètode “reverse”. Utilitza un únic array.

*/
let originalArray = ["A","B","C","D","E"];

const  reverseArr = (arr) => {
    //store the original length of the array, for oprimization and to keep track 
    //of the positions of the original items through the iterations
    const arrLength = arr.length;
    // iterate from last position to the first one (the array will grow on length)
    for (let i = arrLength; i > 0; --i) {
        // add the last element to the end of array, later iterations will count only
        // from the original last backwards to the begining
        arr.push(arr[i-1]);
    }
    // remove original positions of the array
    for (let i = 0; i < arrLength; i++) {
        arr.shift();
    }
    console.log(arr);
}
reverseArr(originalArray);
"use strict";
/*

12. Amb el mètode indexOf, modifica l’anterior programa per a que determini pel cas “altre”,
 quins índexs d’elements són null o undefined.

13. Amb el mètode “reduce”, fes un programa on donat un array que emmagatzema per cada element 
el total de despeses diàries durant una setmana:

        calculi el total de despeses de la primera setmana,

        estimi el total de despeses de la segona setmana, tenint en compte que cada dia de 
        la segona setmana es té previst gastar el mateix que cada dia de la primera setmana, 
        però multiplicat pel factor de correcció del número de dia, sabent que dilluns 
        correspon a 0 i diumenge a 6 (per exemple, dilluns = dilluns setmana anterior x 0).

        estimi el total de despeses de la tercera setmana, tenint en compte que cada dia de 
        la tercera setmana es té previst gastar: el sumatori del total de la primera setmana, 
        i l’acumulat de la segona setmana fins el dia de la setmana en què es calculi (per 
            exemple, si es calcula la despesa per dimarts = total primera setmana + despeses 
            dilluns segona setmana + despeses dimarts segona setmana).

        estimi el total de despeses de la cuarta setmana, tenint en compte que el dilluns 
        gastarà el sumatori de tots els dilluns anterior, el dimarts el sumatori de tots 
        els dimarts i així successivament

*/

/*
11. Amb els mètodes “every” i “some”, fes un programa on donat un array, determini si tots 
els seus elements son de tipus number, string, o altre.

*/


const origArr = [1,2,3,4,5];
const origArr2 = ["1","2","3","4","5"];
const origArr3 = ["1",1,"2",2,"3","4","5"];


const isNum = (currentValue) => !isNaN(currentValue * -1);
const isNotNum = (currentValue) => isNaN(currentValue * -1);

console.log(origArr.every(isNum));
console.log(origArr2.every(isNotNum));
console.log(origArr3.some(isNum));

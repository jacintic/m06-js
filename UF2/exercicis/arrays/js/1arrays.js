"use strict";
/*

1. Amb el mètode “sort”, ordena un array numèric de més petit a més gran.

2. Amb el mètode “sort”, ordena alfabèticament un array sense que 
diferenciï entre lletres majúscules i minúscules.

    1. Exemple:

       1. entrada: [“pera”,”Mandarina”,”kiwi”,”banana”, “Plàtan”]

        2. sortida: [”banana”, ”kiwi”,“pera”,“Plàtan”,”Mandarina”]

*/
/*
1. Amb el mètode “sort”, ordena un array numèric de més petit a més gran.

*/
let myArray2sort = [2,54,7,3,21,3421,3421,4,21,7];
myArray2sort.sort((a, b) => a - b);
console.log(myArray2sort);
"use strict";
/*

1. Amb el mètode “sort”, ordena un array numèric de més petit a més gran.

2. Amb el mètode “sort”, ordena alfabèticament un array sense que 
diferenciï entre lletres majúscules i minúscules.

    1. Exemple:

       1. entrada: [“pera”,”Mandarina”,”kiwi”,”banana”, “Plàtan”]

        2. sortida: [”banana”, ”kiwi”,“pera”,“Plàtan”,”Mandarina”]

*/
/*
2. Amb el mètode “sort”, ordena alfabèticament un array sense que 
diferenciï entre lletres majúscules i minúscules.

    1. Exemple:

       1. entrada: [“pera”,”Mandarina”,”kiwi”,”banana”, “Plàtan”]

        2. sortida: [”banana”, ”kiwi”,“pera”,“Plàtan”,”Mandarina”]

*/
let myArray2sort = ["pera","Mandarina","kiwi","banana", "Plàtan"];

myArray2sort.sort(function (a, b) {
    return a.toLowerCase().localeCompare(b.toLowerCase());
  });
console.log(myArray2sort);
"use strict";
/*
13. Amb el mètode “reduce”, fes un programa on donat un array que emmagatzema per cada element 
el total de despeses diàries durant una setmana:

        1. calculi el total de despeses de la primera setmana,

        2. estimi el total de despeses de la segona setmana, tenint en compte que cada dia de 
        la segona setmana es té previst gastar el mateix que cada dia de la primera setmana, 
        però multiplicat pel factor de correcció del número de dia, sabent que dilluns 
        correspon a 0 i diumenge a 6 (per exemple, dilluns = dilluns setmana anterior x 0).

        3. estimi el total de despeses de la tercera setmana, tenint en compte que cada dia de 
        la tercera setmana es té previst gastar: el sumatori del total de la primera setmana, 
        i l’acumulat de la segona setmana fins el dia de la setmana en què es calculi (per 
            exemple, si es calcula la despesa per dimarts = total primera setmana + despeses 
            dilluns segona setmana + despeses dimarts segona setmana).

        4. estimi el total de despeses de la cuarta setmana, tenint en compte que el dilluns 
        gastarà el sumatori de tots els dilluns anterior, el dimarts el sumatori de tots 
        els dimarts i així successivament

*/
const depseses = [25,35,15,10,5,12,35];

//1.
const setmana1 = depseses.reduce((total, el) => total + el);
console.log("despeses setmana 1: " + setmana1 + "€");


//2. 
const setmana2 = depseses.reduce((total, el, myIndex) => total + (el * myIndex));
console.log("despeses setmana 2: " + setmana2 + "€");

//3.

const myReducer = (total,currentValue, myIndex) => { 
    total += setmana1;
    for (let i = 0; i < myIndex; i++) {
        total += currentValue * myIndex;
    }
    return total;
};

const setmana3 = depseses.reduce(myReducer);
console.log("despeses setmana 3: " + setmana3 + "€");

//4. 
const myReducer2 = (total,currentValue, myIndex) => { 
    total += currentValue; 
    total += setmana1;
    for (let i = 0; i < myIndex; i++) {
        total += currentValue * myIndex;
    }
    return total;
};

const setmana4 = depseses.reduce(myReducer2);
console.log("despeses setmana 3: " + setmana4 + "€");

"use strict";
/*
1. Demana cinc paraules a l’usuari i mostra-les per pantalla o consola.
*/
/*
2. Genera una sopa de lletres amb 20 files i 20 columnes que inclogui les paraules que s’han introduït a l’apartat anterior. Cada paraula s’ha de mostrar d’una manera determinada:

    1. La primera paraula s’ha de poder llegir d’esquerra a dreta. 

    2. La segona paraula s’ha de poder llegir de dreta a esquerra.

    3. La tercera paraula s’ha de poder llegir de dalt a baix.

    4. La quarta paraula s’ha de poder llegir de baix a dalt.

    5. La cinquena paraula s’ha de poder llegir en diagonal, d’esquerra a dreta i de dalt a baix.

    6. La sisena paraula s’ha de poder llegir en diagonal, de dreta a esquerra i de baix a dalt.
*/
/*
3. Genera una segona sopa de lletres amb el mateix contingut que l’anterior. Aquest cop, 
però, ha de tenir les paraules introduïdes per l’usuari ressaltades a mode de solució per 
a que puguin ser consultades en cas de dubte.
*/
// array that will contain the words inserted by the user
const words = [];
// number of words the user is asked to introduce
const NUM_OF_WORDS = 5;
// variable that handles the loop that asks for new words
let counter = 0;
// words prompt loop
const askForWords = () => {
    while (counter <= NUM_OF_WORDS) {
        words[counter] = prompt(`Introdueix la paraula numero ${++counter}.`);
    }
    console.log('The words introduced by the user: ' + words);
}
// 2.
/////////////// GENERATE ALPHABET //////////////
const alphabet = [];
const genAlphabet = () => {
    // begining letter
    const beginniing = 'A'.charCodeAt(0);
    // end letter
    const end  = 'Z'.charCodeAt(0);
    // push each letter to alphabet array
    for (let i = beginniing; i <= end; ++i) {
        alphabet.push(String.fromCharCode(i));
    }
}
genAlphabet();
// random letter picker
// returns a single capital letter
const getAlphabetLetter = () => {
    return alphabet[Math.floor(Math.random()* alphabet.length)];
}
// constant with the number of columns and rows of the table
const NUM_COLS_ROWS = 20;
// create table element
const _table = document.createElement('table');
// assign id to the table for better handling
_table.id = 'myTable';
// iterate through the columns and rows to create them
for (let i = 0; i < NUM_COLS_ROWS; i++) {
    // create row
    const _row = document.createElement('tr');
    for (let j = 0; j < NUM_COLS_ROWS; j++) {
        // create cell
        const _col = document.createElement('td');
        // get alphabet letter to append later to the column/cell
        const letter = getAlphabetLetter();
        // create text node with random uppercase alphabet letter
        const _text = document.createTextNode(letter);
        // append text to cell
        _col.appendChild(_text);
        // append cell to row
        _row.appendChild(_col);
    }
    // append row to table
    _table.appendChild(_row);
}
// select a div in order to append to it later
const _div = document.getElementById('container');
// append table to the div
_div.appendChild(_table);
// select table for further manipulation
const myTable = document.getElementById('myTable');
/* //// THEORY GUIDE ////
 let table = document.body.firstElementChild;

    for (let i = 0; i < table.rows.length; i++) {
      let row = table.rows[i];
      row.cells[i].style.backgroundColor = 'red';
*/

//// DOCUMENTATION ////
// these methods use a combination of:
    // + N => displace the text N rows/cols to one side
    // words[N].length - i => start from the furtherest cell and iterate in reverse
    // rows[i].cells[i] => iterates in diagonal
    // rows[words[N].length - i].cells[words[N].length - i] => write in diagonal in reverse order

//1. La primera paraula s’ha de poder llegir d’esquerra a dreta. 
for (let i = 0;i < words[0].length; i++) {
    const row = myTable.rows[0];
    row.cells[i].textContent = words[0][i];
}
// 2. La segona paraula s’ha de poder llegir de dreta a esquerra.
for (let i = 0;i < words[1].length; i++) {
    // notice we start from the second row and write from the right to the left
    // movement from furtherest column to the colsest one with 
    // ells[words[1].length - i - 1]
    const row = myTable.rows[1];
    row.cells[words[1].length - i - 1].textContent = words[1][i];
}
// 3. La tercera paraula s’ha de poder llegir de dalt a baix.
for (let i = 0;i < words[2].length; i++) {
    const row = myTable.rows[i + 2];
    row.cells[0].textContent = words[2][i];
}
// 4. La quarta paraula s’ha de poder llegir de baix a dalt.
for (let i = 0;i < words[3].length; i++) {
    const row = myTable.rows[i + 2];
    row.cells[1].textContent = words[3][words[3].length - i - 1];
}
// 5. La cinquena paraula s’ha de poder llegir en diagonal, d’esquerra a dreta i de dalt a baix.
for (let i = 0;i < words[4].length; i++) {
    const row = myTable.rows[i + 2];
    row.cells[2 + i].textContent = words[4][i];
}
// 6. La sisena paraula s’ha de poder llegir en diagonal, de dreta a esquerra i de baix a dalt.
for (let i = 0;i < words[5].length; i++) {
    const row = myTable.rows[words[5].length - i + 5];
    row.cells[words[5].length  + 2 - i].textContent = words[5][i];
}
/* Genera una segona sopa de lletres amb el mateix contingut que l’anterior. Aquest cop, 
però, ha de tenir les paraules introduïdes per l’usuari ressaltades a mode de solució per 
a que puguin ser consultades en cas de dubte.
*/
//////////////////////////////////////////////////////
/////////////// SEGUNDA SOPA DE LLETRES //////////////
//////////////////////////////////////////////////////
////////////// VERSIO OPTIMITZADA ////////////////////
//////////////////////////////////////////////////////
// create a new table, this time adding highlight class to the cells the user inserts
const createTable = () => {
    const _table2 = document.createElement('table');
    _table2.id = 'myTable2';
    for (let i = 0; i < NUM_COLS_ROWS; i++) {
        const _row = document.createElement('tr');
        for (let j = 0; j < NUM_COLS_ROWS; j++) {
            const _col = document.createElement('td');
            const letter = getAlphabetLetter();
            const _text = document.createTextNode(letter);
            _col.appendChild(_text);
            _row.appendChild(_col);
        }
        _table2.appendChild(_row);
    }
    _div.appendChild(_table2);
}
const myTable2 = document.getElementById('myTable2');
/* THEORY GUIDE
 let table = document.body.firstElementChild;

    for (let i = 0; i < table.rows.length; i++) {
      let row = table.rows[i];
      row.cells[i].style.backgroundColor = 'red';
*/
const insertHorizontal = (str,row) => {
    for (let i = 0;i < words[0].length; i++) {
        const row = myTable2.rows[row];
        row.cells[i].textContent = str[i];
        // NOTICE this is an example of adding highlight to the cell
        // the user generated, styling is done via CSS stylesheet + class attribute
        row.cells[i].classList.add('highlight');
    }
}
//1. La primera paraula s’ha de poder llegir d’esquerra a dreta.

for (let i = 0;i < words[0].length; i++) {
    const row = myTable2.rows[0];
    row.cells[i].textContent = words[0][i];
    // NOTICE this is an example of adding highlight to the cell
    // the user generated, styling is done via CSS stylesheet + class attribute
    row.cells[i].classList.add('highlight');
}
// 2. La segona paraula s’ha de poder llegir de dreta a esquerra.
for (let i = 0;i < words[1].length; i++) {
    const row = myTable2.rows[1];
    row.cells[words[1].length - i - 1].textContent = words[1][i];
    row.cells[words[1].length - i - 1].classList.add('highlight');
}
// 3. La tercera paraula s’ha de poder llegir de dalt a baix.
for (let i = 0;i < words[2].length; i++) {
    const row = myTable2.rows[i + 2];
    row.cells[0].textContent = words[2][i];
    row.cells[0].classList.add('highlight');
}
// 4. La quarta paraula s’ha de poder llegir de baix a dalt.
for (let i = 0;i < words[3].length; i++) {
    const row = myTable2.rows[i + 2];
    row.cells[1].textContent = words[3][words[3].length - i - 1];
    row.cells[1].classList.add('highlight');
}
// 5. La cinquena paraula s’ha de poder llegir en diagonal, d’esquerra a dreta i de dalt a baix.
for (let i = 0;i < words[4].length; i++) {
    const row = myTable2.rows[i + 2];
    row.cells[2 + i].textContent = words[4][i];
    row.cells[2 + i].classList.add('highlight');
}
// 6. La sisena paraula s’ha de poder llegir en diagonal, de dreta a esquerra i de baix a dalt.
for (let i = 0;i < words[5].length; i++) {
    const row = myTable2.rows[words[5].length - i + 5];
    row.cells[words[5].length  + 2 - i].textContent = words[5][i];
    row.cells[words[5].length  + 2 - i].classList.add('highlight');
}
/*
En un objecte, emmagatzema quantes vegades apareix cadascuna de les lletres de l’abecedari a 
la sopa de lletres. Seguidament mostra aquesta informació a l’usuari.
*/
// create the object we'll use to store the data
const myObj = {};
// iterate through the table
for (let i = 0; i < NUM_COLS_ROWS; i++) {
    for (let j = 0; j < NUM_COLS_ROWS; j++) {
        // store the letter in uppercase (to avoid missmatching between lower and upper case)
        const lletra = myTable2.rows[i].cells[j].textContent.toUpperCase();
        // if the letter is already in the object add +1 to the value of the key
        if (myObj[lletra] != undefined) {
            myObj[lletra]++;
        } else {
            // letter is new in alphabet, initialize it's value and add to the object
            myObj[lletra] = 1;
        }
    }
}
// string used to print the result
let strResult = '';
// itera per l'objecte
// iterat through the object using only the key property
for (const letter in myObj) {
    // this line is mandatory for the proper function of the loop
    if (myObj.hasOwnProperty(letter)) {
        // get value of the key
        const element = myObj[letter];
        // generate new line string with the values
        strResult += `The letter ${letter} is found ${element} times in the table\n`;
    }
}
// show result
console.log(strResult);
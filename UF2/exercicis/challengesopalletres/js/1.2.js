"use strict";
/*
1. Demana cinc paraules a l’usuari i mostra-les per pantalla o consola.
*/
/*
2. Genera una sopa de lletres amb 20 files i 20 columnes que inclogui les paraules que s’han introduït a l’apartat anterior. Cada paraula s’ha de mostrar d’una manera determinada:

    1. La primera paraula s’ha de poder llegir d’esquerra a dreta. 

    2. La segona paraula s’ha de poder llegir de dreta a esquerra.

    3. La tercera paraula s’ha de poder llegir de dalt a baix.

    4. La quarta paraula s’ha de poder llegir de baix a dalt.

    5. La cinquena paraula s’ha de poder llegir en diagonal, d’esquerra a dreta i de dalt a baix.

    6. La sisena paraula s’ha de poder llegir en diagonal, de dreta a esquerra i de baix a dalt.
*/
/*
3. Genera una segona sopa de lletres amb el mateix contingut que l’anterior. Aquest cop, 
però, ha de tenir les paraules introduïdes per l’usuari ressaltades a mode de solució per 
a que puguin ser consultades en cas de dubte.
*/

///////////////// EXERCISE BODY ///////////////


/////////// GLOBAL VARIABLES /////////////////
// array that will contain the words inserted by the user
let words = [];
// number of words the user is asked to introduce
const NUM_OF_WORDS = 5;
// variable that handles the loop that asks for new words
let counterPrompt = 0;
// global variable from which the table element will be called
let myTable;
// number of rows and cols the table has
const NUM_COLS_ROWS = 20;
// div where the tables will be inserted
const _div = document.getElementById('container');
// debuging?
let debug;
/////////// END GLOBAL VARIABLES /////////////


// 0.1 choose between inserting words or debug version with preinserted words
const chooseDebugOrLive = () => {
    const debugPr = prompt('Do you want to insert your own words or run debug mode (with preinserted words)? if you want debug version inser "y"')
    debugPr == 'y'? debug = true : debug = false;
}

// 1. prompt function

// words prompt loop
const askForWords = () => {
    if (debug) {
        // debugging words filling (bypasses the prompt for debugging purposes)
        words = ['bocadillo', 'television', 'arbol', 'azulejo', 'tortilla', 'patatas'];     
        console.log(words);
        return;
    }
    while (words.length <= NUM_OF_WORDS) {
        do {
            words[counterPrompt] = prompt(`Introdueix la paraula numero ${counterPrompt + 1}, ha de ser  de 20 caracters maxim.`);
            if (words[counterPrompt].length > 20) {alert("incorrecte, introdueix una paraula de maxim 20 caracters");}
        } while (words[counterPrompt].length > 20);
        counterPrompt++;
       // words[counterPrompt] = prompt(`Introdueix la paraula numero ${++counterPrompt}.`);
    }
    console.log('The words introduced by the user: ' + words);
}


// 2.0.-1 generate alphabet
/////////////// GENERATE ALPHABET //////////////
const alphabet = [];
const genAlphabet = () => {
    // begining letter
    const beginniing = 'A'.charCodeAt(0);
    // end letter
    const end  = 'Z'.charCodeAt(0);
    // push each letter to alphabet array
    for (let i = beginniing; i <= end; ++i) {
        alphabet.push(String.fromCharCode(i));
    }
}
// 2.0.-2 get random letter from alphabet
// random letter picker
// returns a single capital letter
const getAlphabetLetter = () => {
    return alphabet[Math.floor(Math.random()* alphabet.length)];
}
// 2.0.-3 get random position
const randomPos = () => Math.floor(Math.random() * NUM_COLS_ROWS);
// 2.0.-4 reverse string
const reverseString = (myString) => myString.split("").reverse().join("");

//2.0 generate "sopa de lletras" function
const createTable = () => {
    // initialize alphabet to obtain letters from
    genAlphabet()
    const _table = document.createElement('table');
    _table.id = 'myTable';
    for (let i = 0; i < NUM_COLS_ROWS; i++) {
        const _row = document.createElement('tr');
        for (let j = 0; j < NUM_COLS_ROWS; j++) {
            const _col = document.createElement('td');
            const letter = getAlphabetLetter();
            const _text = document.createTextNode(letter);
            _col.appendChild(_text);
            _row.appendChild(_col);
        }
        _table.appendChild(_row);
    }
    _div.appendChild(_table);
}

// 2.1.etc INSERT WORDS

// HORIZONTAL
const insWordHorizon = (word,xPos,yPos) => {
    // relocate words that are out of bounds to 0 index so they can fit
    if (xPos + word.length > 19) {
        xPos = 0;
    }
    for (let i = 0;i < word.length; i++) {
        const row = myTable.rows[yPos];
        // collision controll (doesn't contemplate word out of bounds
        // only word colliding gets assigned a new possition)
        if (row.cells[xPos + i].classList.contains("highlight")) {
            yPos = randomPos();
            i = -1;
        }
    }
    // write the word and add the highlight classs
    for (let i = 0;i < word.length; i++) {
        const row = myTable.rows[yPos];
        row.cells[xPos + i].textContent = word[i];
        // NOTICE this is an example of adding highlight to the cell
        // the user generated, styling is done via CSS stylesheet + class attribute
        row.cells[xPos + i].classList.add('highlight');
    }
}
// VERTICAL
const insWordVert = (word,xPos,yPos) => {
    // same as above
    if (yPos + word.length > 19) {
        yPos = 0;
    }
    // collission control, if words collide they get a new possition unless the collission
    // happens at the same letter
    for (let i = 0;i < word.length; i++) {
        const row = myTable.rows[i + yPos];
        if (row.cells[xPos].classList.contains("highlight") &&  row.cells[xPos].textContent != word[i]) {
            xPos = randomPos();
            i = -1;
        }
    }
    // write the word and add the highlight classs
    for (let i = 0;i < word.length; i++) {
        const row = myTable.rows[i + yPos];
        row.cells[xPos].textContent = word[i];
        row.cells[xPos].classList.add('highlight');
    }
}
// DIAGONAL TOP BOTTOM LEFT RIGHT
const insWordDiag = (word,xPos,yPos) => {
    // word reassignation to fit the grid
    if (xPos + word.length > 19) {
        xPos = 0;
    }
    if (yPos + word.length > 19) {
        yPos = 0;
    }
    // collision control
    let row;
    for (let i = 0;i < word.length; i++) {
        row = myTable.rows[yPos + i];
        if (row.cells[xPos + i].classList.contains("highlight") && row.cells[xPos + i].textContent !=  word[i] ) {
            // now both possitions x and y get reassigned to reduce chance of collission
            do {
                yPos = randomPos();
                xPos = randomPos();
            } while (yPos + word.length > 19 || xPos + word.length > 19);
            i = -1;
        }
    }
    for (let i = 0;i < word.length; i++) {
        row = myTable.rows[i + yPos];
        row.cells[xPos + i].textContent = word[i];
        row.cells[xPos + i].classList.add('highlight');
    }
}
// DIAGONAL BOTTOM RIGHT TO TOP LEFT
const insWordDiagRev = (word,xPos,yPos) => {
    if (xPos + word.length > 19) {
        xPos = 0;
    }
    if (yPos + word.length > 19) {
        yPos = 0;
    }
    // collision control
    let row;
    for (let i = 0;i < word.length; i++) {
        row = myTable.rows[word.length - i + yPos];
        if (row.cells[word.length  + xPos - i].classList.contains("highlight") && row.cells[word.length  + xPos - i].textContent != word[i]) {
            do {
                yPos = randomPos();
                xPos = randomPos();
            } while (yPos + word.length > 19 || xPos + word.length > 19);
            i = -1;
        }
    }
    for (let i = 0;i < word.length; i++) {
        row = myTable.rows[word.length - i + yPos];
        row.cells[word.length  + xPos - i].textContent = word[i];
        row.cells[word.length  + xPos - i].classList.add('highlight');
    }
}
// 3. Object
// create the object we'll use to store the data
const showObj = () => {
    const myObj = {};
    // iterate through the table
    for (let i = 0; i < NUM_COLS_ROWS; i++) {
        for (let j = 0; j < NUM_COLS_ROWS; j++) {
            // store the letter in uppercase (to avoid missmatching between lower and upper case)
            const lletra = myTable.rows[i].cells[j].textContent.toUpperCase();
            // if the letter is already in the object add +1 to the value of the key
            if (myObj[lletra] != undefined) {
                myObj[lletra]++;
            } else {
                // letter is new in alphabet, initialize it's value and add to the object
                myObj[lletra] = 1;
            }
        }
    }
    // string used to print the result
    let strResult = '';
    // itera per l'objecte
    // iterat through the object using only the key property
    for (const letter in myObj) {
        // this line is mandatory for the proper function of the loop
        if (myObj.hasOwnProperty(letter)) {
            // get value of the key
            const element = myObj[letter];
            // generate new line string with the values
            strResult += `The letter ${letter} is found ${element} times in the table\n`;
        }
    }
    // show result
    console.log(strResult);
}
////////////////////////////////////////////////
/////////////////// MAIN ///////////////////////
////////////////////////////////////////////////
const main = () => {
    //choose between using prefefined words or inserting your own
    chooseDebugOrLive()
    // ask user for 6 wrods 
    askForWords()
    // create table
    createTable();
    // get table id after its creation
    myTable = document.getElementById('myTable');
    /// LOOP THAT HANDLES INSERTING WORDS
    for (let i = 0; i < words.length; i++) {
        switch (i) {
            case 0:
                 // insert horizontal words //////////////
                    // normal order
                insWordHorizon(words[i],randomPos(),randomPos());
                break;
            case 1:
                    // inverted order
                insWordHorizon(reverseString(words[i]),randomPos(),randomPos());
                break;
            case 2:
                // insert vertical words ///////////////
                    // normal order    
                insWordVert(words[i],randomPos(),randomPos());
                break;
            case 3:
                    // inverted order
                insWordVert(reverseString(words[i]),randomPos(),randomPos());
                break;
            case 4:
                // insert diagonal (top left to bottom right) words ///////////////
                insWordDiag(words[i],randomPos(),randomPos());
                break;
            case 5:
                // insert diagonal (top left to bottom right) words ///////////////
                insWordDiagRev(words[i],randomPos(),randomPos());    
                break;
            default:
                // ERROR
                return "ERROR: loop malfunction";
                break;
        } 
    }
    showObj();
}
main();
"use strict";
/* 
Challenge 1#
We're building a football betting app (soccer for my American friends 😅)!

Suppose we get data from a web service about a certain game (below). In this 
challenge we're gonna work with the data. So here are your tasks:

1. Create one player array for each team (variables 'players1' and 'players2')

2. The first player in any player array is the goalkeeper and the others are 
field players. For Bayern Munich (team 1) create one variable ('gk') with the 
goalkeeper's name, and one array ('fieldPlayers') with all the remaining 10 
field players

3. Create an array 'allPlayers' containing all players of both teams (22 players)

4. During the game, Bayern Munich (team 1) used 3 substitute players. So create a 
new array ('players1Final') containing all the original team1 players plus 
'Thiago', 'Coutinho' and 'Perisic'

5. Based on the game.odds object, create one variable for each odd 
(called 'team1', 'draw' and 'team2')

6. Write a function ('printGoals') that receives an arbitrary number of player 
names (NOT an array) and prints each of them to the console, along with the 
number of goals that were scored in total (number of player names passed in)

7. The team with the lower odd is more likely to win. Print to the console which 
team is more likely to win, WITHOUT using an if/else statement or the ternary operator.

TEST DATA FOR 6: Use players 'Davies', 'Muller', 'Lewandowski' and 'Kimmich'. 
Then, call the function again with players from game.scored
*/
//1
const players1 = ['Casillas', 'Cristiano', 'Ramos', 'Paquito', 'Manolo', 'Juan']
const players2 = ['Zubizarreta', 'Stozhkov', 'Kuman', 'Romario', 'Laudrup', 'Vaquero']
//2 goal keeper and fieldplayers as the first element for gk1 and the rest except the first one for fielPayers
// gk1 takes the first position of the array
// fieldplayers take all the positions except the first one
const [gk1, ...fieldPlayers1] = players1;
console.log(`Goal keeper: ${gk1}, fieldPlayers: ${fieldPlayers1}`);
//3 single array, all players
// ...p1 takes the whole array, and the second one is concatenated with ...p2
const allPlayers = [...players1,...players2];
console.log(`All players from both teams: ${allPlayers}`);
//4 add substitutes to the p1 arr and append 3 more players
// ...p1 to include the whole players array and the next possitions to append the substitute players
const players1Final = [...players1, 'Thiago', 'Coutinho', 'Perisic'];
console.log(`All the players from team 1 plus the substitutes: ${players1Final}`);

/*
5. Based on the game.odds object, create one variable for each odd 
(called 'team1', 'draw' and 'team2')
*/
const game =  {
    odds : ['team 1 wins','draw','team 2 wins'],
}
const [team1,draw,team2] = [...game.odds];
console.log(`These are the values. team1: ${team1}, team2: ${team2}, draw: ${draw}`);
//6 function that takes players players names and the goals scored by each of them
// map the players and their goals according to the theory provided
// use positions instead of names to link them to the original array
const playerGoals =  {
    [players1[1]] : {goals : 1,},
    [players1[2]] : {goals : 2,},
    [players1[3]] : {goals : 0,},
    [players1[4]] : {goals : 3,},
};
// recieve a list of players with '...players'
const printGoals = (...players) => {
    // iterate through each player in the list
    players.forEach(function(player){
        // player => single player's name on the list (we'll assume they are all valid and existing in players1 array)
        //playerGoals[player].goals
        // ^this is key, if we don't use object[string] syntax we can't access the key so the only solution I've found is 
        // the [] that takes strings
        // this reads the goals from the player
          console.log(`${ player} scored ${playerGoals[player].goals} goals`);
      });
}
printGoals('Cristiano','Ramos','Manolo');

/*
7. The team with the lower odd is more likely to win. Print to the console which 
team is more likely to win, WITHOUT using an if/else statement or the ternary operator.
*/
// define an objects with the name of the teams and it's odds
const teamsOdds = {team1 : 10, team2: 7};
// initialize array where the odds alone, their numeric value will be stored
const oddsArray = []
// fill the oddsArray array with the numeric value of each team's odds
Object.entries(teamsOdds).forEach(([key, value]) => 
{oddsArray.push(value);
});
// sort the array in ascending order (smaller number to the left)
oddsArray.sort((a,b) => a -b);
// function to find an object's key from it's value 
// it will return the value of the key 
// it's parameters are the object and the value to check against
const getKeyByValue = (object, value) => {
    return Object.keys(object).find(key => object[key] === value);
}
// build the string with the team of greater odds
console.log(`the team with the greater odds to win is ${getKeyByValue(teamsOdds,oddsArray[1])}`);
# Escriu un fitxer markdow  (.md) amb els següent conceptes:  
  
*  Diferències entre arrays i objectes.
*  Què és un prototipus en javascript?
*  Què és un constructor en javascript?

## Diferències entre arrays i objectes.  
### Similaritats  
* tots dos poden incluir valors de tipus mixte, aixo vol dir String, numeric, null, undefined. etc
* esl dos poden ser iterats de manera neutral (sense saver el contingut, per exemple en cas d'un objecte la seva key)
* tots dos son col·lecció de dades
  
### Diferencies
Els arrays es poden recorrer de manera neutral i simple (sense saber que contenen). Els objectes tambe, pero en general es preferible saver el nom de les seves `keys` per poder accedir als seus `value`.  
En el cas de els objectes, l'informació es sol estructurar en format `key` `value` i s'enten que qui vulgui accedir a l'objecte ho fa savent l'estructura i el nom dels valors, lo qual es diferent de com funcionen els arrays.
La forma d'iterar i cercar es tambe bastant diferent, encara que hi ha metodes a partir d'ES6 que ho fan bastant intuitivament. Un exemple es el metode `includes` per arrays o el metode `hasOwnProperty` per objectes.  
  
## Què és un prototipus en javascript?  
```
Prototype Inheritance

All JavaScript objects inherit properties and methods from a prototype:

    Date objects inherit from Date.prototype
    Array objects inherit from Array.prototype
    Person objects inherit from Person.prototype

The Object.prototype is on the top of the prototype inheritance chain:

Date objects, Array objects, and Person objects inherit from Object.prototype.
```
  
Es un concepte abstracte, es la caixa en la qual es fiquen propietats i metodes. Tambe s'utilitza per modificar/interactuar amb objectes abstractes com pot ser una `Class`, un Array, etc.  
  
## Què és un constructor en javascript?  
Es lo mateix que un consturctor en Java, es un tipus de ¿metode? a partir del qual s'instancia una Class i es poden (o no) passar parametres per omplir atributs de la Class a la instanciació especifica.  
  

"use strict";
/*
Challenge #1

1. Use a constructor function to implement a Car. A car has 
a make and a speed property. The speed property is the 
current speed of the car in km/h;

2. Implement an 'accelerate' method that will increase the 
car's speed by 10, and log the new speed to the console;

3. Implement a 'brake' method that will decrease the car's 
speed by 5, and log the new speed to the console;

4. Create 2 car objects and experiment with calling 
'accelerate' and 'brake' multiple times on each of them.

DATA CAR 1: 'BMW' going at 120 km/h
DATA CAR 2: 'Mercedes' going at 95 km/h

GOOD LUCK 😀
 

*/
class Car {
    // constructor
    constructor (make, speed) {
        this.make = make;
        this.speed = speed;
    }
    // string builder method
    strBuild() {
        return this.make + ' going at ' + this.speed + 'km/h';
    }
    // accelerate method
    accelerate() {
        this.speed += 10;
        console.log(this.strBuild());
    }
    // brake method
    brake() {
        this.speed -= 5;
        console.log(this.strBuild());
    }
}
const car1 = new Car("BMW",0);
const car2 = new Car("Mercedes",100);
car1.accelerate();
car1.accelerate();
car1.accelerate();
car2.brake();
car1.brake();
car2.accelerate();
car1.accelerate();
//Challenge 1#

const game = {
  team1: 'Bayern Munich',
  team2: 'Borrussia Dortmund',
  players: [
    [
      'Neuer',
      'Pavard',
      'Martinez',
      'Alaba',
      'Davies',
      'Kimmich',
      'Goretzka',
      'Coman',
      'Muller',
      'Gnarby',
      'Lewandowski',
    ],
    [
      'Burki',
      'Schulz',
      'Hummels',
      'Akanji',
      'Hakimi',
      'Weigl',
      'Witsel',
      'Hazard',
      'Brandt',
      'Sancho',
      'Gotze',
    ],
  ],
  score: '4:0',
  scored: ['Lewandowski', 'Gnarby', 'Lewandowski', 'Hummels'],
  date: 'Nov 9th, 2037',
  odds: {
    team1: 1.33,
    x: 3.25,
    team2: 6.5,
  },
};
/*
We're building a football betting app (soccer for my American friends 😅)!

Suppose we get data from a web service about a certain game (below). In this challenge we're gonna work with the data. So here are your tasks:

1. Create one player array for each team (variables 'players1' and 'players2')
2. The first player in any player array is the goalkeeper and the others are field players. For Bayern Munich (team 1) create one variable ('gk') with the goalkeeper's name, and one array ('fieldPlayers') with all the remaining 10 field players
3. Create an array 'allPlayers' containing all players of both teams (22 players)
4. During the game, Bayern Munich (team 1) used 3 substitute players. So create a new array ('players1Final') containing all the original team1 players plus 'Thiago', 'Coutinho' and 'Perisic'
5. Based on the game.odds object, create one variable for each odd (called 'team1', 'draw' and 'team2')
6. Write a function ('printGoals') that receives an arbitrary number of player names (NOT an array) and prints each of them to the console, along with the number of goals that were scored in total (number of player names passed in)
7. The team with the lower odd is more likely to win. Print to the console which team is more likely to win, WITHOUT using an if/else statement or the ternary operator.

TEST DATA FOR 6: Use players 'Davies', 'Muller', 'Lewandowski' and 'Kimmich'. Then, call the function again with players from game.scored

GOOD LUCK 😀
*/
/*
1. Create one player array for each team (variables 'players1' and 
'players2')
*/
// if we don't use the split operator the players2 array is empty
const [players1,players2] = [...game.players];
console.log("Players from the first team: " + players1);
console.log("Players from the second team: " + players2);
/*
2. The first player in any player array is the goalkeeper and the 
others are field players. For Bayern Munich (team 1) create one 
variable ('gk') with the goalkeeper's name, and one array ('fieldPlayers')
 with all the remaining 10 field players
*/
// we keep the first position in the first variable
// the rest of the array goes to the second variable which is an array
// that only contains the rest of the players minus the goalkeeper
const [gk,...fieldPlayers] = [...players1]; 
console.log("Goal keeper: " + gk, ", Rest of players: " +fieldPlayers);
/*
3. Create an array 'allPlayers' containing all players of both teams 
(22 players)
*/
// we use the split operator to concatenate the two arrays
const allPlayers = [...players1,...players2];
console.log(allPlayers);
/*
4. During the game, Bayern Munich (team 1) used 3 substitute players. 
So create a new array ('players1Final') containing all the original 
team1 players plus 'Thiago', 'Coutinho' and 'Perisic'
*/
const players1Final = [...players1, 'Thiago', 'Couthino', 'Perisic'];
console.log("Players including substitutes: " + players1Final); 
/*
5. Based on the game.odds object, create one variable for each odd 
(called 'team1', 'draw' and 'team2')
*/
// we need the keys of the object and we assign the values to our new defined keys
// note we need to actually match the keys of the object
const {
    odds: { team1: team1, team2: team2 , x: draw},
  } = game;
console.log("Team1's odds: " + team1,"Draw's odds: " + draw,"Team2's odds: " + team2);
/*
6. Write a function ('printGoals') that receives an arbitrary number of
 player names (NOT an array) and prints each of them to the console, 
 along with the number of goals that were scored in total (number of 
    player names passed in)
*/
const printGoals = (...players) => {
    // iterate through the list of players
    for (const player of players) {
        // reset goals for each player
        let goals = 0;
        // iterate through players that scored
        for (const playerGoal of game.scored) {
            // if the player is found inside the players that scored
            // add a goal
            if (player == playerGoal) {
                goals++;
            }
        }
        // once we're out of the playersScored loop, we have
        // the total ammount of goals for each player
        // we can safely print the goals now
        console.log(`${player} scored ${goals} goals.`);
    }
}
// we call the function to debug
// we pass it all the players that scored 1 goal
// 1 player that scored 2 (LEwandowski) and one player
// that scored none (Gotze)
printGoals('Lewandowski', 'Gnarby', 'Hummels', 'Gotze');
/*
7. The team with the lower odd is more likely to win. Print to the console
 which team is more likely to win, WITHOUT using an if/else statement or 
 the ternary operator.
*/
// declare global array
// we'll use it to access the team with the lowest odds
const sortedOdds = [];
// iterate through the odds object
for (const team in game.odds) {
    // fill in our array with a an array of key value for every
    // key value pair in the odds object
    sortedOdds.push([team, game.odds[team]]);
}
// sort the array of arrays with the odds, sort by odds value (smaller goes first)
sortedOdds.sort((a,b) => a[1] - b[1]);
// position 0 is the one with the lowest odds
console.log("Winning team: " + sortedOdds[0]);
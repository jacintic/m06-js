"use strict";
/*
Challenge 3#

1. Use a constructor function to implement an Electric Car 
(called EV) as a CHILD "class" of Car. Besides a make and 
current speed, the EV also has the current battery charge 
in % ('charge' property);

2. Implement a 'chargeBattery' method which takes an argument 
'chargeTo' and sets the battery charge to 'chargeTo';

3. Implement an 'accelerate' method that will increase the 
car's speed by 20, and decrease the charge by 1%. Then log a 
message like this: 'Tesla going at 140 km/h, with a charge of 22%';

4. Create an electric car object and experiment with calling 
'accelerate', 'brake' and 'chargeBattery' (charge to 90%). 
Notice what happens when you 'accelerate'! HINT: Review the definiton of polymorphism 😉

DATA CAR 1: 'Tesla' going at 120 km/h, with a charge of 23%
*/
class Car {
    // constructor
    constructor (make, speed) {
        this.make = make;
        this.speed = speed;
    }
    // string builder method
    strBuild() {
        return this.make + ' going at ' + this.speed + 'km/h';
    }
    ////// GETTERS AND SETTERS ///////
    // getter for speed in miles per hour
    get speedUS() {
        return `${this.speed /1.6} mi/h`;
    }
    // setter speed in miles per hour
    set speedUS(speedParam) {
        this.speed = (speedParam * 1.6);
    }
    ////// METHODS//////
    // accelerate method
    accelerate() {
        this.speed += 10;
        console.log(this.strBuild());
    }
    // brake method
    brake() {
        this.speed -= 5;
        console.log(this.strBuild());
    }

}

// child class (ElectricCar) extends Parent class Car
class ElectricCar extends Car {
    /////// CONSTRUCTOR ///////////
    // super passes parent method's parameter to Car superclass
    constructor(make, speed, battery) {
      super(make, speed); 
      this.battery = battery;
    }
  
    /////// SETTERS //////////
    // charges battery
    set chargeBattery(chargeTo) {
        this.battery = chargeTo;
    }
    /////// METHODS ///////////
    // overrides parent method accelerate (doesn't call paren'ts function actions)
    // to call parent's function actions do:
    /*
        accelerate() {
             super.accelerate();
        }
    */
    accelerate() {
        this.speed += 20;
        this.battery--;
        // call parent method strBuild to reuse its string method:
        // strBuild Parent's code:  "return this.make + ' going at ' + this.speed + 'km/h';""
        console.log(`${this.strBuild()}, with a charge of ${this.battery}%.`);
    }
}
console.log("Calling constructor of child class with parameters:");
console.log("* make: Tesla");
console.log("* speed: 0km/h");
console.log("* battery: 0%");
const tesla = new ElectricCar("Tesla",0, 0);
console.log(tesla.speedUS + " velocitat Tesla parat 'new ElectricCar('Tesla',0, 0)'");
console.log('Charge battery to 90%');
tesla.chargeBattery = 90;
console.log("Accelerate method (-1% battery + 20 km/h).")
tesla.accelerate();
tesla.accelerate();
tesla.accelerate();
tesla.accelerate();

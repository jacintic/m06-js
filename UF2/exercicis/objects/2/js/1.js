"use strict";
/*
Challenge 2#
1. Re-create challenge 1, but this time using an ES6 class;
2. Add a getter called 'speedUS' which returns the current 
speed in mi/h (divide by 1.6);
3. Add a setter called 'speedUS' which sets the current speed 
in mi/h (but converts it to km/h before storing the value, 
    by multiplying the input by 1.6);
4. Create a new car and experiment with the accelerate and 
brake methods, and with the getter and setter.

*/
class Car {
    // constructor
    constructor (make, speed) {
        this.make = make;
        this.speed = speed;
    }
    // string builder method
    // builds a string that will be called by break and accelerate methods
    // applying DRY (Don't Repeat Yourself) concept define string once, call twice
    strBuild() {
        return this.make + ' going at ' + this.speed + 'km/h';
    }
    // getter for speed in miles per hour
    // converts km/h to mi/h
    get speedUS() {
        return `${this.speed /1.6} mi/h`;
    }
    // setter speed in miles per hour
    // gets mi/h param, transforms to km/h when
    // setting the parameter in this.speed
    set speedUS(speedParam) {
        this.speed = (speedParam * 1.6);
    }
    // accelerate method
    // speed + 10
    // also calls string builder method
    accelerate() {
        this.speed += 10;
        console.log(this.strBuild());
    }
    // brake method
    // speed -5
    // also calls string builder method
    brake() {
        this.speed -= 5;
        console.log(this.strBuild());
    }

}
console.log("---- same as exercise 1 ----");
const car1 = new Car("BMW",0);
const car2 = new Car("Mercedes",100);
car1.accelerate();
car1.accelerate();
car1.accelerate();
car2.brake();
car1.brake();
car2.accelerate();
car1.accelerate();
console.log("---- in km/h ----");
car2.accelerate();
car1.accelerate();
console.log("---- getting speed in miles per hour ----");
console.log(car1.speedUS);
console.log(car2.speedUS);
console.log("---- setting speed of car 1 as 100 miles per hour 'car1.speedUS = 100;' ----");
car1.speedUS = 100;
console.log("---- car 1's speed in miles per hour after being set ----");
console.log(car1.speedUS);
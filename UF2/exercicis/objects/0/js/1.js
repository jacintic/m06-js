"use strict";
/*
Seguint l'exemple anterior, farem el següent exercici:

Anem a utilitzar objectes per calcular l'index de massa corporal ( IMC )

Recordeu: IMC = massa / alçada ** 2 = massa / (alçada * alçada). 
(massa en kg i alçada en metres)

1. Per a cadascun d’ells, creeu un objecte amb propietats pel nom 
complet, la massa i l’alçada (un pel Mark Miller i un altre pel John Smith)

2. Creeu un mètode 'calcBMI' en cada objecte per calcular l'IMC 
(de moment, el mateix mètode en tots dos objectes). Emmagatzemeu el valor 
de l'IMC a una propietat i també el torneu del mètode.

3. Inicieu sessió a la consola que tingui l’IMC més alt, juntament amb
 el nom complet i l’IMC respectiu. Exemple: "L'IMC de John Smith (28,3) 
 és superior al de Mark Miller (23,9)!"


DADES DE LA PROVA:

Marks pesa 78 kg i fa 1,69 m d'alçada.

John pesa 92 kg i fa 1,95 m d'alçada.

GOOD LUCK 😀
*/
// declare objects
const mark = {
    name : "Mark Miller",
    mass : 78,
    height : 1.69,
    // declare BMI function
    calcBMI  (){  
        return  this.mass /Math.pow(this.height,2);
    },    
}
const john = {
    name : "John Smith",
    mass : 92,
    height : 1.95,
    calcBMI  () {  
        return  this.mass /Math.pow(this.height,2);
    },  
}
// ATENTION! if the calcBMI function isn't called as mark.calcBMI() 
// including the () it won't return the value the function returns.
const getHighesBMI = () => {
    // get the BMI values
    const marksBmi = mark.calcBMI();
    const johnsBmi = john.calcBMI();
    // get the greater and lesser BMI object (mark or john?)
    const greaterBmi = johnsBmi > marksBmi? john : mark;
    const lesserBmi = johnsBmi < marksBmi? john : mark;
    // print the result using the greater and lesser stored values
    console.log("l'IMC de " + greaterBmi.name + " (" + greaterBmi.height  
    + "," + greaterBmi.mass  +") és superior al de " + lesserBmi.name + 
    " (" + lesserBmi.height + "," + lesserBmi.mass  +")!" );
}
// call function
getHighesBMI();
// value check for debugging purposes
console.log(mark.calcBMI(),john.calcBMI());
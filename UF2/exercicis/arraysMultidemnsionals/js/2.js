"use strict";
/*
Challenge
Un Sudoku es pot considerar un array multidimensional de 9x9, sub-dividit en caixers de 3x3 anomenades regions o blocs. Algunes cel·les estan fixes i tenen un número comprès entre 1 i 9, que possibiliten la solució del sudoku.. L’objectiu és generar un Sudoku amb les cel·les fixes. 

Agafar paper i boli i analitzar quin algoritme es segueix per calcular un sudoku.
Si no ens en sortim, fer recerca per Internet d'un algoritme per calcular Sudokus
Fes un programa en JavaScript que solucioni el següent sudoku.
*/


const originalMat = [
    [0,0,0,0,4,9,0,0,0],
    [7,0,5,0,2,0,3,0,0],
    [1,2,9,3,0,0,0,0,5],
    [0,3,0,8,0,0,5,0,0],
    [2,5,0,4,0,6,0,9,8],
    [0,0,1,0,0,5,0,3,0],
    [8,0,0,0,0,3,9,2,7],
    [0,0,2,0,8,0,6,0,3],
    [0,0,0,2,9,0,0,0,0]
];



const printMatrix = (mat) => {
    let result = "";
    for (let cols = 0; cols < mat.length; cols++) {
        for (let rows = 0; rows < mat[0].length; rows++) {
            result += mat[cols][rows] + "," ;
        }
        result += "\n";
    }
    console.log(result);
} 
printMatrix(originalMat);


// select a number [1-9]
// cehck for definitive position of number
    // no repeat in square
    // no repeat in row
    // no repeat in col
// condition located?
    // no
        // check next number
    // yes
        // add number
            // restart loop

// const norepeatInsquare = (rows,cols,num) => {
//     let range;
//     if (rows < 3 && cols < 3) {
//         range = [0,0,2,2];
//     } else if (rows < 3 && cols < 6) {
//         range = [0,3,2,5];
//     } else if (rows < 3 && cols <= 8) {
//         range = [0,6,2,8];
//     } else if (rows < 6 && cols < 3) {
//         range = [3,0,5,2];
//     } else if (rows < 6 && cols < 6) {
//         range = [3,3,5,5];
//     } else if (rows < 6 && cols <= 8) {
//         range = [3,6,5,8];
//     } else if (rows <= 8 && cols < 3) {
//         range = [6,0,8,2];
//     } else if (rows <= 8 && cols < 6) {
//         range = [6,3,8,5];
//     } else if (rows <= 8 && cols <= 8) {
//         range = [6,6,8,8];
//     }
//     let suqareMatrix = [
//         [0,0,0],
//         [0,0,0],
//         [0,0,0]
//     ];
//     // build sample square matrix 
//         for (let i = 0; i < 3; i++) {
//             for (let j = 0; j < 3; j++) {
//                 suqareMatrix[i][j] = originalMat[range[0] + i][range[1] +j];
//             }
//         }
//     console.log("Number: " + num);
//     printMatrix(suqareMatrix);
//     // check if created square matrix contains num
//     return !suqareMatrix.some(row => row.includes(num));
// }
const norepeatInsquare = (row,col,num,matCopy) => {
    const rangeX = Math.floor(row/3) *3;
    const rangeY = Math.floor(col/3) *3;

    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            if (matCopy[rangeX + i][rangeY +j] == num) {return false;}
        }
    }
    return true;
}

// check for number repeating in row
const rowCheck = (row,num,matCopy) => {
    for (let i = 0; i <= 8; i++) {
        if (matCopy[row][i] == num) {return false}
    }
    return true;
}

// check for number repeating in col
const colCheck = (col,num,matCopy) => {
    for (let i = 0; i <= 8; i++) {
        if (matCopy[i][col] == num) {return false}
    }
    return true;
}

const checkNumsForSquare = () => {
    //let modifyMatt = originalMat.slice(0);
    // only method I've found to copy the original without modifying it
    let modifyMatt = JSON.parse(JSON.stringify(originalMat));
    let counter = 0;
    for (let num = 1; num <= 9 && modifyMatt.some(row => row.includes(0)); num++) {
        console.log(num, modifyMatt.some(row => row.includes(0)));
        let numJustInserted = false;
        for (let rows = 0; rows < modifyMatt.length; rows ++) {
            let noRepSqr = false;
            for (let cols = 0; cols < modifyMatt.length; cols++) {
                numJustInserted = false;
                // if square is empty, check
                if (modifyMatt[rows][cols] == 0) {
                    // if it's the start of a 3x3 matrix check once for non repeating in 3x3
                    if (rows == 0) {
                        if (cols == 0 || cols == 3 || cols == 6) {
                            noRepSqr = norepeatInsquare(rows,cols,num, modifyMatt);
                        }
                    } else if (rows == 3) {
                        if (cols == 0 || cols == 3 || cols == 6 ) {
                            noRepSqr = norepeatInsquare(rows,cols,num, modifyMatt);
                        }
                    } else if (rows == 6) {
                        if (cols == 0 || cols == 3 || cols == 6) {
                            noRepSqr = norepeatInsquare(rows,cols,num, modifyMatt);
                        }
                    }
                    if (noRepSqr && rowCheck(rows,num, modifyMatt) && colCheck(cols,num,modifyMatt)) {
                        modifyMatt[rows][cols] = num;
                        numJustInserted = true;
                        console.log("insert",rows,cols,num);
                    }
                }
                if (numJustInserted) {
                    cols = -1;
                }
            }
            if (numJustInserted) {
                rows = -1;
            }   
        }
        if (numJustInserted){
            console.log("ping");
            numJustInserted = false;
            num = 0;    
        }
        counter++;
        if (num == 9 && modifyMatt.some(row => row.includes(0)) && counter < 10000) {
            num = 0;
        }
    }
    printMatrix(modifyMatt);   
}
checkNumsForSquare();
printMatrix(originalMat);
/*
// check if 2D array includes a number
originalMat.some(row => row.includes(22))
*/

/*
check floor and ceiling functions
Math.floor(x/3) *3 => returns range of the 3x matrix from the x,y postion
*/

/* 
check square cases
0,0     3,0     6,0

0,3     3,3     6,3

0,6     3,6     6,6

// building conditional
if row,col.value == 0

x == 0
    y == 0 || y == 3 || y = 6
x == 3
    y == 0 || y == 3 || y = 6
x == 6
    y == 0 || y == 3 || y = 6

        squareCheck


*/

/*
// squareCheck(row,col,num) // num does not repeat

rangeX = Math.floor(row/3) *3;
rangeY = Math.floor(col/3) *3;

for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
        if (matCopy[rangeX + i][rangeY +j] == num) {return false;}
    }
}
return true;

*/


/*
// row check(row,num)

for (let i = 0; i <= 8; i++) {
    if (matCopy[row][i] == num) {return false}
}
return true;
*/

/*
// col check(col,num)

for (let i = 0; i <= 8; i++) {
    if (matCopy[i][col] == num) {return false}
}
return true;
*/

/*
// writeNum(row,col,num)
if (allConditions) {
    matCopy[row][col] == num;
    numJustInserted = true;
}

*/

/*
loop 1
    numjustinserted true?
        break;
loop 2 
    numjustinserted true?
        break;
loop 3
    num just inserted?
        num =1;
        numJustINserted = false;
*/

/*
// copy

const checkNumsForSquare = () => {
    //let modifyMatt = originalMat.slice(0);
    // only method I've found to copy the original without modifying it
    let modifyMatt = JSON.parse(JSON.stringify(originalMat));
    let counter = 0;
    for (let num = 1; num <= 9 && modifyMatt.some(row => row.includes(0)); num++) {
        console.log(num, modifyMatt.some(row => row.includes(0)));
        let numJustInserted = false;
        for (let rows = 0; rows < modifyMatt.length; rows ++) {
            let noRepSqr = false;
            for (let cols = 0; cols < modifyMatt.length; cols++) {
                numJustInserted = false;
                // 3x3 square matrix
                //    squareArr = ["00","03","06","30","33","36","60","63","66"];
                // if square is empty, check
                if (modifyMatt[rows][cols] == 0) {
                    // if it's the start of a 3x3 matrix check once for non repeating in 3x3
                    //    let checkInSqr = "" + rows + cols;
                    //    if (squareArr.some(el => checkInSqr)) {}
                    //        noRepSqr = norepeatInsquare(rows,cols,num, modifyMatt);
                    //    }
                    if (col % 3 == 0 && row % 3 == 0) {noRepSqr = norepeatInsquare(rows,cols,num, modifyMatt);}
                    if (noRepSqr && rowCheck(rows,num, modifyMatt) && colCheck(cols,num,modifyMatt)) {
                        modifyMatt[rows][cols] = num;
                        numJustInserted = true;
                        console.log("insert",rows,cols,num);
                    }
                }
                if (numJustInserted) {
                    cols = -1;
                }
            }
            if (numJustInserted) {
                rows = -1;
            }   
        }
        if (numJustInserted){
            console.log("ping");
            numJustInserted = false;
            num = 0;    
        }
        counter++;
        if (num == 9 && modifyMatt.some(row => row.includes(0)) && counter < 10000) {
            num = 0;
        }
    }
    printMatrix(modifyMatt);   
}
*/

/* ascended
if (col % 3 == 0 && row % 3 == 0)

*/


if (modifyMatt[rows][cols] == 0) {
    // if it's the start of a 3x3 matrix check once for non repeating in 3x3
    if (rows == 0) {
        if (cols == 0 || cols == 3 || cols == 6) {
            noRepSqr = norepeatInsquare(rows,cols,num, modifyMatt);
        }
    } else if (rows == 3) {
        if (cols == 0 || cols == 3 || cols == 6 ) {
            noRepSqr = norepeatInsquare(rows,cols,num, modifyMatt);
        }
    } else if (rows == 6) {
        if (cols == 0 || cols == 3 || cols == 6) {
            noRepSqr = norepeatInsquare(rows,cols,num, modifyMatt);
        }
    }
}
 

 
 if (col % 3 == 0 && row % 3 == 0) {

 }
 



let range;
     if (rows < 3 && cols < 3) {
         range = [0,0,2,2];
     } else if (rows < 3 && cols < 6) {
         range = [0,3,2,5];
     } else if (rows < 3 && cols <= 8) {
         range = [0,6,2,8];
     } else if (rows < 6 && cols < 3) {
         range = [3,0,5,2];
     } else if (rows < 6 && cols < 6) {
         range = [3,3,5,5];
     } else if (rows < 6 && cols <= 8) {
         range = [3,6,5,8];
     } else if (rows <= 8 && cols < 3) {
         range = [6,0,8,2];
     } else if (rows <= 8 && cols < 6) {
         range = [6,3,8,5];
     } else if (rows <= 8 && cols <= 8) {
         range = [6,6,8,8];
     }

const rangeX = Math.floor(row/3) *3;
const rangeY = Math.floor(col/3) *3;

    2,2
        0 
        0,0
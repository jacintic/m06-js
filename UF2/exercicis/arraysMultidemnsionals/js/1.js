"use strict";
/*
Exercici
Escriu un programa que multipliqui dues matrius de dimensions 2x2 i mostri el resultat per pantalla.

Challenge
Un Sudoku es pot considerar un array multidimensional de 9x9, sub-dividit en caixers de 3x3 anomenades regions o blocs. Algunes cel·les estan fixes i tenen un número comprès entre 1 i 9, que possibiliten la solució del sudoku.. L’objectiu és generar un Sudoku amb les cel·les fixes. 

Agafar paper i boli i analitzar quin algoritme es segueix per calcular un sudoku.
Si no ens en sortim, fer recerca per Internet d'un algoritme per calcular Sudokus
Fes un programa en JavaScript que solucioni el següent sudoku.
*/

/*
Escriu un programa que multipliqui dues matrius de dimensions 2x2 i mostri el resultat per pantalla.
*/

const matrix1 = [
    [1,2],
    [3,4]
];
const matrix2 = [
    [5,6],
    [7,8]
];

const multiplyMatrices = (mat1, mat2) => {
    const resultMat = [
        [0,0],
        [0,0]
    ];

    for (let cols = 0; cols < mat1.length; cols++) {
        for (let rows = 0; rows < mat2[0].length; rows++) {
            for (let elements = 0; elements < mat2[0].length; elements++) {
                resultMat[cols][rows] += mat1[cols][elements] * mat2[elements][rows]; 
            }
        }
    }
    return resultMat;
}


const printMatrix = (mat) => {
    let result = "";
    for (let cols = 0; cols < mat.length; cols++) {
        for (let rows = 0; rows < mat.length; rows++) {
            result += mat[cols][rows] + "," ;
        }
        result += "\n";
    }
    console.log(result);
} 

printMatrix(multiplyMatrices(matrix1,matrix2));


/*
Exemple de referencia de java
public static int[][] multiplyMatrices(int[][] my2Darray, int[][] my2Darray2) {
        // declare the 2D array where the result of the sum of the two 2D arrays will be stored
        int[][] my2Darray3 = new int[my2Darray.length][my2Darray2[0].length];
        // standard for loop nested in another for loop to iterate through the array
        for (int i = 0; i < my2Darray.length; i++) {
            for (int j = 0; j< my2Darray2[0].length; j++) {
                // part of the loop that will su, the product of the two 2D arrays
                for (int k = 0; k < my2Darray[0].length; k++) {
                    my2Darray3[i][j] += my2Darray[i][k] * my2Darray2[k][j];
                }
            }
        }
        // return the array with the result to the main function so it can print it
        return my2Darray3;
    }

*/

/*


5. Amb els mètodes “every” i “some”, fes un programa on donat 
un array, determini si tots els seus elements son de tipus 
number, string, o altre.

5. Amb el mètode indexOf, modifica l’anterior programa per a 
que determini pel cas “altre”, quins índexs d’elements són 
null o undefined.

*/

const origArr = ["1",null,"2",undefined,"3","4","5", "abc"];

// some checks for values null or undefined, logs the index of their position in the original array
origArr.some(currentValue => 
    // condition for not a string, not a number and null (null and undefined)
    // nota per que string no passi he de fer que no sigui string i sigui nan junts
    typeof currentValue != "string" && isNaN(currentValue * -1) || currentValue === null? 
        // log the index of elements matching the criteria
        console.log(origArr.indexOf(currentValue)) : 
        // else do nothing
        null);

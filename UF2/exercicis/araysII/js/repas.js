/*

1. Repassem forEach
// Looping el següent array:
const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
1. Recorrer el següent array amb

    1. Mètode tradicional for
    2. Amb foreach

 1 bis. Per cada iteració mostrar 

    1. Si es > 0 : Has fet un depòsit de XXX
    2. Si es < 0 : Has tret XXX

2. Repassem some

const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];

const movements2 = [430, 1000, 700, 50, 90];

Revisem si es donen les següent condicions:

1) He tret -130€?

2) Hi han hagut dipòsits?

3) Tots els moviments són dipòsits?

*/

// 1.1
console.log("-----------Amb bucle for classic-----------")
const movements = [200, 450, -400, 3000, -650, -130, 70, 1300];
const ml = movements.length;
for(let i = 0; i < ml; i++) {
    const currEl = movements[i];
    currEl > 0 ? console.log(`Has fet un depòsit de ${currEl}`) : currEl == 0 ?  null : console.log(`Has tret ${currEl}`)
}
console.log("---------------------------------");
// 1.2
console.log("-----------Amb forEach-----------")
movements.forEach(el => el > 0 ? console.log(`Has fet un depòsit de ${el}`) : el == 0 ?  null : console.log(`Has tret ${el}`) );

//2
const movements2 = [430, 1000, 700, 50, 90];
//2.1
// -130?
console.log("2.1 -130?")
console.log(movements.some(el => el ==-130));
console.log(movements2.some(el => el == -130));

//2.2
// 0+?
console.log("2.2 some >0?")
console.log(movements.some(el => el > 0));
console.log(movements2.some(el => el > 0));

//2.3
// all > 0 ?
console.log("2.3 All >0?")
console.log(movements.every(el => el > 0));
console.log(movements2.every(el => el > 0));
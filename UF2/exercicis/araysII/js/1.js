/*
1. Amb el mètode “forEach”, fes un programa que reprodueixi el 
següent comportament fins que l’usuari enti un caràcter:

   1. demani un número a l’usuari
   2. el guardi en un array
   3. comprovi si aquest número és el més gran o el més petit de 
   l’array,
   4. si és el més petit, resta aquest valor a tots els elements
   5. si és el més gran, suma aquest valor a tots els elements
   6. informa per pantalla que és el més gran o el més petit, 
   fins el moment
*/
// initialize input set to start program AND not add any value to 
// the array
let input = "start";
// initialize array outside the loop's scope so it can be used
// outside
const myArr = [];
// while exit code is not passed by the user the program will 
// keep running
// exit code is "x"
while (input != "x") {
    // detect non number inputs, avoid the standard procedure 
    // to execute
    switch (isNaN(input * -1)) {
        case true:
            // filters out the exit code ("x") and the start code "start" (do nothing)
            // else it prints an error and warning
            input != "x" && input != "start" ? 
                console.log("Error: Not a number, please insert a valid number") 
                // else do nothing
                : null;
            break;
        // valid number case
        default:
            // convert the input (a number in String format) to numeric 
            // for ease of calculations
            const inpFloat = parseFloat(input);
            // add the number to the array
            myArr.push(inpFloat);
            // condition for number being the maximum (number matches itself
            // as maximum since is the biggest in the array)
             if (Math.max(...myArr) == inpFloat) {
                 console.log(`${input} is max in the array`);
                 // sum input to each element of the array
                myArr.forEach((el,i) =>  myArr[i] = el + inpFloat);
            // condition for minimum
             } else if (Math.min(...myArr) == inpFloat) {
                myArr.forEach((el,i) =>  myArr[i] = el - inpFloat);
                console.log(`${input} is min in the array`);
            }
            // print the array every turn the input is a valid number
             console.log(myArr);
            break;
    }
    input = prompt("introdueix un numero, x per sortir");
}


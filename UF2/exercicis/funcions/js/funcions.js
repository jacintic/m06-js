"use strict";
/*
1.
Programa en Javascript on se li passi  a una funció un paràmetre 
per valor.
*/
let a = "a";
let b = a;
const parametrePerValor = () => {
    console.log(b);
}
parametrePerValor();
// "El valor es 1"

a += "b";
parametrePerValor();
// "El valor es 2"
console.log(a);
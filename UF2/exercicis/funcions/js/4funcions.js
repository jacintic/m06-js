"use strict";
/*
4.
Programa en JavaScript on es calculi l’edat d’un gos. Donat els 
seguents anys de neixement: 1975, 1963, 2000, 2019, 2015. 
*/


const calculaEdat = (anyNeix) => {
    const currYear = new Date().getFullYear();
    console.log( currYear - anyNeix);
}
calculaEdat(1975);
calculaEdat(1963);
calculaEdat(2000);
calculaEdat(2019);
calculaEdat(2015);
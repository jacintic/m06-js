"use strict";
/*
Programa en JavaScript on se li passi a una funció un paràmetre 
per referència.
*/
let x = 10;
const parametrePerReferencia = () => {
    x++;
    console.log("El valor de x es " + x);
}
console.log(x);
parametrePerReferencia();
console.log(x);
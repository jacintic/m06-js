"use strict";
/*
7.
Programa en JavaScript on se li passa a una funció un número 
enter positiu i calcula el seu factorial.
*/
const calculaFactorial = (n) => {
    let result = 1;
    for (let i = 1; i <= n; i++) {
        result *= i;
    }
    console.log(result);
}
calculaFactorial(1);
calculaFactorial(2);
calculaFactorial(3);
calculaFactorial(6);

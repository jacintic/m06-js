"use strict";

/*
Creeu una funció "checkDogs", que accepti 2 arraysd'edats del gos ("dogsJulia" i "dogsKate"), i faci les accions següents:

La Julia es va assabentar que els propietaris dels gossos PRIMER i ÚLTIMS DOS en realitat tenen gats, no gossos. Per tant, 
creeu una còpia superficial de la matriu de Julia i elimineu les edats dels gats d’aquesta matriu copiada (perquè és una mala
     pràctica mutar els paràmetres de la funció)
Creeu un únic array amb les dades de Julia (corregides) i Kate
Per a cada gos que quedi, inicieu sessió a la consola si es tracta d'un adult ("El gos número 1 és adult i té 5 anys") o un 
cadell ("El gos número 2 continua sent un cadell 🐶")
Executeu la funció per als dos conjunts de dades de prova

TIPS
Fer servir casos d'us, exemple:
dades de Julia [3, 5, 2, 12, 7], dades de Kate [4, 1, 15, 8, 3] 
dades de Julia [9, 16, 6, 8, 3], dades de Kate [10, 5, 6, 1, 4]
Es recomana l'us de forEach
Exemple de sortida de dades per consola:
Dog number 1 is an adult, and is 16 years old
Dog number 2 is an adult, and is 6 years old
Dog number 3 is an adult, and is 10 years old
Dog number 4 is an adult, and is 5 years old
Dog number 5 is an adult, and is 6 years old
Dog number 6 is still a puppy 🐶
Dog number 7 is an adult, and is 4 years old
*/


/*
Un gos és adult si té almenys 3 anys i és un cadell si té menys de 3 anys.
*/
const dogsJulia = [3, 5, 2, 12, 7];
const dogsKate = [4, 1, 15, 8, 3];

const dogsJulia2 = [9, 16, 6, 8, 3];
const dogsKate2 = [10, 5, 6, 1, 4];

// these constants will be used to determine if the dog is an adult and to determine the actual age of the dog if it is an adult
const ADULT_CUTOFF = 3;
const DOG_YEAR_PER_HUMAN_YEAR = 7;

const checkAdult = (dogs, nameOwner) => {    
    //log the name of the owner for easier debugging
    console.log(`${nameOwner}'s dogs`);
    // iterate through the array elements
        // keep the element and the index of the element
    dogs.forEach((element,index) => {
        // initialize the string to optimize according the DRY (don't repeat yourself) principle
        // fill in the result string with the data common to both adult and puppy results
            // index + 1 => this translates array position into human readable position
        let result= `Dog number ${index + 1}`;
        // conditional, if adult fill in the array with the adult result info
            // calculate the age of the dog multiplying it by 7 if it is an adult
            // just print a puppy if it is a puppy
        element >= ADULT_CUTOFF ? result += ` is an adult and is ${element * DOG_YEAR_PER_HUMAN_YEAR} years old` : result += ` is still a puppy`;
        // print result for every element
        console.log(result);
    });
}

const checkDogs = (dogsJulia, dogsKate) => {
    // remove cats from julia array
        // first and 2 last
    // make a copy of the array
    // removing first and 2 last elements, storing the result in a copy array without mutating the original array
    const dogsJuliaCopy = dogsJulia.slice(1,dogsJulia.length - 2);
    // printing julia's modified array for debugging purposes
    console.log("Julia's final array (only containing dogs) as according to exercise " + dogsJuliaCopy);
    console.log("Julia's original array non modified: " + dogsJulia);
    // calling a function to check if adult and print the results
    checkAdult(dogsJuliaCopy, 'Julia');
    checkAdult(dogsKate, 'Kate');
}
////// CALLING FUNCTIONS AND PRINTING DEBUGGING CODE//////////
console.log("--------------Printing the original arrays, first set--------------");
console.log("Julia's: " + dogsJulia);
console.log("Kate's: " + dogsKate);

console.log("--------------calling checkDogs function with first set--------------")
checkDogs(dogsJulia,dogsKate);

console.log("--------------Printing the original arrays, second set--------------");
console.log("Julia's: " + dogsJulia2);
console.log("Kate's: " + dogsKate2);

console.log("--------------calling checkDogs function with second set--------------")
checkDogs(dogsJulia2,dogsKate2);

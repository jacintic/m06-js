"use strict";
/////////////////////
///// CONSTANTS /////
/////////////////////
// fragment of the book
const myFragment = `I will here give a brief sketch of the progress of opinion on the Origin of Species. Untilrecently the great majority of naturalists believed that species were immutableproductions, and had been separately created. This view has been ably maintained bymany authors. Some few naturalists, on the other hand, have believed that speciesundergo modification, and that the existing forms of life are the descendants by truegeneration of pre existing forms. Passing over allusions to the subject in the classicalwriters (Aristotle, in his "Physicae Auscultationes" (lib.2, cap.8, s.2), after remarking thatrain does not fall in order to make the corn grow, any more than it falls to spoil the farmer'scorn when threshed out of doors, applies the same argument to organisation; and adds(as translated by Mr. Clair Grece, who first pointed out the passage to me), "So whathinders the different parts (of the body) from having this merely accidental relation innature? as the teeth, for example, grow by necessity, the front ones sharp, adapted fordividing, and the grinders flat, and serviceable for masticating the food; since they werenot made for the sake of this, but it was the result of accident. And in like manner as toother parts in which there appears to exist an adaptation to an end. Wheresoever,therefore, all things together (that is all the parts of one whole) happened like as if theywere made for the sake of something, these were preserved, having been appropriatelyconstituted by an internal spontaneity; and whatsoever things were not thus constituted,perished and still perish. "We here see the principle of natural selection shadowed forth,but how little Aristotle fully comprehended the principle, is shown by his remarks on theformation of the teeth.), the first author who in modern times has treated it in a scientificspirit was Buffon. But as his opinions fluctuated greatly at different periods, and as hedoes not enter on the causes or means of the transformation of species, I need not hereenter on details.`;
// alphabet
const alphabet = [];
// global words record
const wordsPerLetter = {};

class Llibre {
    // constructor
    constructor (book, author, fragment) {
        this.book = book;
        this.author = author;
        this.fragment = fragment;
    }
    ////// METHODS//////
    // ITERATE THROUGH PHRASE METHOD (2)
    recorrerFrase2() {
        // split fragments by phase (. as limiter)
        const phraseDotSplit = this.fragment.split(".");
        // phrase counter 
        let countPhrases = 0;
        phraseDotSplit.forEach(phrase => {
            // increment phrase counter
            countPhrases++;
            // split by word
            const word = phrase.split(" ");
            // temporal show object with words by letter
            const wordStore = Object.assign({}, wordsPerLetter);
            // reset object for each phrase
            alphabet.forEach(letter => {
                // set alphabet as key
                wordStore[letter] = {};
            });
            word.forEach(word => {
                const wordLower = word.toLowerCase();
                // commas, points and other modifiers controll
                //// example 'word,' => 'word'
                const letterWord = wordLower[0];
                if (alphabet.includes(letterWord)) {
                    if(wordStore[letterWord].hasOwnProperty(wordLower)) {
                        wordStore[letterWord][wordLower] = parseInt(wordStore[letterWord][wordLower]) + 1; 
                    } else {
                        wordStore[letterWord][wordLower] = 1;
                    }
                    // same but with global object
                    if(wordsPerLetter[letterWord].hasOwnProperty(wordLower)) {
                        wordsPerLetter[letterWord][wordLower] = (parseInt(wordsPerLetter[letterWord][wordLower]) + 1); 
                    } else {
                        wordsPerLetter[letterWord][wordLower] = 1;
                    }
                }
                
            });
            console.log(`Phrase ${countPhrases}:`)
            console.log(phrase);
            console.log(wordStore);
        });
        // print global object
        console.log('Whole word collection');
        console.log(wordsPerLetter);
    }
    // shows the fragment of the book through console
    showFragment() {
        return this.fragment;
    }
}
// 3.0
////////  GET TOP 3 WORDS //////
const getTopWords = (topN) => {
    // store top values in these 3 variables
    let maxValues = [2,2,2];
    // object in which top words and its repetition values will be stored
    const topWords = {};
    // get the top 3 values of most repeated words
    Object.entries(wordsPerLetter).forEach(([letter, words]) => {
        Object.entries(words).forEach(([word, value]) => {
            if (value > maxValues[topN - 1]) {
                for (let i = 0; i < (topN - 1); i++) {
                    maxValues[i] = maxValues[i + 1]
                }
                maxValues[topN - 1] = value;
            };
        });
    });
    // get words with most repeated value which we obtained in the past iteration
    Object.entries(wordsPerLetter).forEach(([letter, words]) => {
        Object.entries(words).forEach(([word, value]) => {
            for (let i = 0; i < topN; i++) {
                if (value == maxValues[i]) {
                    topWords[word] = value;
                }
            }
        });
    });
    console.log(`These are the top words and it's word count:`);
    console.log(topWords);
}
// 2.0.-1 generate global object word/letter count
//// Format example: 
///////  {a: {}, b : {},(...)}
/////////// HOW IT WILL LOOK IN THE END:
////////////////{a: {arbre: 1, amaca: 2}, b : {bola:1, bo:2},(...)}
/////////////// GENERATE GLOBAL WORD COUNTER //////////////
const genGlobal = () => {
    alphabet.forEach(letter => {
        // set alphabet as key
        wordsPerLetter[letter] = {};
    });
}
// 2.0.-1 generate alphabet
/////////////// GENERATE ALPHABET //////////////
const genAlphabet = () => {
    // begining letter
    const beginniing = 'a'.charCodeAt(0);
    // end letter
    const end  = 'z'.charCodeAt(0);
    // push each letter to alphabet array
    for (let i = beginniing; i <= end; ++i) {
        alphabet.push(String.fromCharCode(i));
    }
}
///////////////////////////////////////
////////// CLICK FUNCTION INIT ////////
///////////////////////////////////////
const inicia1 = () => {
    // create a Class with the current fragment
    const darwin = new Llibre('On the Origin of the Species', 'Charles Darwin',myFragment);
    //
    // create abecedary array
    genAlphabet();
    // initialize global object keys (alphabet letters)
    //// values => object
    genGlobal();
    // show array
    console.log("Alfabet: " + alphabet);
    // prints the whole text
    darwin.showFragment();
    // initiates phrase filter function and shows local (object for each phrase) and global object (once iterated through all the prhases)
    darwin.recorrerFrase2();
    // find top repeated words (the 3 indicates get top 3 words)
    getTopWords(3);
}
// button element
const button_ = document.getElementById("myButton");
//////// CLICK EVENT FOR BUTTON ////////
button_.addEventListener('click',inicia1);
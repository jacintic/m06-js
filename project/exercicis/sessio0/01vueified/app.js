const app = Vue.createApp({
  data() {  // sempre hem de fer servir la funció de Vue que es diu data() .. que busca aquesta propietat
    return {
      // declare value linked to input text content, make input text empty
      // it will also send and recieve text from the item
      // right now it is sending '' to the input element
      // it is also being declared
      textItem: '',
      // array to store the list item's text content
      itemsArr: [],
    }
  },
  methods: {
    // method called by the submit button on click
    addItem() {
      // add element to the array
      this.itemsArr.push(this.textItem);
      // reset input
      this.textItem = '';
    }
  }
})
app.mount('#myApp') //amb això connectem la nosrtra aplicacio de JS amb la seccio del html.


# Exercici de documentació  
  
## Com sempre fem servir un Markdown per documentar les següent preguntes:  
  
1. Quan cridem a vue desde qualsevol html, quan fem servir un codi o l'altre?.
    ```
    <!-- <p v-html="outputGoal()"></p> -->
    <p>{{ outputGoal() }}</p> <!--Mostrem directament el valor -->
    ```
* v-html
Es para insertar contenido html (que incluye formato html como tags, etc)
* {{}}
Es para texto puro ya sean strings o numericos, pero no con tags html ya que lo mostrara como texto interpretado (utilizando html entities) que no formara elementos html.  

2. Quin tipus de codi podem executar entre {{ }}.  
Variables, metodes y operacions (com variableName + 3).  

3. Que es vanilla javascript?.
Javascript sin utilizar librerias externas, ni frameworks.  

4. Quins son els listeners en vue?.
* v-on-click
    * .stop
    * .prevent
    * .cature
    * .self
    * .once
    * .passive
    * .exact: 
        * example => v-on-click.ctrl.exact (fired if ONLY click + ctrl is pressed, not if ctrl + alt + click for example)
* v-on:keyup
    * .keyvalue
    * .keyCode (example .13)
    * predefined key values: 
        * .enter
        * .tab
        * .delete (captures both “Delete” and “Backspace” keys)
        * .esc
        * .space
        * .up
        * .down
        * .left
        * .right
    * key modifiers: 
        * .ctrl
        * .alt
        * .shift
        * .meta

5. Comparteix alguna cosa del Vue, que t'hagi sorprès i/o agradat i/o que no t'hagi agradat gens. 
  
M'han dit que vue internament decideix quins elements ha de re-rendejar i quins no, per exemple, a l'exercici 1. Tenim una llista i podem afegir elements. Un intueix que vue esta re-renderitzant tota la llista cada cop, pero sembla ser, que Vue nomes afegeix els elements nous sense chafar els nous, o lo que es lo mateix, optimitza el renderitzatge d'elements.  
No m'ha agradat que els elments {{}} esmostran per un moment avans de renderitzar la pagina.
No m'ha agradat utilitzar sintaxy Vue/JS dintre de l'html (com per exemple v-on-click), encara que em sembla que cap llibreria o framework esta exempt d'aquesta practica.  
No m'agrada que hi hagin casi nomes 1/4 part de treballs ofertats per a Vue que de React.  
Em sembla que li tinc massa carinyo a Vanilla javascript i les seves "bones practiques" per començar a confiar a un sistema de re-render d'elements que ho fa tot ell mateix i que m'exigeix que fiqui sintaxi Vue dintre del codi HTML (lo qual es considera pecat dintre del mon mes purista de HTML i javascript). Pero entenc que no hi ha alternativa als frameworks/llibreries en el mon professional, i per tant li dono una oportunitat a Vue i a que em combençi.

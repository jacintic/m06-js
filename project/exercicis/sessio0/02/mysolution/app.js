const app = Vue.createApp({
    data() {  // sempre hem de fer servir la funció de Vue que es diu data() .. que busca aquesta propietat
      return {
        name : 'My Name',
        age : 34,
        randNum : Math.round(Math.random()),
        myImgUrl : 'https://miro.medium.com/max/850/0*BRrBBgjCGAzRniV8.jpg',
        populateInput : 'Hello from Vue data()',
        }
    }
  })
  app.mount('#assignment') //amb això connectem la nosrtra aplicacio de JS amb la seccio del html.
  
  
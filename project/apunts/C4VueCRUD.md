# Vue components  
  
:key="friend.id"  
@onclick
v-if
{{ detailsAreVIsible ? 'Hide' : 'Show' }} Details  
  
toggleDetaisl(id) => friend.id  
  
## Components  
  
Codi encapsulat que reutilitzarem. HTML blocks. Split an Application in smaller chunks.  
A component is a feature in Vue.  
`app.component('firend-contact');` <= use 2 words separated by dash to avoid element collission.
```
app.component('friend-component',{
	template: `
	<div>HTMLcomponent here</div>
	`,
	data() {
		return {detailsArevisbile: false};
	
	},
	methids: (...)
});
```
Back in the html =>  
```
<freind-component></friend-component>
```
Every component, has its own variables so toggle visible will only work on a single component and not all at once.  


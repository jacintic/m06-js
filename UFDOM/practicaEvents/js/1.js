/*


    1. Desenvolupa el codi per a utilitzar els esdeveniments click i dblclick en dos botons diferents.

    2. Fes un programa en Javascript on donat un botó qualsevol quan es cliqui el botó del mig, de l’esquerra o la dreta mostri a l’usuari un missatge amb alguna de les propietats de l’esdeveniment. Mostra un altre missatge amb una propietat diferent quan es deixi anar el botó que prèviament ha premut.

    3. Fes un programa en Javascript on donat un botó qualsevol el text del botó canviï a:

        1. “S’ha clicat al botó” quan es cliqui, i

        2. quan es deixa de clicar torni al seu valor original. 

    4. Fes un programa en Javascript on donat un botó qualsevol el text del botó canviï a:

        1. El ratolí ha entrat quan el ratolí passi per sobre del botó, i

        2. El ratolí ha marxat quan el ratolí surti del botó.

Esdeveniments del teclat

    5. Fes un programa en JavaScript on donat un camp d’entrada, l’usuari ha d’escriure una tecla i seguidament mostri per consola quina tecla ha estat. 

    6. Amplia l’anterior exercici per a cobrir les tecles ALT, CTRL, SHIFT i ESC.

Esdeveniments HTML

    7. Fes un programa en JavaScript que al carregar la pàgina llanci un esdeveniment per a dir-te hola, i al tancar-la, tornar-la a carregar o canviar de pàgina, et digui adéu.

*/

/*
1. Desenvolupa el codi per a utilitzar els esdeveniments click i dblclick en dos botons diferents.

*/
const bt1 = document.getElementById("bt1");
const bt2 = document.getElementById("bt2");

bt1.addEventListener("click", () => alert("click"));
bt2.addEventListener("dblclick", () => alert("double click"));


/*
2. Fes un programa en Javascript on donat un botó qualsevol quan es cliqui 
el botó del mig, de l’esquerra o la dreta mostri a l’usuari un missatge 
amb alguna de les propietats de l’esdeveniment. Mostra un altre missatge 
amb una propietat diferent quan es deixi anar el botó que prèviament ha premut.
*/

const btMult = document.getElementById("btMult");

bt1.addEventListener("click", () => alert("click"));
bt2.addEventListener("dblclick", () => alert("double click"));


class MiddleButton {
    handleEvent(event) {
        if (event.button == 0 || event.button == 1 || event.button == 2) {
            console.log("Has clickat el botó " + (event.button == 0 ? 'esquerre' : event.button == 1 ? 'mitja' : 'dret'));
        }
        console.log(event.button)
    }
}


const mouseUp = (event) => {
    console.log("Boto deixat de pitjar")
    console.log(event.type)
}

let mB1 = new MiddleButton();
btMult.addEventListener('mousedown', mB1);

btMult.addEventListener('mouseup', mouseUp);

/*
3. Fes un programa en Javascript on donat un botó qualsevol el text del botó canviï a:

1. “S’ha clicat al botó” quan es cliqui, i

2. quan es deixa de clicar torni al seu valor original. 
*/
const btCh = document.getElementById('btCh');
let origTxt;
const btPreTxt = (event) => {
    origTxt = event.target.textContent;
    console.log(event.target.textContent =  "S’ha clicat al botó");
} 
const btPostTxt = (event) => {
    console.log(event.target.textContent = origTxt);
} 

btCh.addEventListener('mousedown', btPreTxt);

btCh.addEventListener('mouseup', btPostTxt);

/*
4. Fes un programa en Javascript on donat un botó qualsevol el text del botó canviï a:

        1. El ratolí ha entrat quan el ratolí passi per sobre del botó, i

        2. El ratolí ha marxat quan el ratolí surti del botó.
*/




const btOv = document.getElementById('btOver');

const btOver = (event) => {
    event.target.textContent =  "El ratolí ha entrat";
} 

const btOut = (event) => {
    event.target.textContent = 'El ratolí ha marxat';
} 

btOv.addEventListener('mouseover', btOver);

btOv.addEventListener('mouseout', btOut);


/*
5. Fes un programa en JavaScript on donat un camp d’entrada, 
l’usuari ha d’escriure una tecla i seguidament mostri per 
consola quina tecla ha estat. 
*/



const textInput = document.getElementById('textIn');

const getKey = (event) => {
    console.log(event.data);
}

textInput.addEventListener('input', getKey);

/*
6. Amplia l’anterior exercici per a cobrir les tecles ALT, CTRL, SHIFT i ESC.

Esdeveniments HTML

*/
const specialKeys = (event) => {
    switch(event.code) {
        case 'ControlLeft':
            console.log(event.code);
            break;
        case 'AltLeft':
                console.log(event.code);
                break;
        case 'ShiftLeft':
            console.log(event.code);
            break;
        case 'Escape':
            console.log(event.code);
            break;
        default:
            break;
    }
}

textInput.addEventListener('keydown', specialKeys);


/*
7. Fes un programa en JavaScript que al carregar la 
pàgina llanci un esdeveniment per a dir-te hola, i al 
tancar-la, tornar-la a carregar o canviar de pàgina, 
et digui adéu.
*/
window.addEventListener('DOMContentLoaded', () => alert('hola'));



// nota el evento "beforeunload" no permite alert, solo console.log
window.addEventListener("beforeunload", function (event) {
    console.log("adeu");
    event.preventDefault();
});
/*
1. Nom: L’usuari ha de poder escriure el seu nom. Ha de tenir 
una longitud de 40, però com a molt ha de poder escriure 30 
caràcters.

2. 1er Cognom: L’usuari ha de poder escriure el seu cognom. 
Ha de tenir una longitud de 80, però com a molt ha de poder 
escriure 60 caràcters.

3. 2on Cognom: L’usuari ha de poder escriure el seu cognom. 
Ha de tenir una longitud de 80, però com a molt ha de poder 
escriure 60 caràcters.

4. Tipus de document: DNI/NIE
    a) Si es DNI: L’usuari ha de poder escriure el seu número 
    i lletra del DNI. Ha de tenir una longitud de 10, i com a 
    molt ha de poder escriure 9 caràcters.
    b)Si es NIE: L’usuari ha de poder escriure la lletra seguida 
    de 7 números.

5. Ets: Home / dona

6. Data naixement. Format dd/mm/aaaa.
No t’has de poder registrar si ets menor de 18 anys.

7. Nivell d’estudis. llistat Opcions: Educació secundària obligatòria, 
Programes de formació i inserció, Batxillerat, Formació professional, 
Ensenyaments artístics superiors, Ensenyaments esportius, Idiomes a 
les EOI, Educació d'adults, Cursos per accedir a cicles, Itineraris 
formatius específics, Estudis estrangers

8. Situació laboral: estudiant, aturat, treball compte propi, treball 
compte aliè, pensionista.

9. Nacionalitat :espanyola, UE, FUE

10. Pais: Busca un llistat de Països per Internet.
*/

/*
4. Tipus de document: DNI/NIE
    a) Si es DNI: L’usuari ha de poder escriure el seu número 
    i lletra del DNI. Ha de tenir una longitud de 10, i com a 
    molt ha de poder escriure 9 caràcters.
    b)Si es NIE: L’usuari ha de poder escriure la lletra seguida 
    de 7 números.
*/

const globalScope = {
    accessKeyVerified : false,
    conditionsAccepted : false,
    livesInBarcelona : false
}


/*
///////// ATENCIÓ VALIDA DNI I NIE D'ACORD AMB: 
http://www.interior.gob.es/web/servicios-al-ciudadano/dni/calculo-del-digito-de-control-del-nif-nie
I
https://ejemplos.net/ejemplos-de-nie/
*/
const dniValidation = ['T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'];


const myDoc = document.getElementById('docType');

const validateDocs = (docnums, letter) => {
    return dniValidation[docnums % 23] == letter;
}
const docMod = document.getElementById('doc'); 
const setLengthDoc = () => { 
    let result = false;
    
    let myLength = 9;
    if (myDoc.value == 'nie') {
        myLength = 9;
        if (docMod.value.match(/[X-Z]{1}[0-9]{7}[A-Z]{1}/)){
            const nieCtrl = ['X','Y','Z'];
            const strgBuildNie = nieCtrl.indexOf(docMod.value.substring(0,1)) + docMod.value.substring(1,8);
            result = validateDocs(strgBuildNie,docMod.value.substring(8,9));
            //console.log(strgBuildNie , docMod.value.substring(8,9), result, dniValidation[strgBuildNie % 23]);
        }
    } else {
        result = validateDocs(docMod.value.substring(0,8),docMod.value.substring(8,9));
        //console.log(docMod.value.substring(0,8),docMod.value.substring(8,9));
    }
    docMod.maxLength = myLength;
    const docWarn = document.getElementById('docWarn');
    if (!result && !docWarn.classList.contains('visible')) {
        docMod.setCustomValidity("Invalid field.");
        docWarn.classList.add('visible');
    } else if (result && docWarn.classList.contains('visible')){
        docMod.setCustomValidity("");
        docWarn.classList.remove('visible');
    }
}

myDoc.addEventListener('blur', setLengthDoc);
docMod.addEventListener('blur', setLengthDoc);




/*
6. Data naixement. Format dd/mm/aaaa.
No t’has de poder registrar si ets menor de 18 anys.
*/
const myAge = document.getElementById('age');

const verifyAge = () => {
    // get value in yyyy-mm-dd format
    const myAgeVal = myAge.value;
    // get year
    const year = myAgeVal.substring(0,4);
    const month = myAgeVal.substring(5,7);
    const day = +myAgeVal.substring(8,10);
    // get current date
    const date = new Date();
    const myDay = date.getDate();
    const myMonth = date.getMonth() + 1;
    const myYear = date.getFullYear();
    // calculate age
    let age = myYear - year;
    if (myMonth < month || myMonth == month && day > myDay) {
        age--;
        
    }
    // error message
    const errorAge = document.getElementById('errAge');
    // apply red outline to form if age is lower than 18
    if (age < 18) {
        myAge.setCustomValidity("Invalid field.");
        
        errorAge.classList.add('checked');
    } else {
        myAge.setCustomValidity("")
        if (errorAge.classList.contains('checked')) {
            errorAge.classList.remove('checked');
        }
        
    }
    myAge.min = (myYear - 18) + "-" + myMonth + "-" +  myDay;
}

myAge.addEventListener('change', verifyAge);


/*
7. Nivell d’estudis. llistat Opcions: Educació secundària obligatòria, 
Programes de formació i inserció, Batxillerat, Formació professional, 
Ensenyaments artístics superiors, Ensenyaments esportius, Idiomes a 
les EOI, Educació d'adults, Cursos per accedir a cicles, Itineraris 
formatius específics, Estudis estrangers
*/
// get parent element
const mySelect = document.getElementById('studies');

// define array of select elements
const studiesArr = ['Educació secundària obligatòria', 
    'Programes de formació i inserció', 
    'Batxillerat', 
    'Formació professional', 
    'Ensenyaments artístics superiors', 
    'Ensenyaments esportius', 
    'Idiomes a les EOI', 
    `Educació d'adults`, 
    'Cursos per accedir a cicles', 
    'Itineraris formatius específics', 
    'Estudis estrangers'];

// add elements to select
const processEl = (el, index, elem) => {
    const  opt = document.createElement('option');
    opt.value = el;
    const text = document.createTextNode(el);
    opt.appendChild(text);
    elem.appendChild(opt);
}
// iterat through elements array building select options
studiesArr.forEach((el,index) => processEl(el, index,mySelect));

/*
8. Situació laboral: estudiant, aturat, treball compte propi, treball 
compte aliè, pensionista.
*/

// get parent element
const selSit = document.getElementById('situacio');

// situacio array
const sitArr = ['estudiant', 
    'aturat', 
    'batxillerat', 
    'treball compte propi', 
    'treball compte aliè', 
    'pensionista'];

 // iterat through elements array building select options
sitArr.forEach((el,index) => processEl(el, index,selSit));

/*
9. Nacionalitat :espanyola, UE, FUE
*/
// get parent element
///////// EDITED TO AVOID DUPLICATES /////
/*
const nationality = document.getElementById('nationality');

// situacio array
const natArr = ['espanyola', 
    'UE', 
    'FUE',];

 // iterat through elements array building select options
natArr.forEach((el,index) => processEl(el, index,nationality));
*/

/*
10. Pais: Busca un llistat de Països per Internet.
*/
// get parent element
const country = document.getElementById('country');

// countries array
const countries = [
'Albania',
'Andorra',
'Armenia',
'Austria',
'Azerbaijan',
'Belarus',
'Belgium',
'Bosnia and Herzegovina',
'Bulgaria',
'Croatia',
'Cyprus',
'Czech Republic',
'Denmark',
'Estonia',
'Finland',
'France',
'Georgia',
'Germany',
'Greece',
'Hungary',
'Iceland',
'Ireland',
'Italy',
'Kosovo',
'Latvia',
'Liechtenstein',
'Lithuania',
'Luxembourg',
'Macedonia',
'Malta',
'Moldova',
'Monaco',
'Montenegro',
'The Netherlands',
'Norway',
'Poland',
'Portugal',
'Romania',
'Russia',
'San Marino',
'Serbia',
'Slovakia',
'Slovenia',
'Spain',
'Sweden',
'Switzerland',
'Turkey',
'Ukraine',
'United Kingdom',
'Vatican City'
];

 // iterat through elements array building select options
countries.forEach((el,index) => processEl(el, index,country));

/*
Dades de contacte

1. Email: L’usuari ha de poder escriure el seu email.
*/

// validate email
const myMail = document.getElementById('email');
const mailWarn = document.getElementById('mailWarn');

const setInvalid = (valid = true) => {
    if (valid && !mailWarn.classList.contains('invalid')) {
        mailWarn.classList.add('invalid');
        myMail.setCustomValidity("Invalid field.");
    } else if (!valid && mailWarn.classList.contains('invalid')) {
        mailWarn.classList.remove('invalid');
        myMail.setCustomValidity("");
    }

}

const verifyMail = () => {
    const mailTxt = myMail.value == '' ? 'Is null' : myMail.value;
    const myValue = mailTxt.match(/[^@]+@[^@]+\.\D{2,3}/) != null ? mailTxt.match(/[^@]+@[^@]+\.\D{2,3}/)[0] : 'Is null';
    myValue != mailTxt ? setInvalid(true): setInvalid(false);
}

myMail.addEventListener('blur', verifyMail);

/*
2. Telèfon: L’usuari ha de poder escriure el seu número de telèfon mòbil (sense +34). 
Ha de tenir una longitud de 10 però com a molt a de poder escriure 9 caràcters.
*/

// DONE WITH HTML

/*
3. Vius a Barcelona: SI/NO
    a) Si la opció és No, no mostrar res.
    b) Si la opció és si, mostrar el següent formulari:
*/

const liveInBar = document.getElementById('yesBar');
const noBar = document.getElementById('noBar');

const showHide = (visible) => {
    const myDivBar =  document.getElementById('bar');
    if (visible) {
        myDivBar.classList.add('visible');
        globalScope.livesInBarcelona = true;
        //fillStreets();
    } else {
        myDivBar.classList.remove('visible');
        globalScope.livesInBarcelona = false;
    }
}

const setVisible = () => {
    liveInBar.checked ? showHide(true): showHide(false);
}

liveInBar.addEventListener('click', setVisible);
noBar.addEventListener('click', setVisible);
/*
De moment afegim tots els camps, però els mostrarem amb dades de proves.
Més endavant, els omplirem fent servir la API RESTful de GeoBCN, mapes i 
serveis d'informació geoespacial de la ciutat de Barcelona.
*/

/*
Clau d'accés i nota legal

    * Valida que la clau d’accés i la confirmació sigui la mateixa.
    * 
    * El formulari no s’enviarà si la opció He llegit, comprenc la política de privacitat no està seleccionada.
*/

const accK = document.getElementById('accessKey');
const confA = document.getElementById('confAcc');
const keyWarn = document.getElementById('keyWarn');

const verifyKey = () => {
    if (confA.value != accK.value && !keyWarn.classList.contains('visible')) {
        keyWarn.classList.add('visible');
        globalScope.accessKeyVerified = false;
    } else if (confA.value == accK.value) {
        
        globalScope.accessKeyVerified = true;
        if (keyWarn.classList.contains('visible')) {
            keyWarn.classList.remove('visible');
        }
    }

}           

confA.addEventListener('blur', verifyKey);



/*
Botó Registrar
El formulari s’enviarà via servei SERVEI POST. Investiguem y documentar amb el que coneixem, com ho podem fer. Més endavant o implementarem.


Feu servils estils per donar-li una mica de disseny i per millorar l'aparença. 

*/

/*
const globalScope = {
    accessKeyVerified : false,
    conditionsAccepted : false
}
*/

const mySubm = document.getElementById('mySubm');
const conditions = document.getElementById('conditions');

const setTerms = () => {
    if (!conditions.checked && !condWarn.classList.contains('visible')) {
        condWarn.classList.add('visible');
        globalScope.conditionsAccepted = false;
        conditions.setCustomValidity("Invalid field.");
    } else if (conditions.checked ) {
        if (condWarn.classList.contains('visible')) {
            condWarn.classList.remove('visible');
        }
        globalScope.conditionsAccepted = true;
        conditions.setCustomValidity("");
    }
    
}

mySubm.addEventListener('click', setTerms);
conditions.addEventListener('click', setTerms);


/*

A) Abans de carregar la web, omple els següents elements via Javascript:

    1. Afegir als tipus de document existents, l'opció passaport.
*/

// on document load (called from body inside HTML)
const doOnLoad = () => {
    // add passport to documents input
    fillInDoc();
    // add studies to studies input
    fillInStudies();
    // add status
    fillInStatus();
    // fill in nationality
    fillInNat();
    // add streets
    ///// DOESN'T TRIGGER ON LOAD CAUSE DEFAULT OPTION IS NOT CHECKED ON RESET
    //fillStreets();
}

const createAndAppendNodes = (myParent,childType,text) => {
    // append all the elements with a loop
    text.forEach(el => {
        // create option element (child)
        const inputItem = document.createElement(childType);
        // create text element
        const inputText = document.createTextNode(el);
        // append text to node
        inputItem.appendChild(inputText);
        // append value
        inputItem.value = el.split(' ')[0].toLowerCase();
        // append node to parent node already loaded
        myParent.appendChild(inputItem);
    });
}
/*

A) Abans de carregar la web, omple els següents elements via Javascript:

    1. Afegir als tipus de document existents, l'opció passaport.
*/

const fillInDoc = () => {
    // get the input element (parent)
    const myParent = document.getElementById('docType');
    // call function to create and append element
    createAndAppendNodes(myParent, 'option', ['Passport']);    
}
/*
    2. Omplir la llista d’opcions de Nivells d’estudis via el DOM de Javascript:
    Educació secundària obligatòria, Programes de formació i inserció, Batxillerat, Formació professional, Ensenyaments artístics superiors, Ensenyaments esportius, Idiomes a les EOI, Educació d'adults, Cursos per accedir a cicles, Itineraris formatius específics, Estudis estrangers
*/
const fillInStudies = () => {
   // get the parent element
    const studiesEl = document.getElementById('studies');
    // define input text array
    const studiesArr = [`Educació secundària obligatòria`,`Programes de formació i inserció`,`Batxillerat`,`Formació professional`,`Ensenyaments artístics superiors`,`Ensenyaments esportius`,`Idiomes a les EOI`,`Educació d'adults`,`Cursos per accedir a cicles`,`Itineraris formatius específics`,`Estudis estrangers`];
// call function to create and append element
    createAndAppendNodes(studiesEl, 'option', studiesArr);    
}
/*
    3. Afegir la llista d’opcions de Situació laboral via el DOM de Javascript.
*/
const fillInStatus = () => {
    // get the parent element
    const situacioEl = document.getElementById('sitLab');
     // define input text array
    const statusArr = ['Employed','Unemployed','Retired'];
 // call function to create and append element
    createAndAppendNodes(situacioEl, 'option', statusArr);    
}
/*
    4. Afegir les opcions del check Nacionalitat via el DOM de Javascript 
    espanyola, UE, FUE
*/
const fillInNat = () => {
    // get the parent element
    const natParent = document.getElementById('nationality');
     // define input text array
    const natArr = ['Espanyola', 'UE', 'FUE'];
 // call function to create and append element
    createAndAppendNodes(natParent, 'option', natArr);    
}


/*
    5. Si vius a Barcelona = si
        a) Mostrar totes les dades del carrerer de Barcelona.
        Tipus Via, adreça, via, num ….
        b) Omplir el Tipus de Via i adreça amb la següent API de Javascript.
https://w33.bcn.cat/geoBCN/exemples/0.4/cercadors.aspx
Si vius a Barcelona= no
No mostrar informació de carrerer
*/

//// NOTE FUNCTION IS CALLED ON EVENT HANDLER DEFINED IN PREVIOUS EXERCISE
//////// function name => showHide

/// REFACTOR USING STREET API
/*
const fillStreets = () => {
    // get the parent element
    const streetParent = document.getElementById('streetType');
     // define input text array
    const typeOfStreet = [`Apartat`,`Avinguda`,`Baixada`,`Barranc`,`Barriada`,`Camí`,`Canal`,`Carrer`,`Carreró`,`Carretera`,`Cinturó`,`Costa`,`Davallada`,`Dic`,`Drecera`,`Entitat Autoidentificant`,`Era`,`Escales`,`Escullera`,`Espigó`,`Estació`,`Estació Metro`,`Font`,`Galeria`,`Gran Via`,`Grup`,`Habitacle`,`Jardí`,`Jardins`,`Mercat`,`Mirador`,`Moll`,`Nus`,`Pantalà`,`Parc`,`Pas`,`Passadís`,`Passatge`,`Passeig`,`Pati`,`Pavelló`,`Pla`,`Plaça`,`Placeta`,`Platja`,`Polígon`,`Pont`,`Port`,`Porta`,`Pòrtics`,`Pujada`,`Racó`,`Rambla`,`Rec`,`Riera`,`Ronda`,`Rotonda`,`Saló`,`Sector`,`Sèquia`,`Torrent`,`Travessera`,`Travessia`,`Túnel`,`Urbanització`,`Via`,`Via Ferrocarril`,`Viaducte`,`Vial`];
    // check if the type of streets are already loaded
    if (streetParent.children.length < 2 && globalScope.livesInBarcelona) {
        // call function to create and append element
        createAndAppendNodes(streetParent, 'option', typeOfStreet);
    }
}
*/






/*
B) Al enviar el Formulari, heu de confeccionar un mail de confirmació d’alta amb un resum de les dades.  
A l’executar el codi javascript mailto s’obrirà el gestor de correu predeterminat amb el detall  
l’assumpte del missatge.
*/


function getMailtoUrl(to, subject, body) {
    var args = [];
    if (typeof subject !== 'undefined') {
        args.push('subject=' + encodeURIComponent(subject));
    }
    if (typeof body !== 'undefined') {
        args.push('body=' + encodeURIComponent(body))
    }

    var url = 'mailto:' + encodeURIComponent(to);
    if (args.length > 0) {
        url += '?' + args.join('&');
    }
    return url;
}

const isValidated = () => {
    if (globalScope.accessKeyVerified && globalScope.conditionsAccepted) {
        return true;
    }
    return false;

}

const getMail = () => document.getElementById('email').value;
let result = '';
const getBody = () => {
    const inputs = document.querySelectorAll('input[type=text],select,input[type=radio],input[type=age],input[type=checkbox]');
    let result = '';
    inputs.forEach(el => {
        // check if form element is visible (live in barcelona?)
        if (el.offsetParent != null) {
            const myLabel = el.labels[0].textContent;
            // formatting the output
            let repetition = 25 - myLabel.length < 0? 1 : 25 - myLabel.length;
            // special formatting for radio buttons (label is no longer desirable)
            if(el.type != 'radio') {
                result += myLabel +  " ".repeat(repetition) +  (el.value != ''?el.value: '<empty>' )  + "\n";
            // only use checked radio buttons
            } else if (el.checked){
                repetition = 25 - el.name.length < 0? 1 : 25 - el.name.length;
                result += el.name +  " ".repeat(repetition) +  (el.value != ''?el.value: '<empty>' )  + "\n";
            }
        }
    });
    // replace 'on' with a checkmark
    result = result.replaceAll(' on',' ✓')
    return result;
    //console.log(result);
}
const  logSubmit = (event) => {
    if (isValidated()) {
    // function that handles write mail
        const mail = document.createElement("a");
        const myMail = getMail() != ''? getMail(): 'no mail given';
        const body = getBody();
        const myUrl = getMailtoUrl(myMail, 'Data summary confirmation.', body)
        mail.href = myUrl;
        mail.click();
    } else {
        event.preventDefault();
    }
}

const form = document.getElementById('form');
form.addEventListener('submit', logSubmit);




/*
Model d’objectes del Document
Fes un programa en Javascript que recorri tots els nodes de la pàgina web HTML que tingui un “title” 
al “head” i tres paràgrafs amb text al “body”. A cada paràgraf afegeix un link diferent.
Per a cada node has de mostrar el seu nom, el tipus i el valor. 

*/
// gets all elements with a non empty title attribute
const myTitleCollection = document.querySelectorAll(`[title]:not([title=""])`);
console.log(myTitleCollection);

// gets title element inside head
const headTitle = document.querySelector('head > title');
console.log(headTitle)

// get al paragraphs within the body
const myPcoll = document.querySelectorAll(`body  p`);
console.log(myPcoll);

myPcoll.forEach((el, index) => {
    const linkEl = document.createElement('a');
    const myText = document.createTextNode('link ' + (index + 1));
    linkEl.appendChild(myText);
    linkEl.href = 'google.com';
    el.appendChild(linkEl);
});
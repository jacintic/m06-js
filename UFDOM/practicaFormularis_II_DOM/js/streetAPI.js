var outBox = document.getElementById('out');

    var cercadorTipusVia = new geoBCN.Html.InputAutocomplete({
        inputId: 'tipusVia',
        label: 'Tipus Via',
        combobox: true,
        "origen": function (request, response) {

            geoBCN.Territori.get('tipusvies', {
                nom: cercadorTipusVia.getText(),
                nomesCamps: ['nom', 'codi'], //Solament necessitem aquests camps de retorn
            }).then(function (data) {

                if (data.estat === 'OK') {
                    var candidates = [];
                    var list = data.resultats;
                    for (var i = 0; i < list.length; i++) {
                        candidates.push({
                            label: list[i].nom,
                            value: list[i].codi
                        });
                    }
                    response(candidates);
                } else {
                    console.warn("Error en la cerca")
                }
            });
        },
        esborrat: function () {
            outBox.innerHTML = '';
        },
        seleccio: function () {
            outBox.innerHTML = '';
        }
    });

    /*Autocomplete de carrers*/
    var cercadorCarrer = new geoBCN.Html.InputAutocomplete({
        inputId: 'carrer',
        label: 'Carrer',
        master: cercadorTipusVia,   //El carrer depén del tipus via
        masterRequired: false, 	  //El tipus NO es obligatori
        origen: function (request, response) {

            geoBCN.Territori.get('vies', {
                id_tipus_via: cercadorTipusVia.getCodi(),
                nom: cercadorCarrer.getText(),
                nomesCamps: ['Nom18', 'Codi'], //Solament necessitem aquests camps de retorn
            }).then(function (data) {

                if (data.estat === 'OK') {
                    var candidates = [];
                    var list = data.resultats;
                    for (var i = 0; i < list.length; i++) {
                        candidates.push({
                            label: list[i].nom18,
                            value: list[i].codi
                        });
                    }
                    response(candidates);
                } else {
                    console.warn("Error en la cerca")
                }
            });
        },
        esborrat: function () {
            outBox.innerHTML = '';
        },
        seleccio: function () {
            outBox.innerHTML = '';
        }
    });

    var cercadorPortal = new geoBCN.Html.InputAutocomplete({
        inputId: 'portal',
        label: 'Portal',
        master: cercadorCarrer,   //El portal depèn del carrer
        masterRequired: true, 	//El carrer es obligatori
        "origen": function (request, response) {
            geoBCN.Territori.get('portals', {
                id_via: cercadorCarrer.getCodi(),
                numero: cercadorPortal.getText(),
            }).then(function (data) {

                if (data.estat === 'OK') {
                    var candidates = [];
                    var list = data.resultats;
                    for (var i = 0; i < list.length; i++) {
                        candidates.push({
                            label: list[i]["numeracioPostal"],
                            value: list[i]
                        });
                    }
                    response(candidates);
                } else {
                    console.warn("Error en la cerca")
                }
            });
        },
        esborrat: function () {
           outBox.innerHTML = '';
        },
        seleccio: function (ev, adr) {
            var coordTressor = geoBCN.Utils.projectaCoordenades('EPSG:25831', 'TRESOR', adr.localitzacio.x, adr.localitzacio.y);
            var text = '';
            var tab = "    ";
            text += 'Adreça seleccionada:<br/><br/>' +
                tab + '- Coordenades (' + adr.localitzacio.proj + '): ' + adr.localitzacio.x + ', ' + adr.localitzacio.y + '\n' +
                tab + '- CoordenadesTressor (TRESOR): ' + coordTressor[0] + ', ' + coordTressor[1] + '\n' +
                tab + '- Tipus Via: ' + adr.carrer.tipusVia.nom + ' (' + adr.carrer.tipusVia.abreviatura + ')\n' +
                tab + '- Tipus Via Id: ' + adr.carrer.tipusVia.codi + '\n' +
                tab + ' - Carrer - nom: ' + adr.carrer.nom + '\n' +
                tab + ' - Carrer - nom18: ' + adr.carrer.nom18 + '\n' +
                tab + ' - Carrer - nom27: ' + adr.carrer.nom27 + '\n' +
                tab + ' - Carrer - nomComplet: ' + adr.nomComplet + '\n' +
                tab + ' - numeroInicial: ' + adr.numeroPostalInicial + adr.lletraPostalInicial + '\n' +
                tab + ' - numeroFinal: ' + adr.numeroPostalFinal + adr.lletraPostalFinal + '\n' +
                tab + ' - codiCarrer: ' + adr.carrer.codi + '\n' +
                tab + ' - codiIlla: ' + adr.illa.codi + '\n' +
                tab + ' - codiDistricte: ' + adr.districte.codi + '\n' +
                tab + ' - descripcioDistricte: ' + adr.districte.descripcio + '\n' +
                tab + ' - codiBarri: ' + adr.barri.codi + '\n' +
                tab + ' - nomBarri: ' + adr.barri.nom + '\n' +
                tab + ' - codiPostal: 080' + adr.districtePostal + '\n' +
                tab + ' - Solar: ' + adr.solar + '\n' +
                tab + ' - Parcela: ' + adr.parcelaId + '\n' +
                tab + ' - referenciaCadastral: ' + adr.referenciaCadastral + '\n' +
                tab + ' - Secció censal: ' + adr.seccioCensal + '\n';

            text = text.replace(/\n/g, '<br/>');
            outBox.innerHTML = text;
        }
    });
/*
1. Nom: L’usuari ha de poder escriure el seu nom. Ha de tenir 
una longitud de 40, però com a molt ha de poder escriure 30 
caràcters.

2. 1er Cognom: L’usuari ha de poder escriure el seu cognom. 
Ha de tenir una longitud de 80, però com a molt ha de poder 
escriure 60 caràcters.

3. 2on Cognom: L’usuari ha de poder escriure el seu cognom. 
Ha de tenir una longitud de 80, però com a molt ha de poder 
escriure 60 caràcters.

4. Tipus de document: DNI/NIE
    a) Si es DNI: L’usuari ha de poder escriure el seu número 
    i lletra del DNI. Ha de tenir una longitud de 10, i com a 
    molt ha de poder escriure 9 caràcters.
    b)Si es NIE: L’usuari ha de poder escriure la lletra seguida 
    de 7 números.

5. Ets: Home / dona

6. Data naixement. Format dd/mm/aaaa.
No t’has de poder registrar si ets menor de 18 anys.

7. Nivell d’estudis. llistat Opcions: Educació secundària obligatòria, 
Programes de formació i inserció, Batxillerat, Formació professional, 
Ensenyaments artístics superiors, Ensenyaments esportius, Idiomes a 
les EOI, Educació d'adults, Cursos per accedir a cicles, Itineraris 
formatius específics, Estudis estrangers

8. Situació laboral: estudiant, aturat, treball compte propi, treball 
compte aliè, pensionista.

9. Nacionalitat :espanyola, UE, FUE

10. Pais: Busca un llistat de Països per Internet.
*/

/*
4. Tipus de document: DNI/NIE
    a) Si es DNI: L’usuari ha de poder escriure el seu número 
    i lletra del DNI. Ha de tenir una longitud de 10, i com a 
    molt ha de poder escriure 9 caràcters.
    b)Si es NIE: L’usuari ha de poder escriure la lletra seguida 
    de 7 números.
*/

const globalScope = {
    accessKeyVerified : false,
    conditionsAccepted : false
}


/*
///////// ATENCIÓ VALIDA DNI I NIE D'ACORD AMB: 
http://www.interior.gob.es/web/servicios-al-ciudadano/dni/calculo-del-digito-de-control-del-nif-nie
I
https://ejemplos.net/ejemplos-de-nie/
*/
const dniValidation = ['T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E'];


const myDoc = document.getElementById('docType');

const validateDocs = (docnums, letter) => {
    return dniValidation[docnums % 23] == letter;
}
const docMod = document.getElementById('doc'); 
const setLengthDoc = () => { 
    let result = false;
    
    let myLength = 9;
    if (myDoc.value == 'nie') {
        myLength = 9;
        if (docMod.value.match(/[X-Z]{1}[0-9]{7}[A-Z]{1}/)){
            const nieCtrl = ['X','Y','Z'];
            const strgBuildNie = nieCtrl.indexOf(docMod.value.substring(0,1)) + docMod.value.substring(1,8);
            result = validateDocs(strgBuildNie,docMod.value.substring(8,9));
            //console.log(strgBuildNie , docMod.value.substring(8,9), result, dniValidation[strgBuildNie % 23]);
        }
    } else {
        result = validateDocs(docMod.value.substring(0,8),docMod.value.substring(8,9));
        //console.log(docMod.value.substring(0,8),docMod.value.substring(8,9));
    }
    docMod.maxLength = myLength;
    const docWarn = document.getElementById('docWarn');
    if (!result && !docWarn.classList.contains('visible')) {
        docMod.setCustomValidity("Invalid field.");
        docWarn.classList.add('visible');
    } else if (result && docWarn.classList.contains('visible')){
        docMod.setCustomValidity("");
        docWarn.classList.remove('visible');
    }
}

myDoc.addEventListener('blur', setLengthDoc);
docMod.addEventListener('blur', setLengthDoc);




/*
6. Data naixement. Format dd/mm/aaaa.
No t’has de poder registrar si ets menor de 18 anys.
*/
const myAge = document.getElementById('age');

const verifyAge = () => {
    // get value in yyyy-mm-dd format
    const myAgeVal = myAge.value;
    // get year
    const year = myAgeVal.substring(0,4);
    const month = myAgeVal.substring(5,7);
    const day = +myAgeVal.substring(8,10);
    // get current date
    const date = new Date();
    const myDay = date.getDate();
    const myMonth = date.getMonth() + 1;
    const myYear = date.getFullYear();
    // calculate age
    let age = myYear - year;
    if (myMonth < month || myMonth == month && day > myDay) {
        age--;
        
    }
    // error message
    const errorAge = document.getElementById('errAge');
    // apply red outline to form if age is lower than 18
    if (age < 18) {
        myAge.setCustomValidity("Invalid field.");
        
        errorAge.classList.add('checked');
    } else {
        myAge.setCustomValidity("")
        if (errorAge.classList.contains('checked')) {
            errorAge.classList.remove('checked');
        }
        
    }
    myAge.min = (myYear - 18) + "-" + myMonth + "-" +  myDay;
}

myAge.addEventListener('change', verifyAge);


/*
7. Nivell d’estudis. llistat Opcions: Educació secundària obligatòria, 
Programes de formació i inserció, Batxillerat, Formació professional, 
Ensenyaments artístics superiors, Ensenyaments esportius, Idiomes a 
les EOI, Educació d'adults, Cursos per accedir a cicles, Itineraris 
formatius específics, Estudis estrangers
*/
// get parent element
const mySelect = document.getElementById('studies');

// define array of select elements
const studiesArr = ['Educació secundària obligatòria', 
    'Programes de formació i inserció', 
    'Batxillerat', 
    'Formació professional', 
    'Ensenyaments artístics superiors', 
    'Ensenyaments esportius', 
    'Idiomes a les EOI', 
    `Educació d'adults`, 
    'Cursos per accedir a cicles', 
    'Itineraris formatius específics', 
    'Estudis estrangers'];

// add elements to select
const processEl = (el, index, elem) => {
    const  opt = document.createElement('option');
    opt.value = index;
    const text = document.createTextNode(el);
    opt.appendChild(text);
    elem.appendChild(opt);
}
// iterat through elements array building select options
studiesArr.forEach((el,index) => processEl(el, index,mySelect));

/*
8. Situació laboral: estudiant, aturat, treball compte propi, treball 
compte aliè, pensionista.
*/

// get parent element
const selSit = document.getElementById('situacio');

// situacio array
const sitArr = ['estudiant', 
    'aturat', 
    'batxillerat', 
    'treball compte propi', 
    'treball compte aliè', 
    'pensionista'];

 // iterat through elements array building select options
sitArr.forEach((el,index) => processEl(el, index,selSit));

/*
9. Nacionalitat :espanyola, UE, FUE
*/
// get parent element
const nationality = document.getElementById('nationality');

// situacio array
const natArr = ['espanyola', 
    'UE', 
    'FUE',];

 // iterat through elements array building select options
natArr.forEach((el,index) => processEl(el, index,nationality));


/*
10. Pais: Busca un llistat de Països per Internet.
*/
// get parent element
const country = document.getElementById('country');

// countries array
const countries = [
'Albania',
'Andorra',
'Armenia',
'Austria',
'Azerbaijan',
'Belarus',
'Belgium',
'Bosnia and Herzegovina',
'Bulgaria',
'Croatia',
'Cyprus',
'Czech Republic',
'Denmark',
'Estonia',
'Finland',
'France',
'Georgia',
'Germany',
'Greece',
'Hungary',
'Iceland',
'Ireland',
'Italy',
'Kosovo',
'Latvia',
'Liechtenstein',
'Lithuania',
'Luxembourg',
'Macedonia',
'Malta',
'Moldova',
'Monaco',
'Montenegro',
'The Netherlands',
'Norway',
'Poland',
'Portugal',
'Romania',
'Russia',
'San Marino',
'Serbia',
'Slovakia',
'Slovenia',
'Spain',
'Sweden',
'Switzerland',
'Turkey',
'Ukraine',
'United Kingdom',
'Vatican City'
];

 // iterat through elements array building select options
countries.forEach((el,index) => processEl(el, index,country));

/*
Dades de contacte

1. Email: L’usuari ha de poder escriure el seu email.
*/

// validate email
const myMail = document.getElementById('email');
const mailWarn = document.getElementById('mailWarn');

const setInvalid = (valid = true) => {
    if (valid && !mailWarn.classList.contains('invalid')) {
        mailWarn.classList.add('invalid');
        myMail.setCustomValidity("Invalid field.");
    } else if (!valid && mailWarn.classList.contains('invalid')) {
        mailWarn.classList.remove('invalid');
        myMail.setCustomValidity("");
    }

}

const verifyMail = () => {
    const mailTxt = myMail.value == '' ? 'Is null' : myMail.value;
    const myValue = mailTxt.match(/[^@]+@[^@]+\.\D{2,3}/) != null ? mailTxt.match(/[^@]+@[^@]+\.\D{2,3}/)[0] : 'Is null';
    myValue != mailTxt ? setInvalid(true): setInvalid(false);
}

myMail.addEventListener('blur', verifyMail);

/*
2. Telèfon: L’usuari ha de poder escriure el seu número de telèfon mòbil (sense +34). 
Ha de tenir una longitud de 10 però com a molt a de poder escriure 9 caràcters.
*/

// DONE WITH HTML

/*
3. Vius a Barcelona: SI/NO
    a) Si la opció és No, no mostrar res.
    b) Si la opció és si, mostrar el següent formulari:
*/

const liveInBar = document.getElementById('yesBar');
const noBar = document.getElementById('noBar');

const showHide = (visible) => {
    const myDivBar =  document.getElementById('bar');
    if (visible) {
        myDivBar.classList.add('visible');
    } else {
        myDivBar.classList.remove('visible');
    }
}

const setVisible = () => {
    liveInBar.checked ? showHide(true): showHide(false);
}

liveInBar.addEventListener('click', setVisible);
noBar.addEventListener('click', setVisible);
/*
De moment afegim tots els camps, però els mostrarem amb dades de proves.
Més endavant, els omplirem fent servir la API RESTful de GeoBCN, mapes i 
serveis d'informació geoespacial de la ciutat de Barcelona.
*/

/*
Clau d'accés i nota legal

    * Valida que la clau d’accés i la confirmació sigui la mateixa.
    * 
    * El formulari no s’enviarà si la opció He llegit, comprenc la política de privacitat no està seleccionada.
*/

const accK = document.getElementById('accessKey');
const confA = document.getElementById('confAcc');
const keyWarn = document.getElementById('keyWarn');

const verifyKey = () => {
    if (confA.value != accK.value && !keyWarn.classList.contains('visible')) {
        keyWarn.classList.add('visible');
        globalScope.accessKeyVerified = false;
    } else if (confA.value == accK.value && keyWarn.classList.contains('visible')) {
        keyWarn.classList.remove('visible');
        globalScope.accessKeyVerified = true;
    }
}           

confA.addEventListener('blur', verifyKey);



/*
Botó Registrar
El formulari s’enviarà via servei SERVEI POST. Investiguem y documentar amb el que coneixem, com ho podem fer. Més endavant o implementarem.


Feu servils estils per donar-li una mica de disseny i per millorar l'aparença. 

*/

/*
const globalScope = {
    accessKeyVerified : false,
    conditionsAccepted : false
}
*/

const mySubm = document.getElementById('mySubm');
const conditions = document.getElementById('conditions');

const setTerms = () => {
    if (!conditions.checked && !condWarn.classList.contains('visible')) {
        condWarn.classList.add('visible');
        globalScope.conditionsAccepted = false;
        conditions.setCustomValidity("Invalid field.");
    } else if (conditions.checked && condWarn.classList.contains('visible')) {
        condWarn.classList.remove('visible');
        globalScope.conditionsAccepted = true;
        conditions.setCustomValidity("");
    }
}

mySubm.addEventListener('click', setTerms);
conditions.addEventListener('click', setTerms);
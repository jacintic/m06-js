/*
-(1 punt)Amb “Event Listeners” mostra un missatgea l’usuari quan es carrega lapàgina.
*/
window.addEventListener('load', (event) => {
    console.log('Page just loaded');
});
/**
 * -(1 punt)Implementa el següents camps del formulariamb les seves 
 * opcions corresponents tot configurant-los per a que no tinguin
 * seleccionats cap valor perdefecte:
 *  -Placa Base
 *  -Processador
 *  -RAM
 * -Font d’Alimentació
 * -SSD
 */
///// IMPLEMENTADO EN EL HTML//////
/**
  * -(1 punt)Assegura’t que abans d’enviar el formulari,tots els camps del formulari
  * anterior tenen seleccionat una opció. A més, implementales condicions de validació pel 
  * Processador i9. Si no es compleix, cal indicar-hoa l’usuari i no podrà enviar 
  * elformulari.
  *     -Processador i9 (només es pot muntar amb la “PlacaBase C”).
  */

const formIsValid = true;

const elementComp = document.getElementById('elementComp');

const myForm = document.getElementById('myForm');

const moboWarning = document.getElementById('mobowarn');

const checkSelected = () => {
    // check selected
    const allSelects = document.querySelectorAll('form select');
    // isValid
    let isValid = true;
    const selectsAreSel = (el) => el.selectedIndex > 0;
    const selectsVal =  allSelects.forEach(el => {
        if (el.selectedIndex == 0)
            isValid = false;
    });
    return isValid;
}

const setWarningProcessor = () => {
    moboWarning.classList.add('warn');
}

const checkProcessor = () => {
    const proc = document.getElementById('processor');
    const moBo = document.getElementById('motherboard');
    if (moBo.value == 'Motherboard C' && proc.value == 'i9') {
        if(moboWarning.classList.contains('warn')) {
            moboWarning.classList.remove('warn');
        }
        return true;
    }
    setWarningProcessor();
    return false;
}




/**
 * -(1 punt)Amb “checkbox”’s o “radiobuttons”’s:
 *  -Permet a l’usuari escollir si cal enviar l’ordinadoro el passarà a 
 *  recollir per labotiga. El camp no ha d’estar seleccionat per defecte.
 */


/*
*-(1 punt)Assegura’t que abans d’enviar el formulari,el camp anterior estàseleccionat. 
Si no està seleccionat, cal indicar-hoa l’usuari i no podrà enviar elformulari.
 */

const delivery = document.getElementById('deliveryWarn');
const setWarningDelivery = () => {
    delivery.classList.add('warn');
}

const radioChecked = () => {
    const delElems = document.getElementsByName('delivery');
    // isValid
    let isValid = false;
    const selectsVal =  delElems.forEach(el => {
        if (el.checked) {
            if(delivery.classList.contains('warn')) {
                delivery.classList.remove('warn');
            }
            isValid = true;
        }
    });
    console.log(isValid);

    if (!isValid) {
        setWarningDelivery();
    }
    return isValid;
}



/**
 * -(1 punt)Si cal enviar l’ordinador, l’usuari hauràd’entrar les següents 
 * dades per apoder enviar el formulari:
 *  
 * -Nom i Cognoms: L’usuari ha de poder 
 * escriure el seunom. Ha de tenir unalongitud de 60, però com a molt ha de 
 * poder escriure50 caràcters.
 *  
 * -Telèfon: L’usuari ha de poder escriure el seu 
 * númerode telèfon mòbil (sense+34). Ha de tenir una longitud de 10 però com a 
 * molta de poder escriure 9caràcters.-Adreça:
 *  
 * -Carrer: Camp de text. Ha de tenir 
 * una longitud de60, però com a moltha de poder escriure 50 caràcters.
 *  
 * -Número del 
 * carrer: Camp numèric. Ha de tenir una longitudde 20,però com a molt l’usuari 
 * ha de poder escriure 4 caràcters.
 *  
 * -Codi Postal: Camp numèric. Ha de tenir una longitudde 20, peròcom a molt 
 * l’usuari ha de poder escriure 5 caràcters.-Ciutat: Camp de text. Ha de 
 * tenir una longitud de60, però com a moltl’usuari ha de poder escriure 
 * 50 caràcters.
 */

const deliveryGroup = document.getElementById('deliveryGrp');

const toggleDel = (opt) => {
    if (opt) {
        if (deliveryGroup.classList.contains('hidden')) {
            deliveryGroup.classList.remove('hidden');
        }
        return;
    }
    if (!deliveryGroup.classList.contains('hidden')) {
        deliveryGroup.classList.add('hidden');
    }
}


const pick = document.getElementById('pickup'); 
const del = document.getElementById('send');

del.addEventListener('click',toggleDel(true));
pick.addEventListener('click',toggleDel(false));

const validateForm = () => {
    checkSelected();
    checkProcessor();
    radioChecked();
    return checkSelected() && checkProcessor() && radioChecked();
    
};





myForm.addEventListener('submit', (event) => {
    if (!validateForm()) {
        console.log("Form is not valid.");
        event.preventDefault();
        return;
    }
    console.log("Form is valid.");
    event.preventDefault(); // comentar para version final
});
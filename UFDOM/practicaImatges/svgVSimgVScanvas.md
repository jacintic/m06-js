# Exercici 6: IMAGE versus SVG versus CANVAS Ara que coneixes més els elements imatges, SVG i CANVAS, podries descriure un comparatiu entre ells?. Fes servir un markdown.  
  
## Imagenes  
**Pros:** Pueden ser bastante resultonas por defecto. Aceptan transparencias por defecto.  
**Cons:** Parece ser que animar una imagen con javascript utiliza bastantes recursos. Si amplias la imagen por supuesto se vera pixelada y es algo no deseable.  
  
## SVG  
**Pros:** Permite efectos muy resultones por defecto, como ahora la animación de un trazado. En principio se pueden escalar sin problemas.
  
**Cons:** Transformaciones de posicion con transformZ o similares resultan en perdida de calidad del mismo modo que lo hace una imagen, es decir no conserva propiedades vectoriales. Tras leer un poco parace ser que si es posible conservar las propiedades vecotriales pero hay qu seguir unos metodos especificos para ello:
[link escalado svg](https://css-tricks.com/scale-svg/).  
  
### Canvas  
**Pros:** Es bastante comodo trabajar nativamente con una reticula, su naturlaeza "matematica" parece muy ocnveniente para animaciones como por ejemplo la simulación de propiedades físicas. 
**Cons:** La naturaleza de tipo pixel supongo que puede ser relativamente problematica tambien.
/**
  * Exercici 5: Aprenentatge actiu amb CANVAS
En aquest exercici us animo a fer el mateix que en l’exercici 
anterior, però en  aquest cas amb CANVAS. Investiga i programa 
alguna transició bàsica amb CANVAS. 

  * 
  */
var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var raf;

var ball = {
    x: 100,
    y: 100,
    vx: 5,
    vy: 2,
    radius: 25,
    color: 'blue',
<<<<<<< HEAD
=======
    friction: 0.005,
>>>>>>> 77da7a0e89ab389055795d6d82c1bd006741aaca
    draw: function () {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.fillStyle = this.color;
        ctx.fill();
    }
};


function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ball.draw();

    // physics
    ball.vy *= .99;
    ball.vy += .25;

    // velocity
    ball.x += ball.vx;
    ball.y += ball.vy;

    // lower boundary check
    if ((ball.y + ball.radius) > canvas.height) {
        ball.y = canvas.height - ball.radius;
    }

    // collission taking radius into account
    if (ball.y + ball.vy + ball.radius > canvas.height || ball.y + ball.vy - ball.radius < 0) {
        ball.vy = -ball.vy;
    }
    if (ball.x + ball.vx + ball.radius > canvas.width || ball.x + ball.vx - ball.radius < 0) {
        ball.vx = -ball.vx;
    } 
<<<<<<< HEAD
    
=======

    // friction
    ball.vx > 0 ? 
        ball.vx -= ball.friction : 
        ball.vx < 0 ? 
            ball.vx += ball.friction : 
        ball.vx == 0 ? 
            ball.vx = 0 : ball.vx = 0;

>>>>>>> 77da7a0e89ab389055795d6d82c1bd006741aaca
    raf = window.requestAnimationFrame(draw);
}

canvas.addEventListener('mouseover', function (e) {
    raf = window.requestAnimationFrame(draw);
});

canvas.addEventListener('mouseout', function (e) {
    window.cancelAnimationFrame(raf);
});

ball.draw();

// soruce: https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Advanced_animations
<<<<<<< HEAD
//// tweaked to not go outside of the frame by me ^^
=======
//// tweaked to not go outside of the frame and use friction by me ^^
>>>>>>> 77da7a0e89ab389055795d6d82c1bd006741aaca
/**
 *
 * Exercici 6: IMAGE versus SVG versus CANVAS
Ara que coneixes més els elements imatges, SVG i CANVAS, podries
descriure un comparatiu entre ells?. Fes servir un markdown.

 */
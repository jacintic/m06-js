/*
Exercici 1: Moure imatges amb botons
En una pàgina html inserim una imatge <img>. 
Volem moure la imatge a una ubicació diferent 
mitjançant botons.
Programarem quatre botons (esquerra, dreta, 
superior, inferior) per establir la
direcció de moviment de la imatge.
TIPS: La ubicació actual de la imatge es pot 
recollir mitjançant els valors offsetLeft i
offsetTop. A aquest valor li afegirem un valor 
de pas que estableix la nova posició de la imatge. 
*/

// bee movement constants
const beePos  = {
    x: 0,
    y: 0,
    facing: -100,
    xMove: 15,
    yMove: 5
}

// buttons that will move the bee and the bee
const topB = document.getElementById("top");
const bottB = document.getElementById("bottom");
const leftB = document.getElementById("left");
const rightB = document.getElementById("right");

// bee
const bee = document.getElementById("bee");

// build the move function
const moveBee = (e) => {
    const direcc = e.target.id;
    switch (direcc) {
        case 'bottom':
            if (beePos.y < 265) {
                beePos.y += beePos.yMove;
            }
            break;
            
        case 'top':
            if (beePos.y > 0) {
                beePos.y -= beePos.yMove;
            }
            break;
        case 'left':
            if (beePos.x >= 0) {
                beePos.facing = 100;
                beePos.x -= beePos.xMove;
            }
            break;
        case 'right':
            if (beePos.x <= 510) {
                beePos.facing = -100;
                beePos.x += beePos.xMove;
                
            }
            break;
        default:
            break;
    };
    bee.style.transform = `translateX(${beePos.x}px) translateY(${beePos.y}px) scaleX(${beePos.facing}%)`;
} 

// build the click handlers
const beebuttons = [topB,bottB,leftB,rightB];
beebuttons.forEach(el => {
    el.addEventListener("click",moveBee);
});


/*
Exercici 2: Moviment continu de la imatge a la pantalla
En l’exercici anterior, hem vist com moure la imatge a la 
pantalla, ampliarem la mateixa tècnica per moure la 
imatge automàticament verticalment de dalt a baix i 
després horitzontalment d’esquerra a dreta. Per fer-ho 
farem crea un nou html i farem ús del temporitzador
TIP: Per donar l'ordre de continu moviment podem fer ús 
de la funció setTimeout. El mateix temporitzador es 
restableix mitjançant la funció clearTimeout. Cada 
restabliment posicionarà la imatge en una ubicació fixa. 
Per a això hem utilitzat una funció reset1 ()

*/

// animation constatnts
const animatedBee = {
    isAnimated: false,
    myInterval: null,
}

// get buttons
const myStart = document.getElementById("start");
const myReset = document.getElementById("reset");

// get bee
const bee2 = document.getElementById("bee2");



// start animation function
const startAnimation = () => {
    // define antimation
    const myAnimation = () => {
        bee2.style.transform = `translateX(0px) translateY(0px) scaleX(-100%)`;
        setTimeout(function(){
            bee2.style.transform = `translateX(350px) translateY(250px) scaleX(-100%)`;
        }, 1000);
        bee2.style.transform = `translateX(350px) translateY(250px) scaleX(100%)`;
        setTimeout(function(){
            bee2.style.transform = `translateX(150px) translateY(100px) scaleX(100%)`;
        }, 2000);
        setTimeout(function(){
            bee2.style.transform = `translateX(0px) translateY(0px) scaleX(100%)`;
        }, 2850);
        bee2.style.transform = `translateX(0px) translateY(0px) scaleX(-100%)`;
    }
    // before loading interval
    myAnimation();
    // after interval is loaded (3 seconds)
    animatedBee.myInterval = setInterval(function(){
        myAnimation();
    }, 3000);
}
// reset animation function
const resetAnimation = () => {
    clearInterval(animatedBee.myInterval);
    setTimeout(function(){
        bee2.style.transform = `translateX(0px) translateY(0px) scaleX(-100%)`;
    }, 3000);
    
}

// click animate function
const animateBee = (e) => {
    const anim = e.target.id;
    switch (anim) {
        case "start":
            if (!animatedBee.isAnimated) {
                startAnimation();
                animatedBee.isAnimated = true;
            };
            break;
        case "reset":
            if (animatedBee.isAnimated) {
                resetAnimation();
                animatedBee.isAnimated = false;
            };
            break;
        default:
            break;
    }
}

// click handlers
const beebuttons2 = [myStart,myReset];
beebuttons2.forEach(el => el.addEventListener("click",animateBee));

/*
Exercici 3: Crear una galeria d’imatges
Afegim una imatge a través de la etiqueta  html <img>. Aquesta 
imatge la anirem  canviant amb javascript segons l’opció 
seleccionada d’una llista d’opcions ( <select> ).  

TIPS: 
objecte imatge: document.images.img.src 

*/

// documentació exercici 3:
    // https://github.com/nolimits4web/Swiper/blob/master/demos/030-pagination.html
    // https://swiperjs.com/demos#

/**
 * 
 * Exercici 4: Aprenentatge actiu SVG 
En aquest exercici us animo a que feu un viatge ràpid sobre 
els que són els gràfics vectorials i SVG, perquè són útils 
conèixer-los, i com s'inclouen els SVG dins de les pàgines 
web. Per això, escull algún element SVG de forma simple ( 
    cuadrat, linea, cercle …. ) i fes alguna transformació 
    ( més gran, més petit, ... ) i/o transició (moure’l, 
        rotar, transposar ... ) d’aquest element a partir 
        de Javascript. 

 */

/**
  * Exercici 5: Aprenentatge actiu amb CANVAS
En aquest exercici us animo a fer el mateix que en l’exercici 
anterior, però en  aquest cas amb CANVAS. Investiga i programa 
alguna transició bàsica amb CANVAS. 

  * 
  */


/**
 * 
 * Exercici 6: IMAGE versus SVG versus CANVAS
Ara que coneixes més els elements imatges, SVG i CANVAS, podries 
descriure un comparatiu entre ells?. Fes servir un markdown. 

 */
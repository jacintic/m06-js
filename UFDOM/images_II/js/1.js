/*
var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");

c.style.width = '100vh';
c.style.height = '100vw'; 

// Create gradient
var grd = ctx.createRadialGradient(540, 360, 150, 540, 360, 720);
grd.addColorStop(0, "red");
grd.addColorStop(1, "white");

// Fill with gradient
ctx.fillStyle = grd;
ctx.fillRect(0, 0, 1080, 720);
*/

// mostro
const mostro = document.getElementsByClassName('mostro')[0]; 

let mosX = 0;
let isMoving = false;
let justFired = true;
let shooting = false;
const abc =  ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
let keysPressed = {};

// getpos for random
const randPos = () => Math.random();
// gen random string
const genRanStr = () => Math.random();

// ismoving mostro
const animateRight = () => {
    console.log("im moving!")
    mostro.style.backgroundImage = "url(img/mostro/seq.gif)";
    console.log(mostro.style.background);
}

//
const isIdle = () => {
    mostro.style.backgroundImage = 'url(img/mostro/midle.png)';
    console.log("it stopped " + isMoving); 
    justFired = true;
}

// mostro pos
let mosPos = mostro.style.left == '' ? 3 : mostro.style.left.replaceAll('px');

// move dude
const moveRight = () => {
    mosX += mosPos + 3;
    mostro.style.left =  `${mosX}px`;
    console.log("moving ring now");
    if(isMoving && justFired) {
        animateRight();
        justFired = false;
    }
    return;
}



// shoot function
const shoot = () => {
    if (!shooting) {
        shooting = true;
        const loaderMaker = () => {
            const preFix = window.origin;
            const loaderGifUrl = `url(img/mostro/shoot.gif?a=${genRanStr()})`;
            console.log(loaderGifUrl);
            mostro.style.background = `${loaderGifUrl}`;
        };
            loaderMaker();
    }
    setTimeout(() => {
        isIdle();
    }, 2000);
    shooting = false;
}

// key press handler
document.addEventListener('keydown', (event) => {
    const keyName = event.key;
    console.log('keydown event\n\n' + 'key: ' + keyName);
    switch (keyName) {
        case 'ArrowRight':
            if(!shooting) {
                isMoving = true;
                moveRight();
            }
            break;
        case 'e':
            shoot();
            break;
        default:
            return;
            break;
    }
});

// key press handler
document.addEventListener('keyup', (event) => {
    const keyName = event.key;
    console.log('keydown event\n\n' + 'key: ' + keyName);
    switch (keyName) {
        case 'ArrowRight':
            if(isMoving) {
                isMoving = false;
                isIdle();
            }
            break;
    
        default:
            return;
            break;
    }
});